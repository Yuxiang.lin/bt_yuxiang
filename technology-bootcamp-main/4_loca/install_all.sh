#!/bin/bash

# install_all.sh

# source params.sh
source ./params.sh

# install US 
./install_us.sh

# install CTS
./install_cts.sh

# install AM
./install_am.sh

# install IG
./install_ig.sh

echo "[loca] All components successfully installed"