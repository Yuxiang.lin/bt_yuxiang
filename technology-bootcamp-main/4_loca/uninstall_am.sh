#!/bin/bash

# uninstall_am.sh

source ./params.sh

# stop tomcat server if it is running
"$TOMCAT_INSTALLDIR"/tomcat/bin/shutdown.sh

# sleep for 5 seconds to wait for a proper shutdown
sleep 5

# remove the truststore file
rm -r -f "$DS_TRUSTSTORE"/*

# remove amster folder
rm -r -f "$AMSTER_INSTALLDIR"/*

# remove ssh key path folder
# -- your script here

# remove the default openam folder that is created in the home directory
# -- your script here

# remove the .openamcfg folder created in the home directory
# -- your script here

# remove the tomcat folder in apps
rm -r -f "$TOMCAT_INSTALLDIR"/*

echo "[loca] Removed Access Manager from ${TOMCAT_INSTALLDIR} successfully"