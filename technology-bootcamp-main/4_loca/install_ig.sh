#!/bin/bash

# install_ig.sh
# Guide: https://backstage.forgerock.com/docs/ig/2023.11/getting-started/preface.html

source ./params.sh

# unzip IG-2023.11.0.zip into your IG dir if this dir does not already exist
mkdir -p "$IG_INSTALLDIR"
unzip "$ARTEFACT_DIR"/IG/IG-2023.11.0.zip -d "$IG_INSTALLDIR" >/dev/null
mkdir -p "$IG_INSTALLDIR"/identity-gateway/ig_config
mkdir -p "$IG_INSTALLDIR"/identity-gateway/ig_config/config

cp admin.json "$IG_INSTALLDIR"/identity-gateway/ig_config/config/admin.json

# start the IG and pass in the installation directory so that it reads the admin.json from the "config" folder
"$IG_INSTALLDIR"/identity-gateway/bin/start.sh "$IG_INSTALLDIR"/identity-gateway/ig_config &