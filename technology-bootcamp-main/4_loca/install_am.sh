#!/bin/bash

# install_am.sh
# Guide: https://backstage.forgerock.com/docs/am/7.4/eval-guide/step-1-prepare-server.html
# Guide: https://backstage.forgerock.com/docs/am/7.4/install-guide/prepare-networking.html
# Truststore Guide: https://backstage.forgerock.com/docs/am/7.4/install-guide/prepare-trust-store.html
# Amster Installation: https://backstage.forgerock.com/docs/amster/7.1/user-guide/amster-install.html
# Amster Connecting to AM with Keys: https://backstage.forgerock.com/docs/amster/7.4/user-guide/amster-connecting.html
# OpenAM Amster Installation: https://backstage.forgerock.com/docs/amster/7.1/user-guide/amster-reference.html
# OpenAM Amster CTS Installation: https://backstage.forgerock.com/knowledge/kb/article/a78932209
 
# reference params.sh 
source ./params.sh

# check if tomcat already exists in your app folder and if not extract tomcat from the zip and rename the folder
if [ -d "$TOMCAT_INSTALLDIR"/tomcat ]; then
    echo "TOMCAT Already there"
else
    mkdir -p "$TOMCAT_INSTALLDIR"
    unzip "$ARTEFACT_DIR"/AM/apache-tomcat-9.0.84.zip -d "$TOMCAT_INSTALLDIR" >/dev/null
    mv "$TOMCAT_INSTALLDIR"/apache-tomcat-9.0.84 "$TOMCAT_INSTALLDIR"/tomcat
fi

# check if ds truststore exists in your current folder and if so skip
mkdir -p "$DS_TRUSTSTORE"
cp "$JAVA_HOME"/lib/security/cacerts "$DS_TRUSTSTORE"
keytool -storepasswd -keystore "$DS_TRUSTSTORE"/cacerts -storepass changeit -new "$TRUST_STORE_PASSWORD"

export US_INSTALLATION_PATH="$USERSTORE_DIR"/opendj
export CTS_INSTALLATION_PATH="$CTS_INSTALLDIR"/opendj

keytool -exportcert \
-keystore "$US_INSTALLATION_PATH"/config/keystore \
-storepass $(cat "$US_INSTALLATION_PATH"/config/keystore.pin) \
-alias ssl-key-pair \
-rfc \
-file us-cert.pem 

keytool -exportcert \
-keystore "$CTS_INSTALLATION_PATH"/config/keystore \
-storepass $(cat "$CTS_INSTALLATION_PATH"/config/keystore.pin) \
-alias ssl-key-pair \
-rfc \
-file cts-cert.pem 

keytool \
-importcert \
-file us-cert.pem \
-keystore "$DS_TRUSTSTORE"/cacerts \
-storepass "$TRUST_STORE_PASSWORD" \
-alias us \
-noprompt

keytool \
-importcert \
-file cts-cert.pem \
-keystore "$DS_TRUSTSTORE"/cacerts \
-storepass "$TRUST_STORE_PASSWORD" \
-alias cts \
-noprompt

# check if setenv.sh does not exist in Tomcat's bin dir and if not create it with the following below and change all .sh files in tomcat bin to become executable

cp setenv.sh "$TOMCAT_INSTALLDIR"/tomcat/bin/setenv.sh
chmod +x "$TOMCAT_INSTALLDIR"/tomcat/bin/*.sh

# extract openam from the AM zip to the apps folder
cp "$ARTEFACT_DIR"/AM/AM-7.4.0.war "$TOMCAT_INSTALLDIR"/tomcat/webapps/openam.war

# copy the openam war (AM-7.4.0.war) inside the OPENAM_INSTALLDIR to tomcat/webapps/openam.war folder

# start the tomcat server with bin/startup.sh
"$TOMCAT_INSTALLDIR"/tomcat/bin/startup.sh

# check if AMSTER_INSTALLDIR exists, else unzip the amster here
mkdir -p "$AMSTER_INSTALLDIR"
unzip "$ARTEFACT_DIR"/AM/Amster-7.4.0.zip -d "$AMSTER_INSTALLDIR" >/dev/null

#sleep for 5 seconds more waiting for tomcat to startup
sleep 5

# now try and access ${HOSTNAME}:8080/openam with -L option. Since tomcat could be starting, retry 5 times over 120 seconds
ATTEMPTS=5
INTERVAL=24

for i in $(seq 1 $ATTEMPTS)
do
    STATUS=$(curl -s -o /dev/null -w "%{http_code}" "${HOSTNAME}":8080/openam)
    if [ "$STATUS" -eq 302 ]; then
        echo "am up"
        break
    fi
    echo
    sleep $INTERVAL
done

# generate an SSH Key pair called "amster_rsa" to use when setting up OpenAM to enable secure amster connections
mkdir -p "$SSH_KEY_PATH"
ssh-keygen -m pem -t rsa -N "" -f "$SSH_KEY_PATH"/amster_rsa -b 2048

# check the exit status of curl and print a success or failure message to launch openam
# -- your script here
# -- your script here
# -- your script here
# -- your script here
# -- your script here

# install openam using amster. Update the cfg.amster file with the params needed for installation and invoke amster here
"$AMSTER_INSTALLDIR"/amster/./amster cfg.amster

# connect openam to the CTS server. Update the file cts.amster with the params needed for installation and invoke amster here
"$AMSTER_INSTALLDIR"/amster/./amster cts.amster

# stop tomcat server if it is running
"$TOMCAT_INSTALLDIR"/tomcat/bin/shutdown.sh

# sleep for 5 seconds to wait for a proper shutdown
sleep 5

# start the tomcat server with bin/startup.sh
"$TOMCAT_INSTALLDIR"/tomcat/bin/startup.sh
# now try and access ${HOSTNAME}:8080/openam with -L option. Since tomcat could be starting, retry 5 times over 120 seconds

for i in $(seq 1 $ATTEMPTS)
do
    STATUS=$(curl -s -o /dev/null -w "%{http_code}" "${HOSTNAME}":8080/openam)
    if [ "$STATUS" -eq 302 ]; then
        echo "am up"
        break
    fi
    echo
    sleep $INTERVAL
done

# check the exit status of curl and print a success or failure message to launch openam
# -- your script here
# -- your script here
# -- your script here
# -- your script here
# -- your script here