#!/bin/bash
# params.sh

# create a variable (HOSTNAME) that stores your computer's hostname. This is avoid using localhost
export HOSTNAME=http://openam.example.com

# export a variable (JAVA_HOME) that captures the location of your JDK installation
export JAVA_HOME=/Library/Java/JavaVirtualMachines/zulu-17.jdk/Contents/Home

# create a variable (ARTEFACT_DIR) referencing the folder containing all the FR component zip files
export ARTEFACT_DIR=/Users/yuxianglin/Documents/ForgerockSoftware

# create a variable (INSTALL_BASEDIR) referencing the folder where all the FR components will be installed into
export INSTALL_BASEDIR=/Users/yuxianglin/ForgerockIAM/SingleLocal

# create a variable (USERSTORE_DIR) referencing the folder where the user store will be installed. Concatenate with INSTALL_BASEDIR.
export USERSTORE_DIR=$INSTALL_BASEDIR/US

# create a variable (US_DEPLOYMENT_ID_PASSWORD) which is the password used to create a deployment id
export US_DEPLOYMENT_ID_PASSWORD=changeit

# create a variable (US_DEPLOYMENT_ID_FILE) which is the password used to store a deployment id
export US_DEPLOYMENT_ID_FILE=us_deploymentID.txt

# create a variable (US_PASSWORD) which acts as the userstore root, monitor and identitystore password
export US_PASSWORD=changeit

# create a variable (CTS_INSTALLDIR) referencing the folder where the user store will be installed. Concatenate with INSTALL_BASEDIR.
export CTS_INSTALLDIR=$INSTALL_BASEDIR/CTS

# create a variable (CTS_PASSWORD) which acts as the userstore root, monitor and identitystore password
export CTS_DEPLOYMENT_ID_PASSWORD=changeit
export CTS_PASSWORD=changeit
export CTS_DEPLOYMENT_ID_FILE=cts_deploymentID.txt

# create a variable (TOMCAT_INSTALLDIR) where tomcat will be installed
export TOMCAT_INSTALLDIR=$INSTALL_BASEDIR/AM

# create a variable (SSH_KEY_PATH) pointing to a new directory called ssh under install basedir
export SSH_KEY_PATH=$TOMCAT_INSTALLDIR/Amster/SSH

# create a variable (OPENAM_INSTALLDIR) where openam will be installed
export OPENAM_INSTALLDIR=$TOMCAT_INSTALLDIR/app

# create a variable (AMSTER_INSTALLDIR) where amster will be installed
export AMSTER_INSTALLDIR=$TOMCAT_INSTALLDIR/Amster

# create a variable (DS_TRUSTSTORE) where DS certs will be imported into. You can use your local dir for this 
export DS_TRUSTSTORE=$INSTALL_BASEDIR/trust_store
export TRUST_STORE_PASSWORD=password

# create a variable (IG_INSTALLDIR) where IG will be installed
export IG_INSTALLDIR=$INSTALL_BASEDIR/IG
