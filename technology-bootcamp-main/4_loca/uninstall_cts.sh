#!/bin/bash

# uninstall_cts.sh

# reference the params file
source ./params.sh

# stop the cts server"
export CTS_INSTALLATION_PATH="$CTS_INSTALLDIR"/opendj
echo "stop existing DS first"
"$CTS_INSTALLATION_PATH"/bin/stop-ds

# delete the userstore directory inside your apps folder
rm -r -f "$CTS_INSTALLDIR"/*