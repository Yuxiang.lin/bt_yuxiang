#!/bin/bash
source ./params.sh

export CTS_INSTALLATION_PATH="$CTS_INSTALLDIR"/opendj

"$CTS_INSTALLATION_PATH"/bin/ldapsearch \
--hostname cfg.example.com \
--port 1635 \
--bindDn uid=admin \
--bindPassword "$US_PASSWORD" \
--useSsl \
--usePkcs12TrustStore "$CTS_INSTALLATION_PATH"/config/keystore \
--trustStorePassword:file "$CTS_INSTALLATION_PATH"/config/keystore.pin \
--baseDN ou=tokens \
"coreTokenUserId=*test*"