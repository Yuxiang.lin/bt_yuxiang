#!/bin/bash

# install_us.sh
# Guide: https://backstage.forgerock.com/docs/ds/7.4/getting-started/install.html
# Guide: https://backstage.forgerock.com/docs/ds/7.4/install-guide/profile-am-idrepo.html

# reference the params file here so all variables are loaded
source ./params.sh

# unzip DS-7.4.0.zip into your userstore dir if this dir does not already exist
mkdir -p "$USERSTORE_DIR"
export US_INSTALLATION_PATH="$USERSTORE_DIR"/opendj
if [ -d "$US_INSTALLATION_PATH" ]; then
    sh ./uninstall_us.sh
else
    echo "fresh install"
fi 

unzip -o "$ARTEFACT_DIR"/DirectoryService/DS-7.4.0.zip -d "$USERSTORE_DIR" >/dev/null

# create a variable (DEPLOYMENT_ID). Using the command dskeymgr and the US_DEPLOYMENT_ID_PASSWORD, set the value of DEPLOYMENT_ID variable.
# Store it into the file referenced by env var US_DEPLOYMENT_ID_FILE.
# do the above only if the file deployment.txt does not exist. Else use the value in the US_DEPLOYMENT_ID_FILE and set it into DEPLOYMENT_ID.
# Use bash's if-else command for this
if [ -e "$US_DEPLOYMENT_ID_FILE" ]; then
    echo "use existing deploymentID"
    DEPLOYMENT_ID=$(<"$US_DEPLOYMENT_ID_FILE")
else
    echo "using new deploymentID"
    DEPLOYMENT_ID=$("$US_INSTALLATION_PATH"/bin/dskeymgr create-deployment-id --deploymentIdPassword "$US_DEPLOYMENT_ID_PASSWORD")
    export DEPLOYMENT_ID
    echo "$DEPLOYMENT_ID" > "$US_DEPLOYMENT_ID_FILE"
fi



# run the ds setup command using the profile am-identity-store. You can hard code the port numbers here
# set the result of the setup command ($?) into a variable called install_status
"$US_INSTALLATION_PATH"/setup \
 --serverId first-ds-IDP \
 --deploymentId $DEPLOYMENT_ID \
 --deploymentIdPassword "$US_DEPLOYMENT_ID_PASSWORD" \
 --rootUserDN uid=admin \
 --rootUserPassword "$US_PASSWORD" \
 --monitorUserPassword "$US_PASSWORD" \
 --hostname localhost \
 --adminConnectorPort 4444 \
 --ldapPort 1389 \
 --enableStartTls \
 --ldapsPort 1636 \
 --httpsPort 8443 \
 --replicationPort 8989 \
 --profile am-identity-store \
 --set am-identity-store/amIdentityStoreAdminPassword:"$US_PASSWORD" \
 --start \
 --acceptLicense

# check the status of the setup command and store it into a variable(install_status)
$US_INSTALLATION_PATH/bin/status \
 --bindDn uid=admin \
 --bindPassword "$US_PASSWORD" \
 --hostname localhost \
 --port 4444 \
 --usePkcs12TrustStore "$US_INSTALLATION_PATH"/config/keystore \
 --trustStorePassword:file "$US_INSTALLATION_PATH"/config/keystore.pin

# check if install_status is 0. If yes, then print a success message and start the ds server. Else, print an error message and quit
# if the server started successfully run the status command. Be sure to specify arguments for hostname, port, identity password and trusting all ssl certificates