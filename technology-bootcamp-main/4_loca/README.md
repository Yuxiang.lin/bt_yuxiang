# Exercise 4. Loca-ccelerator

Welcome to Exercise 4! Hopefully by now you've realised Midships is currently a ForgeRock-centric company and we specialise in building and deploying solutions for our clients with a focus on ForgeRock.

Our flagship product is the [Midships Accelerator](https://gitlab.com/midships/forgerock-accelerator-cloud).

The Midships Accelerator automates the deployment of ForgeRock components in any Kubernetes-compliant environment (and this is only one of its many core capabilities).

By the end of this bootcamp, you will be very comfortable working on the Midships Accelerator. However, one way to learn up to it is by trying to replicate the Accelerator in a simpler form - i.e. trying to automate the deployment of a ForgeRock stack on your local computer.

Whilst this idea may sound crazy, I have found that the best way of learning something is actually trying to do it. And so, in this exercise you will build your own accelerator locally on your computer - The `<span style="color:orange">`**Loca-ccelerator**!!! (as i will affectionately refer to it as "loca").

### Key Design Principles

- Loca will be built with shell scripting (assuming bash as your shell)
- Loca will deploy a single instance of the entire FR stack on your local computer
- Loca will provide both install and uninstall option. And by default loca will install in a non-interactive manner. All values will be specified in the paramters file.
- Loca will consist of a parameters file so that there are no computer or environment specific paths hard-coded and once you develop this, it can run on any bash-supported environment.

## Pre-requisities

1. JDK 11. Install JDK 11 for your platform. Once done, you should be able to check the version by running a command like `java --version`. This will display an output similar to this:

```
openjdk 11.0.22 2024-01-16
OpenJDK Runtime Environment (build 11.0.22+7-post-Ubuntu-0ubuntu222.04.1)
OpenJDK 64-Bit Server VM (build 11.0.22+7-post-Ubuntu-0ubuntu222.04.1, mixed mode, sharing)
```

As long is it is version 11+, you should be good. (For those of you who already have JDK17 installed, you can proceed with that version as well.)

2. Login to Taweh's sftp server and download the following FR binaries. They are located in `/ping-identity/forgerock/`. The binaries are:

- AM-7.4.0.zip
- DS-7.4.0.zip
- IDM-7.4.0.zip
- IG-2023.11.0.zip
  Download tomcat from `/apache/tomcat`. The version is:
- apache-tomcat-9.0.85.zip
  Downlaod amster from `/ping-identity/forgerock/access-manager/tools/amster`

 I have placed them in `~/downloads` and will assume this location in this exercise. You may do likewise.

3. Make sure you have at least 4GB of RAM (`free -g`) allocated to your linux environment and 10GB of free disk space(`df -h`).

## Automating the installation

 Throughout this entire exercise, you will be writing shell scripts that install and uninstall each component. So, you will have scripts that install DS, AM, IDM and IG. And you will write a parent script that in-turn invokes each of them (along with appropriate error handling).

 I will provide high-level instructions for creating each script. You are expected to do a little research and write the installer and verify it by yourself.

 **`<span style="color:red">`WARNING: In this exercise, you will hard-code passwords and keys in your deployment scripts. The reason is because I want you to focus on learning the installation and the product. In the real-world, secrets management is absolutely critical when working with clients but not the focus of this exercise.

## Submitting this exercise

1. Copy 4_loca from technology-bootcamp into your own repo (Bt_aja, for example) and push it into the main branch.
2. Create a feature branch and work on the entire exercise from the feature branch.
3. Once you are done with the whole exercise, create a merge request from your feature branch to your own repo's main branch (i.e. from br_feature to main inside bt_aja). Assign Paul and Aja as reviewers.
4. Once we have approved this merge request, go to the repo technology-bootcamp. Change  the main readme.md file to update the check mark and raise a merge request with Paul and Aja as reviewers.
5. Setup a call with Aja or Paul and demo how you can use FR to protect a static website that you created. (instructions for this are below)

**Note: Please make sure you have ShellCheck (linting tool) and Bash Debug installed for this exercise. Make sure you address all the linting issues (that you consider reasonable) and try using Bash Debug to debug the installation process. Remember you can use breakpoints and watch variables **

## [WSL users only] Mapping your hostname correctly

Remember you cannot use localhost, especially with AM. So setting and using the hostname is very important. Since WSL uses NAT you need to perform a few additional steps for testing your installation.

1. Check your hostname in wsl with `hostname` command
2. Check the IP address of your wsl with `hostname -I` command
3. Confirm you can ping this IP from your windows command line (not WSL)
4. Using Admin rights, modify the file c:\windows\system32\drivers\etc\host and add a line mapping your hostname to the wsl ip address. For e.g., this is what i have in my file:

```
# localhost name resolution is handled within DNS itself.
#	127.0.0.1       localhost
#	::1             localhost
172.27.173.131	ajamidships
```

Note: All lines are commented except the last one.

## Installing The User Store (Directory Service)

 For this part, you will be modifying 3 provided files:

- params.sh
- install_us.sh
- uninstall_us.sh

 This is how your install script output should look like.

```
 Using deployment id - AKp1MNvxN34SIFWpe2hUcRxeCbh_1Gg5CBVN1bkVDX_1KwvzX3MJpiFA
Extracting DS in /home/aja/apps/us
Archive:  /home/aja/downloads/DS-7.4.0.zip
<<Unzip output>>
Extraction of DS into UserStore successful
<<license agreement>>
Validating parameters..... Done
Configuring certificates..... Done
Configuring server..... Done
Configuring profile AM identity data store............. Done

To see basic server status and configuration, you can launch
/home/aja/apps/us/opendj/bin/status

DS Server installed successfully
[31/May/2024:22:23:17 +0800] category=JVM severity=NOTICE msgID=1058 msg=ForgeRock Directory Services 7.4.0-20230927045624-f9d8590d5e85dc72a04bd85c415ade5ac66f0fc5 (build 20230927051532, revision number f9d8590d5e85dc72a04bd85c415ade5ac66f0fc5) starting up
[31/May/2024:22:23:17 +0800] category=JVM severity=NOTICE msgID=2233 msg=Installation Directory:  /home/aja/apps/us/opendj
[31/May/2024:22:23:17 +0800] category=JVM severity=NOTICE msgID=2235 msg=Instance Directory:      /home/aja/apps/us/opendj
[31/May/2024:22:23:17 +0800] category=JVM severity=NOTICE msgID=2229 msg=JVM Information: 11.0.22+7-post-Ubuntu-0ubuntu222.04.1 by Ubuntu, 64-bit architecture, 4171235328 bytes heap size
[31/May/2024:22:23:17 +0800] category=JVM severity=NOTICE msgID=2230 msg=JVM Host: ajamidships default/Joan_Warnock, running Linux 5.15.146.1-microsoft-standard-WSL2 amd64, 16679124992 bytes physical memory size, number of processors available 20
[31/May/2024:22:23:17 +0800] category=JVM severity=NOTICE msgID=2231 msg=JVM Arguments: "-Dorg.opends.server.scriptName=start-ds"
[31/May/2024:22:23:17 +0800] category=CORE severity=WARNING msgID=1025 msg='Admin Endpoint' is DEPRECATED for removal since 7.4.0. Its usage is highly discouraged.
[31/May/2024:22:23:19 +0800] category=BACKEND severity=NOTICE msgID=608 msg=The database backend amIdentityStore containing 5 entries has started
[31/May/2024:22:23:20 +0800] category=SYNC.LIFECYCLE severity=NOTICE msgID=2022 msg=Replication server RS(Joan_Warnock) started listening for new connections on address 0.0.0.0 port 8989
[31/May/2024:22:23:20 +0800] category=SYNC.STATE severity=NOTICE msgID=2204 msg=Directory server DS(Joan_Warnock) did not find a generation ID for domain 'cn=schema'. A new generation ID will be computed by exporting the first 1 entries in the domain.
[31/May/2024:22:23:21 +0800] category=SYNC.CONNECTIONS severity=NOTICE msgID=2026 msg=Directory server DS(Joan_Warnock) has connected to replication server RS(Joan_Warnock) for domain "cn=schema" at 127.0.1.1:8989 with generation ID 8408
[31/May/2024:22:23:21 +0800] category=SYNC.CONNECTIONS severity=NOTICE msgID=2026 msg=Directory server DS(Joan_Warnock) has connected to replication server RS(Joan_Warnock) for domain "uid=Monitor" at 127.0.1.1:8989 with generation ID 48
[31/May/2024:22:23:21 +0800] category=SYNC.STATE severity=NOTICE msgID=2204 msg=Directory server DS(Joan_Warnock) did not find a generation ID for domain 'ou=identities'. A new generation ID will be computed by exporting the first 5 entries in the domain.
[31/May/2024:22:23:21 +0800] category=BACKEND severity=NOTICE msgID=610 msg=Exported 5 entries and skipped 0 in 0 seconds (average rate 185.2/sec)
[31/May/2024:22:23:21 +0800] category=SYNC.CONNECTIONS severity=NOTICE msgID=2026 msg=Directory server DS(Joan_Warnock) has connected to replication server RS(Joan_Warnock) for domain "ou=identities" at 127.0.1.1:8989 with generation ID 64947
[31/May/2024:22:23:21 +0800] category=CORE severity=NOTICE msgID=1409 msg=Registered 0 static groups, 0 dynamic groups and 0 virtual static groups. The static group cache is using 0 bytes of memory
[31/May/2024:22:23:21 +0800] category=PROTOCOL.LDAP severity=NOTICE msgID=1845 msg=Started listening for new connections on Administration Connector 0.0.0.0:4444
[31/May/2024:22:23:21 +0800] category=PROTOCOL.LDAP severity=NOTICE msgID=1845 msg=Started listening for new connections on LDAP 0.0.0.0:1389
[31/May/2024:22:23:21 +0800] category=PROTOCOL.HTTP severity=NOTICE msgID=1845 msg=Started listening for new connections on HTTPS 0.0.0.0:8443
[31/May/2024:22:23:21 +0800] category=PROTOCOL.LDAP severity=NOTICE msgID=1845 msg=Started listening for new connections on LDAPS 0.0.0.0:1636
[31/May/2024:22:23:21 +0800] category=CORE severity=NOTICE msgID=1059 msg=The Directory Server has started successfully
[31/May/2024:22:23:21 +0800] category=CORE severity=NOTICE msgID=1061 msg=The Directory Server has sent an alert notification generated by class org.opends.server.core.DirectoryServer (alert type org.opends.server.DirectoryServerStarted, alert ID org.opends.messages.server-1059): The Directory Server has started successfully

>>>> General details

Version                        : ForgeRock Directory Services 7.4.0-20230927045624-f9d8590d5e85dc72a04bd85c415ade5ac66f0fc5
Installation and instance path : /home/aja/apps/us/opendj
Run status                     : Started
Host name                      : ajamidships.
Server ID                      : Joan_Warnock
Administration port (LDAPS)    : 4444
Open connections               : 1


>>>> Running server Java details

Java version       : 11.0.22
Java vendor        : Ubuntu
JVM available CPUs : 20
JVM max heap size  : 4.18 gb


>>>> Connection handlers

Name        : Port : Load m1 rate : Load m5 rate
------------:------:--------------:-------------
HTTPS       : 8443 :          0.0 :          0.0
LDAP        : 1389 :          0.0 :          0.0
LDAPS       : 1636 :          0.0 :          0.0
Replication : 8989 :            - :            -


>>>> Local backends

Base DN       : Entries : Replication : Receive delay : Replay delay : Backend         : Type  : Active cache
--------------:---------:-------------:---------------:--------------:-----------------:-------:-------------
ou=identities :       5 : Normal      :          0 ms :         0 ms : amIdentityStore : DB    :      3.23 mb
uid=Monitor   :       1 : Normal      :          0 ms :         0 ms : monitorUser     : Other :            -
uid=admin     :       1 : -           :             - :            - : rootUser        : Other :            -


>>>> Proxy backends

There are no proxy backends setup in the server

>>>> Disk space

Disk space : State  : Free space
-----------:--------:-----------
/          : normal :    1.03 tb
```

## Installing The Core Token Service (Directory Service)

For this part, you will be modifying 3 provided files:

- params.sh
- install_cts.sh
- uninstall_cts.sh

This is how your install script output should look like.

```
Using deployment id - AKp1MNvxN34SIFWpe2hUcRxeCbh_1Gg5CBVN1bkVDX_1KwvzX3MJpiFA
Extracting DS in /home/aja/apps/cts
Archive:  /home/aja/downloads/DS-7.4.0.zip
<<Unzip output>>
Extraction of DS into CTS successful
<< License agreement output >>
Validating parameters..... Done
Configuring certificates..... Done
Configuring server..... Done
Configuring profile AM CTS data store......... Done

To see basic server status and configuration, you can launch
/home/aja/apps/cts/opendj/bin/status

CTS Server installed successfully
[31/May/2024:22:42:54 +0800] category=JVM severity=NOTICE msgID=1058 msg=ForgeRock Directory Services 7.4.0-20230927045624-f9d8590d5e85dc72a04bd85c415ade5ac66f0fc5 (build 20230927051532, revision number f9d8590d5e85dc72a04bd85c415ade5ac66f0fc5) starting up
[31/May/2024:22:42:54 +0800] category=JVM severity=NOTICE msgID=2233 msg=Installation Directory:  /home/aja/apps/cts/opendj
[31/May/2024:22:42:54 +0800] category=JVM severity=NOTICE msgID=2235 msg=Instance Directory:      /home/aja/apps/cts/opendj
[31/May/2024:22:42:54 +0800] category=JVM severity=NOTICE msgID=2229 msg=JVM Information: 11.0.22+7-post-Ubuntu-0ubuntu222.04.1 by Ubuntu, 64-bit architecture, 4171235328 bytes heap size
[31/May/2024:22:42:54 +0800] category=JVM severity=NOTICE msgID=2230 msg=JVM Host: ajamidships default/Glendon_Marcom, running Linux 5.15.146.1-microsoft-standard-WSL2 amd64, 16679124992 bytes physical memory size, number of processors available 20
[31/May/2024:22:42:54 +0800] category=JVM severity=NOTICE msgID=2231 msg=JVM Arguments: "-Dorg.opends.server.scriptName=start-ds"
[31/May/2024:22:42:54 +0800] category=CORE severity=WARNING msgID=1025 msg='Admin Endpoint' is DEPRECATED for removal since 7.4.0. Its usage is highly discouraged.
[31/May/2024:22:42:55 +0800] category=BACKEND severity=NOTICE msgID=608 msg=The database backend amCts containing 5 entries has started
[31/May/2024:22:42:55 +0800] category=SYNC.LIFECYCLE severity=NOTICE msgID=2022 msg=Replication server RS(Glendon_Marcom) started listening for new connections on address 0.0.0.0 port 8990
[31/May/2024:22:42:55 +0800] category=SYNC.STATE severity=NOTICE msgID=2204 msg=Directory server DS(Glendon_Marcom) did not find a generation ID for domain 'cn=schema'. A new generation ID will be computed by exporting the first 1 entries in the domain.
[31/May/2024:22:42:55 +0800] category=SYNC.CONNECTIONS severity=NOTICE msgID=2026 msg=Directory server DS(Glendon_Marcom) has connected to replication server RS(Glendon_Marcom) for domain "cn=schema" at 127.0.1.1:8990 with generation ID 8408
[31/May/2024:22:42:55 +0800] category=SYNC.CONNECTIONS severity=NOTICE msgID=2026 msg=Directory server DS(Glendon_Marcom) has connected to replication server RS(Glendon_Marcom) for domain "uid=Monitor" at 127.0.1.1:8990 with generation ID 48
[31/May/2024:22:42:55 +0800] category=SYNC.STATE severity=NOTICE msgID=2204 msg=Directory server DS(Glendon_Marcom) did not find a generation ID for domain 'ou=tokens'. A new generation ID will be computed by exporting the first 5 entries in the domain.
[31/May/2024:22:42:55 +0800] category=BACKEND severity=NOTICE msgID=610 msg=Exported 5 entries and skipped 0 in 0 seconds (average rate 416.7/sec)
[31/May/2024:22:42:55 +0800] category=SYNC.CONNECTIONS severity=NOTICE msgID=2026 msg=Directory server DS(Glendon_Marcom) has connected to replication server RS(Glendon_Marcom) for domain "ou=tokens" at 127.0.1.1:8990 with generation ID 69125
[31/May/2024:22:42:55 +0800] category=CORE severity=NOTICE msgID=1409 msg=Registered 0 static groups, 0 dynamic groups and 0 virtual static groups. The static group cache is using 0 bytes of memory
[31/May/2024:22:42:55 +0800] category=PROTOCOL.LDAP severity=NOTICE msgID=1845 msg=Started listening for new connections on Administration Connector 0.0.0.0:4445
[31/May/2024:22:42:55 +0800] category=PROTOCOL.LDAP severity=NOTICE msgID=1845 msg=Started listening for new connections on LDAP 0.0.0.0:1390
[31/May/2024:22:42:55 +0800] category=PROTOCOL.HTTP severity=NOTICE msgID=1845 msg=Started listening for new connections on HTTPS 0.0.0.0:8444
[31/May/2024:22:42:55 +0800] category=PROTOCOL.LDAP severity=NOTICE msgID=1845 msg=Started listening for new connections on LDAPS 0.0.0.0:1637
[31/May/2024:22:42:55 +0800] category=CORE severity=NOTICE msgID=1059 msg=The Directory Server has started successfully
[31/May/2024:22:42:55 +0800] category=CORE severity=NOTICE msgID=1061 msg=The Directory Server has sent an alert notification generated by class org.opends.server.core.DirectoryServer (alert type org.opends.server.DirectoryServerStarted, alert ID org.opends.messages.server-1059): The Directory Server has started successfully

>>>> General details

Version                        : ForgeRock Directory Services 7.4.0-20230927045624-f9d8590d5e85dc72a04bd85c415ade5ac66f0fc5
Installation and instance path : /home/aja/apps/cts/opendj
Run status                     : Started
Host name                      : ajamidships.
Server ID                      : Glendon_Marcom
Administration port (LDAPS)    : 4445
Open connections               : 1


>>>> Running server Java details

Java version       : 11.0.22
Java vendor        : Ubuntu
JVM available CPUs : 20
JVM max heap size  : 4.18 gb


>>>> Connection handlers

Name        : Port : Load m1 rate : Load m5 rate
------------:------:--------------:-------------
HTTPS       : 8444 :          0.0 :          0.0
LDAP        : 1390 :          0.0 :          0.0
LDAPS       : 1637 :          0.0 :          0.0
Replication : 8990 :            - :            -


>>>> Local backends

Base DN     : Entries : Replication : Receive delay : Replay delay : Backend     : Type  : Active cache
------------:---------:-------------:---------------:--------------:-------------:-------:-------------
ou=tokens   :       5 : Normal      :          0 ms :         0 ms : amCts       : DB    :      3.25 mb
uid=Monitor :       1 : Normal      :          0 ms :         0 ms : monitorUser : Other :            -
uid=admin   :       1 : -           :             - :            - : rootUser    : Other :            -


>>>> Proxy backends

There are no proxy backends setup in the server

>>>> Disk space

Disk space : State  : Free space
-----------:--------:-----------
/          : normal :    1.03 tb

```

## Installing Access Manager

When installing the AM, there are a few manual steps needed to edit the .amster files. Everything else should be fully automated.
For this part, you will be modifying 3 provided files:

- params.sh
- install_am.sh
- uninstall_am.sh
- cfg.amster
- cts.amster

This is how your install script output should look like.

```
Using deployment id - AKp1MNvxN34SIFWpe2hUcRxeCbh_1Gg5CBVN1bkVDX_1KwvzX3MJpiFA
Archive:  /home/aja/downloads/apache-tomcat-9.0.85.zip
<< extracting tomcat>>
[loca] Extracted and moved tomcat to /home/aja/apps/tomcat successfully
[loca] Userstore cert added to truststores
[loca] CTS cert added to truststores
[loca] Truststore successfully created with the ds certs
[loca] successfully created setenv.sh inside /home/aja/apps/tomcat/bin/
Using CATALINA_BASE:   /home/aja/apps/tomcat
Using CATALINA_HOME:   /home/aja/apps/tomcat
Using CATALINA_TMPDIR: /home/aja/apps/tomcat/temp
Using JRE_HOME:        /usr/lib/jvm/java-11-openjdk-amd64/
Using CLASSPATH:       /home/aja/apps/tomcat/bin/bootstrap.jar:/home/aja/apps/tomcat/bin/tomcat-juli.jar
Using CATALINA_OPTS:    -Xmx2g -XX:MaxMetaspaceSize=256m -Djavax.net.ssl.trustStore=/home/aja/apps/tomcat/dstruststore -Djavax.net.ssl.trustStorePassword=changeit -Djavax.net.ssl.trustStoreType=jks
Tomcat started.
Archive:  /home/aja/downloads/Amster-7.4.0.zip
<<extracting amster>>
[loca] Successfully extracted Amster into /home/aja/apps/amster
[loca] Sleeping for 5 seconds
[loca] Waiting for tomcat to start and load openam....
<<AM HTML>>
[loca] OpenAM launched successfully
[loca] Instaling AM using Amster
Amster AM Shell (7.4.0 build 56df33df40977206569052a683f5317ade19ee3e, JVM: 11.0.22)
Type ':help' or ':h' for help.
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------
am> :load cfg.amster
06/08/2024 12:52:59:342 PM SGT: Checking license acceptance...
06/08/2024 12:52:59:342 PM SGT: License terms accepted.
06/08/2024 12:52:59:347 PM SGT: Checking configuration directory /home/aja/openam.
06/08/2024 12:52:59:406 PM SGT: ...Success.
06/08/2024 12:53:01:172 PM SGT: Extracting DS, please wait...
06/08/2024 12:53:03:827 PM SGT: Complete
06/08/2024 12:53:03:827 PM SGT: Running DS setup
06/08/2024 12:53:07:012 PM SGT: ...Success
06/08/2024 12:53:07:012 PM SGT: Starting DS for serverRoot /home/aja/openam/opends
06/08/2024 12:53:10:817 PM SGT: ...Success.
06/08/2024 12:53:10:818 PM SGT: ...Success.
06/08/2024 12:53:10:819 PM SGT: Installing embedded DS configuration store in /home/aja/openam/opends
06/08/2024 12:53:10:970 PM SGT: Creating AM suffix
06/08/2024 12:53:13:151 PM SGT: ...Success
06/08/2024 12:53:13:234 PM SGT: Tag swapping schema files.
06/08/2024 12:53:13:265 PM SGT: ...Success.
06/08/2024 12:53:13:266 PM SGT: Loading Schema opendj_config_schema.ldif
06/08/2024 12:53:13:660 PM SGT: ...Success.
06/08/2024 12:53:13:660 PM SGT: Loading Schema opendj_user_schema.ldif
06/08/2024 12:53:13:775 PM SGT: ...Success.
06/08/2024 12:53:13:776 PM SGT: Loading Schema opendj_embinit.ldif
06/08/2024 12:53:13:821 PM SGT: ...Success.
06/08/2024 12:53:13:821 PM SGT: Loading Schema opendj_user_index.ldif
06/08/2024 12:53:13:971 PM SGT: ...Success.
06/08/2024 12:53:13:971 PM SGT: Loading Schema cts-container.ldif
06/08/2024 12:53:14:071 PM SGT: ...Success.
06/08/2024 12:53:14:072 PM SGT: Loading Schema cts-add-schema.ldif
06/08/2024 12:53:14:179 PM SGT: ...Success.
06/08/2024 12:53:14:180 PM SGT: Loading Schema cts-add-multivalue.ldif
06/08/2024 12:53:14:375 PM SGT: ...Success.
06/08/2024 12:53:14:375 PM SGT: Loading Schema cts-add-ttlexpire.ldif
06/08/2024 12:53:14:528 PM SGT: ...Success.
06/08/2024 12:53:14:528 PM SGT: Loading Schema cts-indices.ldif
06/08/2024 12:53:15:221 PM SGT: ...Success.
06/08/2024 12:53:15:221 PM SGT: Loading Schema cts-add-multivalue-indices.ldif
06/08/2024 12:53:15:294 PM SGT: ...Success.
06/08/2024 12:53:15:294 PM SGT: Loading Schema cts-add-ttlexpire-indices.ldif
06/08/2024 12:53:15:334 PM SGT: ...Success.
06/08/2024 12:53:15:334 PM SGT: Loading Schema opendj_dashboard.ldif
06/08/2024 12:53:15:442 PM SGT: ...Success.
06/08/2024 12:53:15:442 PM SGT: Loading Schema opendj_deviceprint.ldif
06/08/2024 12:53:15:527 PM SGT: ...Success.
06/08/2024 12:53:15:527 PM SGT: Loading Schema opendj_kba.ldif
06/08/2024 12:53:15:646 PM SGT: ...Success.
06/08/2024 12:53:15:646 PM SGT: Loading Schema opendj_uma_audit.ldif
06/08/2024 12:53:15:668 PM SGT: ...Success.
06/08/2024 12:53:15:668 PM SGT: Loading Schema opendj_uma_resource_sets.ldif
06/08/2024 12:53:15:689 PM SGT: ...Success.
06/08/2024 12:53:15:689 PM SGT: Loading Schema opendj_uma_labels_schema.ldif
06/08/2024 12:53:15:775 PM SGT: ...Success.
06/08/2024 12:53:15:775 PM SGT: Loading Schema opendj_uma_resource_set_labels.ldif
06/08/2024 12:53:15:795 PM SGT: ...Success.
06/08/2024 12:53:15:795 PM SGT: Loading Schema opendj_uma_pending_requests.ldif
06/08/2024 12:53:15:814 PM SGT: ...Success.
06/08/2024 12:53:15:815 PM SGT: Loading Schema opendj_oathdevices.ldif
06/08/2024 12:53:15:930 PM SGT: ...Success.
06/08/2024 12:53:15:930 PM SGT: Loading Schema opendj_pushdevices.ldif
06/08/2024 12:53:16:029 PM SGT: ...Success.
06/08/2024 12:53:16:029 PM SGT: Loading Schema opendj_webauthndevices.ldif
06/08/2024 12:53:16:122 PM SGT: ...Success.
06/08/2024 12:53:16:122 PM SGT: Loading Schema opendj_deviceprofiles.ldif
06/08/2024 12:53:16:215 PM SGT: ...Success.
06/08/2024 12:53:16:216 PM SGT: Loading Schema opendj_iplanet-am-user-alias-list-index.ldif
06/08/2024 12:53:16:252 PM SGT: ...Success.
06/08/2024 12:53:16:252 PM SGT: Loading Schema opendj_bounddevices.ldif
06/08/2024 12:53:16:344 PM SGT: ...Success.
06/08/2024 12:53:16:344 PM SGT: Stopping DS for serverRoot /home/aja/openam/opends
06/08/2024 12:53:16:657 PM SGT: ...Success.
06/08/2024 12:53:16:657 PM SGT: Rebuilding DS indexes for baseDN dc=openam,dc=forgerock,dc=org
06/08/2024 12:53:18:316 PM SGT: ...Success.
06/08/2024 12:53:18:316 PM SGT: Starting DS for serverRoot /home/aja/openam/opends
06/08/2024 12:53:19:583 PM SGT: ...Success.
06/08/2024 12:53:19:584 PM SGT: Reinitializing system properties.
06/08/2024 12:53:20:042 PM SGT: ...Done
06/08/2024 12:53:20:057 PM SGT: Registering service amAuthConfig.xml
06/08/2024 12:53:20:456 PM SGT: ...Success.
06/08/2024 12:53:20:456 PM SGT: Registering service amAuthHTTPBasic.xml
06/08/2024 12:53:20:529 PM SGT: ...Success.
06/08/2024 12:53:20:530 PM SGT: Registering service idRepoService.xml
06/08/2024 12:53:21:239 PM SGT: ...Success.
06/08/2024 12:53:21:239 PM SGT: Registering service amAuth.xml
06/08/2024 12:53:21:508 PM SGT: ...Success.
06/08/2024 12:53:21:508 PM SGT: Registering service amAuthAD.xml
06/08/2024 12:53:21:560 PM SGT: ...Success.
06/08/2024 12:53:21:560 PM SGT: Registering service amAuthAdaptive.xml
06/08/2024 12:53:21:635 PM SGT: ...Success.
06/08/2024 12:53:21:635 PM SGT: Registering service amAuthAnonymous.xml
06/08/2024 12:53:21:686 PM SGT: ...Success.
06/08/2024 12:53:21:686 PM SGT: Registering service amAuthCert.xml
06/08/2024 12:53:21:737 PM SGT: ...Success.
06/08/2024 12:53:21:738 PM SGT: Registering service amAuthDataStore.xml
06/08/2024 12:53:21:935 PM SGT: ...Success.
06/08/2024 12:53:21:935 PM SGT: Registering service amAuthPersistentCookie.xml
06/08/2024 12:53:21:976 PM SGT: ...Success.
06/08/2024 12:53:21:976 PM SGT: Registering service amAuthJDBC.xml
06/08/2024 12:53:22:021 PM SGT: ...Success.
06/08/2024 12:53:22:021 PM SGT: Registering service amAuthLDAP.xml
06/08/2024 12:53:22:146 PM SGT: ...Success.
06/08/2024 12:53:22:146 PM SGT: Registering service amAuthMSISDN.xml
06/08/2024 12:53:22:185 PM SGT: ...Success.
06/08/2024 12:53:22:186 PM SGT: Registering service amAuthMembership.xml
06/08/2024 12:53:22:221 PM SGT: ...Success.
06/08/2024 12:53:22:221 PM SGT: Registering service amAuthNT.xml
06/08/2024 12:53:22:255 PM SGT: ...Success.
06/08/2024 12:53:22:255 PM SGT: Registering service amAuthOAuth.xml
06/08/2024 12:53:22:305 PM SGT: ...Success.
06/08/2024 12:53:22:305 PM SGT: Registering service amAuthWindowsDesktopSSO.xml
06/08/2024 12:53:22:341 PM SGT: ...Success.
06/08/2024 12:53:22:341 PM SGT: Registering service amAuthOpenIdConnect.xml
06/08/2024 12:53:22:383 PM SGT: ...Success.
06/08/2024 12:53:22:384 PM SGT: Registering service amDelegation.xml
06/08/2024 12:53:22:924 PM SGT: ...Success.
06/08/2024 12:53:22:924 PM SGT: Registering service amFilteredRole.xml
06/08/2024 12:53:22:967 PM SGT: ...Success.
06/08/2024 12:53:22:967 PM SGT: Registering service amG11NSettings.xml
06/08/2024 12:53:23:037 PM SGT: ...Success.
06/08/2024 12:53:23:037 PM SGT: Registering service amLogging.xml
06/08/2024 12:53:23:119 PM SGT: ...Success.
06/08/2024 12:53:23:119 PM SGT: Registering service amNaming.xml
06/08/2024 12:53:23:171 PM SGT: ...Success.
06/08/2024 12:53:23:172 PM SGT: Registering service amPlatform.xml
06/08/2024 12:53:23:288 PM SGT: ...Success.
06/08/2024 12:53:23:289 PM SGT: Registering service amPolicy.xml
06/08/2024 12:53:23:767 PM SGT: ...Success.
06/08/2024 12:53:23:767 PM SGT: Registering service amPolicyConfig.xml
06/08/2024 12:53:23:889 PM SGT: ...Success.
06/08/2024 12:53:23:889 PM SGT: Registering service amRealmService.xml
06/08/2024 12:53:23:904 PM SGT: ...Success.
06/08/2024 12:53:23:904 PM SGT: Registering service amSession.xml
06/08/2024 12:53:24:062 PM SGT: ...Success.
06/08/2024 12:53:24:062 PM SGT: Registering service amWebAgent.xml
06/08/2024 12:53:24:115 PM SGT: ...Success.
06/08/2024 12:53:24:115 PM SGT: Registering service amUser.xml
06/08/2024 12:53:24:170 PM SGT: ...Success.
06/08/2024 12:53:24:171 PM SGT: Registering service identityLocaleService.xml
06/08/2024 12:53:24:295 PM SGT: ...Success.
06/08/2024 12:53:24:295 PM SGT: Registering service amAgent70.xml
06/08/2024 12:53:24:316 PM SGT: ...Success.
06/08/2024 12:53:24:316 PM SGT: Registering service amAuthRadius.xml
06/08/2024 12:53:24:361 PM SGT: ...Success.
06/08/2024 12:53:24:361 PM SGT: Registering service amAuthHOTP.xml
06/08/2024 12:53:24:465 PM SGT: ...Success.
06/08/2024 12:53:24:465 PM SGT: Registering service amAuthSecurID.xml
06/08/2024 12:53:24:492 PM SGT: ...Success.
06/08/2024 12:53:24:492 PM SGT: Registering service AgentService.xml
06/08/2024 12:53:24:719 PM SGT: ...Success.
06/08/2024 12:53:24:719 PM SGT: Registering service policyIndex.xml
06/08/2024 12:53:24:833 PM SGT: ...Success.
06/08/2024 12:53:24:834 PM SGT: Registering service entitlement.xml
06/08/2024 12:53:25:045 PM SGT: ...Success.
06/08/2024 12:53:25:045 PM SGT: Registering service amAuthAuthenticatorOATH.xml
06/08/2024 12:53:25:075 PM SGT: ...Success.
06/08/2024 12:53:25:075 PM SGT: Registering service fmAuthFederation.xml
06/08/2024 12:53:25:157 PM SGT: ...Success.
06/08/2024 12:53:25:157 PM SGT: Registering service fmAuthSAE.xml
06/08/2024 12:53:25:285 PM SGT: ...Success.
06/08/2024 12:53:25:285 PM SGT: Registering service fmCOTConfig.xml
06/08/2024 12:53:25:315 PM SGT: ...Success.
06/08/2024 12:53:25:315 PM SGT: Registering service fmSAML2.xml
06/08/2024 12:53:25:336 PM SGT: ...Success.
06/08/2024 12:53:25:336 PM SGT: Registering service fmSOAPBinding.xml
06/08/2024 12:53:25:367 PM SGT: ...Success.
06/08/2024 12:53:25:368 PM SGT: Registering service fmSAML2SOAPBinding.xml
06/08/2024 12:53:25:389 PM SGT: ...Success.
06/08/2024 12:53:25:389 PM SGT: Registering service fmWSFederation.xml
06/08/2024 12:53:25:412 PM SGT: ...Success.
06/08/2024 12:53:25:413 PM SGT: Registering service fmMultiProtocol.xml
06/08/2024 12:53:25:434 PM SGT: ...Success.
06/08/2024 12:53:25:434 PM SGT: Registering service famFederationCommon.xml
06/08/2024 12:53:25:466 PM SGT: ...Success.
06/08/2024 12:53:25:466 PM SGT: Registering service RestSecurity.xml
06/08/2024 12:53:25:492 PM SGT: ...Success.
06/08/2024 12:53:25:492 PM SGT: Registering service amAuthScripted.xml
06/08/2024 12:53:25:523 PM SGT: ...Success.
06/08/2024 12:53:25:523 PM SGT: Registering service amAuthDeviceIdMatch.xml
06/08/2024 12:53:25:551 PM SGT: ...Success.
06/08/2024 12:53:25:551 PM SGT: Registering service amAuthDeviceIdSave.xml
06/08/2024 12:53:25:576 PM SGT: ...Success.
06/08/2024 12:53:25:576 PM SGT: Registering service restSTS.xml
06/08/2024 12:53:25:621 PM SGT: ...Success.
06/08/2024 12:53:25:621 PM SGT: Registering service soapSTS.xml
06/08/2024 12:53:25:669 PM SGT: ...Success.
06/08/2024 12:53:25:669 PM SGT: Registering service amBaseURL.xml
06/08/2024 12:53:25:694 PM SGT: ...Success.
06/08/2024 12:53:25:694 PM SGT: Registering service amAuthOATH.xml
06/08/2024 12:53:25:827 PM SGT: ...Success.
06/08/2024 12:53:25:827 PM SGT: Registering service amAuthSAML2.xml
06/08/2024 12:53:25:877 PM SGT: ...Success.
06/08/2024 12:53:25:877 PM SGT: Registering service audit.xml
06/08/2024 12:53:26:032 PM SGT: ...Success.
06/08/2024 12:53:26:033 PM SGT: Registering service amAuthAuthenticatorPush.xml
06/08/2024 12:53:26:054 PM SGT: ...Success.
06/08/2024 12:53:26:054 PM SGT: Registering service amAuthAuthenticatorPushRegistration.xml
06/08/2024 12:53:26:088 PM SGT: ...Success.
06/08/2024 12:53:26:088 PM SGT: Registering service amAuthAmster.xml
06/08/2024 12:53:26:237 PM SGT: ...Success.
06/08/2024 12:53:26:238 PM SGT: Registering service amPlugins.xml
06/08/2024 12:53:26:312 PM SGT: ...Success.
06/08/2024 12:53:26:312 PM SGT: Registering service amAuthSocialAuthOAuth2.xml
06/08/2024 12:53:26:346 PM SGT: ...Success.
06/08/2024 12:53:26:346 PM SGT: Registering service amAuthSocialAuthVK.xml
06/08/2024 12:53:26:384 PM SGT: ...Success.
06/08/2024 12:53:26:385 PM SGT: Registering service amAuthSocialAuthWeChat.xml
06/08/2024 12:53:26:424 PM SGT: ...Success.
06/08/2024 12:53:26:424 PM SGT: Registering service amAuthSocialAuthWeChatMobile.xml
06/08/2024 12:53:26:462 PM SGT: ...Success.
06/08/2024 12:53:26:462 PM SGT: Registering service amAuthSocialAuthOpenID.xml
06/08/2024 12:53:26:499 PM SGT: ...Success.
06/08/2024 12:53:26:499 PM SGT: Registering service amAuthSocialAuthInstagram.xml
06/08/2024 12:53:26:571 PM SGT: ...Success.
06/08/2024 12:53:26:571 PM SGT: Registering service amAuthSocialAuthTwitter.xml
06/08/2024 12:53:26:607 PM SGT: ...Success.
06/08/2024 12:53:26:607 PM SGT: Registering service amAuthJwtProofOfPossession.xml
06/08/2024 12:53:26:635 PM SGT: ...Success.
06/08/2024 12:53:26:635 PM SGT: Registering service AuthenticatorWebAuthn.xml
06/08/2024 12:53:26:659 PM SGT: ...Success.
06/08/2024 12:53:26:659 PM SGT: Registering service DeviceId.xml
06/08/2024 12:53:26:682 PM SGT: ...Success.
06/08/2024 12:53:26:683 PM SGT: Registering service amDataStore.xml
06/08/2024 12:53:26:780 PM SGT: ...Success.
06/08/2024 12:53:26:780 PM SGT: Registering service DeviceProfiles.xml
06/08/2024 12:53:26:808 PM SGT: ...Success.
06/08/2024 12:53:26:808 PM SGT: Registering service amAuthAccountActiveCheck.xml
06/08/2024 12:53:26:829 PM SGT: ...Success.
06/08/2024 12:53:30:359 PM SGT: Configuring system.
06/08/2024 12:53:31:797 PM SGT: ...Done
06/08/2024 12:53:31:797 PM SGT: Configuring server instance.
06/08/2024 12:53:31:833 PM SGT: ...Done
06/08/2024 12:53:32:277 PM SGT: Loading Schema /home/aja/openam/opendj_user_schema.ldif
06/08/2024 12:53:32:329 PM SGT: ...Success.
06/08/2024 12:53:32:329 PM SGT: Loading Schema /home/aja/openam/opendj_userinit.ldif
06/08/2024 12:53:32:441 PM SGT: ...Success.
06/08/2024 12:53:32:442 PM SGT: Loading Schema /home/aja/openam/opendj_user_index.ldif
06/08/2024 12:53:32:504 PM SGT: ...Success.
06/08/2024 12:53:32:504 PM SGT: Loading Schema /home/aja/openam/opendj_dashboard.ldif
06/08/2024 12:53:32:533 PM SGT: ...Success.
06/08/2024 12:53:32:533 PM SGT: Loading Schema /home/aja/openam/opendj_deviceprint.ldif
06/08/2024 12:53:32:564 PM SGT: ...Success.
06/08/2024 12:53:32:565 PM SGT: Loading Schema /home/aja/openam/opendj_kba.ldif
06/08/2024 12:53:32:594 PM SGT: ...Success.
06/08/2024 12:53:32:595 PM SGT: Loading Schema /home/aja/openam/opendj_oathdevices.ldif
06/08/2024 12:53:32:618 PM SGT: ...Success.
06/08/2024 12:53:32:618 PM SGT: Loading Schema /home/aja/openam/opendj_pushdevices.ldif
06/08/2024 12:53:32:641 PM SGT: ...Success.
06/08/2024 12:53:32:641 PM SGT: Loading Schema /home/aja/openam/opendj_webauthndevices.ldif
06/08/2024 12:53:32:662 PM SGT: ...Success.
06/08/2024 12:53:32:662 PM SGT: Loading Schema /home/aja/openam/opendj_deviceprofiles.ldif
06/08/2024 12:53:32:684 PM SGT: ...Success.
06/08/2024 12:53:32:684 PM SGT: Loading Schema /home/aja/openam/opendj_bounddevices.ldif
06/08/2024 12:53:32:706 PM SGT: ...Success.
06/08/2024 12:53:32:785 PM SGT: Installing new plugins...
06/08/2024 12:53:37:737 PM SGT: Plugin installation complete.
06/08/2024 12:53:38:080 PM SGT: Setting up monitoring authentication file.
Configuration complete!
[loca] Configuring CTS in the AM
Amster AM Shell (7.4.0 build 56df33df40977206569052a683f5317ade19ee3e, JVM: 11.0.22)
Type ':help' or ':h' for help.
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------
am> :load cts.amster
===> {
    "amconfig.org.forgerock.services.cts.store.common.section": {
        "org.forgerock.services.cts.store.location": "external",
        "org.forgerock.services.cts.store.root.suffix": "ou=famrecords,ou=openam-session,ou=tokens",
        "org.forgerock.services.cts.store.max.connections": "10",
        "org.forgerock.services.cts.store.page.size": "0",
        "org.forgerock.services.cts.store.vlv.page.size": "1000"
    },
    "amconfig.org.forgerock.services.cts.store.external.section": {
        "org.forgerock.services.cts.store.mtls.enabled": false,
        "org.forgerock.services.cts.store.starttls.enabled": false,
        "org.forgerock.services.cts.store.password": null,
        "org.forgerock.services.cts.store.loginid": "uid=openam_cts,ou=admins,ou=famrecords,ou=openam-session,ou=tokens",
        "org.forgerock.services.cts.store.heartbeat": "10",
        "org.forgerock.services.cts.store.ssl.enabled": true,
        "org.forgerock.services.cts.store.directory.name": "ajamidships:1637"
    },
    "_rev": "1791913024",
    "_id": "null/properties/cts"
}
Using CATALINA_BASE:   /home/aja/apps/tomcat
Using CATALINA_HOME:   /home/aja/apps/tomcat
Using CATALINA_TMPDIR: /home/aja/apps/tomcat/temp
Using JRE_HOME:        /usr/lib/jvm/java-11-openjdk-amd64/
Using CLASSPATH:       /home/aja/apps/tomcat/bin/bootstrap.jar:/home/aja/apps/tomcat/bin/tomcat-juli.jar
Using CATALINA_OPTS:    -Xmx2g -XX:MaxMetaspaceSize=256m -Djavax.net.ssl.trustStore=/home/aja/apps/tomcat/dstruststore -Djavax.net.ssl.trustStorePassword=changeit -Djavax.net.ssl.trustStoreType=jks
[loca] Sleeping for 5 seconds waiting for tomcat to stop...
Using CATALINA_BASE:   /home/aja/apps/tomcat
Using CATALINA_HOME:   /home/aja/apps/tomcat
Using CATALINA_TMPDIR: /home/aja/apps/tomcat/temp
Using JRE_HOME:        /usr/lib/jvm/java-11-openjdk-amd64/
Using CLASSPATH:       /home/aja/apps/tomcat/bin/bootstrap.jar:/home/aja/apps/tomcat/bin/tomcat-juli.jar
Using CATALINA_OPTS:    -Xmx2g -XX:MaxMetaspaceSize=256m -Djavax.net.ssl.trustStore=/home/aja/apps/tomcat/dstruststore -Djavax.net.ssl.trustStorePassword=changeit -Djavax.net.ssl.trustStoreType=jks
Tomcat started.
[loca] Sleeping for 5 seconds waiting for tomcat to start...
[loca] Waiting for tomcat to start and load openam....
<<AM HTML>>
[loca] OpenAM launched successfully with external US and CTS
```

## Installing Identity Gateway

For this part, you will be modifying 3 provided files:

- params.sh
- install_ig.sh
- uninstall_ig.sh

This is how your install script output should look like.

```
Using deployment id - AKp1MNvxN34SIFWpe2hUcRxeCbh_1Gg5CBVN1bkVDX_1KwvzX3MJpiFA
Archive:  /home/aja/downloads/IG-2023.11.0.zip
<<extracting IG>>
[loca] Extraction of IG into identity-gateway successful
[loca] Created admin.json with the a new port number of 9090
IG successfully started. Press enter to continue....
[loca] All components successfully installed
[main] INFO  o.f.openig.launcher.Launcher @system - Reading the configuration from /home/aja/apps/ig/config/admin.json
[main] INFO  o.f.openig.launcher.Launcher @system - Identity Gateway 2023.11.0 (branch sustaining/2023.11.x, revision 8fff5a5028fa3c8022b9cef4e4c5424a17be7ef2)
[main] INFO  o.f.openig.launcher.Launcher @system - Environment:
[main] INFO  o.f.openig.launcher.Launcher @system - - instance directory: /home/aja/apps/ig
[main] INFO  o.f.openig.launcher.Launcher @system - - temporary directory: /home/aja/apps/ig/tmp
[main] INFO  o.f.openig.launcher.Launcher @system - - PID file: /home/aja/apps/ig/tmp/ig.pid
[main] INFO  org.forgerock.api.models.Items @system - Asked for Items for annotated type, but type does not have required RequestHandler annotation. No api descriptor will be available for class org.forgerock.monitoring.DropwizardMetricsCollectionResourceProvider
[main] INFO  org.forgerock.api.models.Resource @system - Asked for Resource for annotated type, but type does not have required RequestHandler annotation. No api descriptor will be available for class org.forgerock.monitoring.DropwizardMetricsCollectionResourceProvider
[main] INFO  o.f.openig.launcher.Launcher @system - /home/aja/apps/ig/config/config.json not readable, using default-config.json
[main] INFO  o.f.openig.launcher.Launcher @system - All 20 verticles started in 3546ms on ports : [9090]
```

## Creating a dummy resource we will protect with our new Forgerock stack

1. Create a folder 'treasure' and add 1 file to it 'valuable.txt' with the string "Hello world"
2. Start a python server here with a default port of 8000 with the command `python3 -m http.server`
3. Try to access http://ajamidships:8000/valuable.txt and you should see "hello world"
4. We will now protect this with our new loca stack.

## Setting up an IG Agent and Validation Service in AM

1. Login to the AM Admin Console using amadmin
2. In the Top Level Realm, go to Application > Agents > Identity Gateway and setup an IG Agent with id:ig_agent and password: abcd2468
3. In the top level realm, go to Service > Add a Service > Validation Service and add the url http://ajamidships:8000/*?*
4. Go to Identities and create a new user with the name 'test'
5. Logout of the AM Console and login with 'test' user.

How can you check the entry of 'test' user in UserStore. Enter the ldapsearch command below

```
see us_search.sh
```

How can you check the entry of 'test' user's session token in CTS. Enter the ldapsearch command below

```
see cts_search.sh
```

## Setting up a route in IG

1. In you IG config folder, open admin.json and add "mode":"DEVELOPEMENT" to run the IG in development mode. Restart your IG.
2. Navigate to the URL http://ajamidships:9090/openig/studio and confirm IG Studio opens up
3. Create a new Standard Route called valuable.txt and protect it with the AM service. (https://backstage.forgerock.com/docs/ig/2023.11/studio-guide/structured.html)
4. Deploy the route from the studio by clicking the deploy button

## Test your setup

1. Go to http://ajamidships:9090/valuable.txt (Note, i am going to the IG URL with the proected context path)
2. You should get re-directed to AM to perform a login (If you directly see "Hello world", be sure to logout of AM and clear your cookies)
3. Login and you should get redirected to http://ajamidships:8000/valuable.txt and see "Hello World"

Congatulations! You have now deployed Forgerock locally and used it to protect a resource.
Notice how quick it was to setup Forgerock!

## Using the All scripts

1. Uninstall all components and confirm your apps folder is empty
2. Run the `install_all.sh` script and confirm you can install the entire stack in one shot
3. Test your new setup
4. Run the `uninstall_all.sh` script and confirm you can uninstall the entire stack in one shot

Go forth and play with this and may you get a lot of benefit and joy of testing things out locally!!
