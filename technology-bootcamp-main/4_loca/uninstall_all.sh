#!/bin/bash

# uninstall_all.sh

# source params.sh
source ./params.sh

# uninstall IG
./uninstall_ig.sh

# uninstall AM
./uninstall_am.sh

# uninstall CTS
./uninstall_cts.sh

#uninstall US
./uninstall_us.sh

echo "[loca] All components successfully uninstalled"