#!/bin/bash

# uninstall_us.sh

# reference the params file
source ./params.sh

# stop the US server"
export US_INSTALLATION_PATH="$USERSTORE_DIR"/opendj
echo "stop existing DS first"
"$US_INSTALLATION_PATH"/bin/stop-ds

# delete the userstore directory inside your apps folder
rm -r -f "$USERSTORE_DIR"/*