#!/bin/bash

# install_cts.sh
# Guide: https://backstage.forgerock.com/docs/ds/7.4/getting-started/install.html
# Guide: https://backstage.forgerock.com/docs/ds/7.4/install-guide/profile-am-cts.html


# reference the params file here so all variables are loaded
source ./params.sh

# unzip DS-7.4.0.zip into your cts dir if this dir does not already exist
mkdir -p "$CTS_INSTALLDIR"
export CTS_INSTALLATION_PATH="$CTS_INSTALLDIR"/opendj
if [ -d "$CTS_INSTALLATION_PATH" ]; then
    sh ./uninstall_cts.sh
else
    echo "fresh install"
fi 

unzip -o "$ARTEFACT_DIR"/DirectoryService/DS-7.4.0.zip -d "$CTS_INSTALLDIR" >/dev/null

if [ -e "$CTS_DEPLOYMENT_ID_FILE" ]; then
    echo "use existing deploymentID"
    DEPLOYMENT_ID=$(<"$CTS_DEPLOYMENT_ID_FILE")
else
    echo "using new deploymentID"
    DEPLOYMENT_ID=$("$CTS_INSTALLATION_PATH"/bin/dskeymgr create-deployment-id --deploymentIdPassword "$CTS_DEPLOYMENT_ID_PASSWORD")
    export DEPLOYMENT_ID
    echo "$DEPLOYMENT_ID" > "$CTS_DEPLOYMENT_ID_FILE"
fi


# run the ds setup command using the profile 'am-cts'. You can hard code the port numbers here
# set the result of the setup command ($?) into a variable called install_status
# be sure to change port numbers to avoid clashing with User store
$CTS_INSTALLATION_PATH/setup \
 --serverId first-ds-cts \
 --deploymentId $DEPLOYMENT_ID \
 --deploymentIdPassword "$CTS_PASSWORD" \
 --rootUserDN uid=admin \
 --rootUserPassword "$CTS_PASSWORD" \
 --monitorUserPassword "$CTS_PASSWORD" \
 --hostname cfg.example.com \
 --adminConnectorPort 4443 \
 --ldapPort 1388 \
 --enableStartTls \
 --ldapsPort 1635 \
 --httpsPort 8442 \
 --replicationPort 8988 \
 --profile am-cts \
 --set am-cts/amCtsAdminPassword:"$CTS_PASSWORD" \
 --start \
 --acceptLicense


# check the status of the setup command and store it into a variable(install_status)
 # -- your script here

# check if install_status is 0. If yes, then print a success message and start the ds server. Else, print an error message and quit
# if the server started successfully run the status command. Be sure to specify arguments for hostname, port, identity password and trusting all ssl certificates
$CTS_INSTALLATION_PATH/bin/status \
 --bindDn uid=admin \
 --bindPassword "$CTS_PASSWORD" \
 --hostname cfg.example.com \
 --port 4443 \
 --usePkcs12TrustStore "$CTS_INSTALLATION_PATH"/config/keystore \
 --trustStorePassword:file "$CTS_INSTALLATION_PATH"/config/keystore.pin


