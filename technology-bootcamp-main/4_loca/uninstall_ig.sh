#!/bin/bash

# uninstall_ig.sh

# reference the params file
source ./params.sh

# stop the IG server. Be sure to specify the parent folder where the config is installed as an argument to the stop command"

"$IG_INSTALLDIR"/identity-gateway/bin/stop.sh "$IG_INSTALLDIR"/identity-gateway/ig_config

# delete the IG directory inside your apps folder
rm -r -f "$IG_INSTALLDIR"/*