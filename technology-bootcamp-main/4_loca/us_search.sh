#!/bin/bash
source ./params.sh
export US_INSTALLATION_PATH="$USERSTORE_DIR"/opendj

"$US_INSTALLATION_PATH"/bin/ldapsearch \
--hostname localhost \
--port 1636 \
--bindDn uid=admin \
--bindPassword "$US_PASSWORD" \
--useSsl \
--usePkcs12TrustStore "$US_INSTALLATION_PATH"/config/keystore \
--trustStorePassword:file "$US_INSTALLATION_PATH"/config/keystore.pin \
--baseDN ou=identities \
"uid=test"
