#!/usr/bin/env bash

sleep_seconds="${1}"

if [ -z "${sleep_seconds}" ]; then
  echo "Usage: ./load-test.sh 1"
  exit 1;
fi

while true; do
  curl "http://cube/echo?echo_env_body=HOSTNAME"
  echo ""
  sleep "${sleep_seconds}"
done