# 03 - Configuring Apps

In the previous exercise, we introduced a ConfigMap, which was used to load a configuration file into a Pod to
change its behaviour.

In this exercise, we will introduce some more type of ConfigMaps and other types of configuration:

- Configuration embedded in Deployments
- Secrets

## Create a ConfigMap YAML file

A ConfigMap is a way to define simple data structures and make them available to a running Pod.

They can be made available on the file system

Create a file named `echo-server-config-map.yaml` with the following content:

```yaml
apiVersion: v1
kind: ConfigMap
metadata:
  name: echo-server
data:
  HELLO: echo echo echo echo echo 
```

## Apply the ConfigMap

Use kubectl to apply the ConfigMap configuration to your Kubernetes cluster.

```shell
kubectl apply -f echo-server-config-map.yaml
```

This command will create the ConfigMap in your cluster but that doesn't achieve anything on its own as it's just a
piece of data.

We can get the ConfigMap to prove that its been loaded (using the YAML output flag this time to pull the entire
resource from the database)

```yaml
k get cm echo-server -o yaml
apiVersion: v1
data:
  HELLO: echo echo echo echo echo
kind: ConfigMap
metadata:
  annotations:
    kubectl.kubernetes.io/last-applied-configuration: |
      {"apiVersion":"v1","data":{"HELLO":"echo echo echo echo echo"},"kind":"ConfigMap","metadata":{"annotations":{},"name":"echo-server","namespace":"bootcamp"}}
  creationTimestamp: "2024-07-05T04:55:31Z"
  name: echo-server
  namespace: bootcamp
  resourceVersion: "217633"
  uid: 24735947-2dbc-4bc2-a476-b3d59ad14f47
```

We need to configure our deployment of the Echo Server to pick up that piece of configuration and use it.

## ✅Exercise

First of all, lets prove that is not working yet by trying to see if the echo server can echo that environment
variable when requested:

```shell
curl "http://cube/echo?echo_env_body=HELLO"
```

Nothing will be printed.  Your job is to work out how to link the ConfigMap to the Echo Server deployment you
created in exercise 1, in a file called `echo-server-deployment.yaml`.

Go and read the excellent docs on [kubernetes.io](https://kubernetes.io/) and work out how you can use this
ConfigMap as an environment variable in a Deployment.

Paste your updated `echo-server-deployment.yaml` file below:

```
apiVersion: apps/v1
kind: Deployment
metadata:
  name: echo-server
spec:
  replicas: 1
  selector:
    matchLabels:
      app: echo-server
  template:
    metadata:
      labels:
        app: echo-server
    spec:
      containers:
        - image: ealen/echo-server:latest
          imagePullPolicy: IfNotPresent
          name: echo-server
          ports:
            - containerPort: 80
          env:
            - name: PORT
              value: "80"
            - name: HELLO
              valueFrom:
                configMapKeyRef:
                  name: echo-server
                  key: HELLO
```

Now run the command `curl "http://cube/echo?echo_env_body=HELLO"` and paste the output below:

```
"echo echo echo echo echo"
```

## Inline Configuration Change

The `echo-server-deployment.yaml` already has an inline configuration property:

```yaml
          env:
            - name: PORT
              value: "80"
```

Change the value to 9000 and apply the file.  Now describe what has happened to the deployment.

```
this should be the port pass to the application running in the image , by changing it to 9000, the application should then be running on port 9000
```

And now test the server again, `curl "http://cube/echo?echo_env_body=HELLO"`, do it work and why not?

```
not working, because the echo service is targing port 80 but nothing is running on port 80 in the pod
```

Let's fix the deployment before we move on, so change the PORT value in the deployment file back to 80 and re-apply
the deployment file and test the server again `curl "cube/echo?echo_env_body=HELLO"`.

If the command returns successfully, move on to the next part of the exercise.

## Secrets

Secrets are a special type of ConfigMap and work in a very similar way.  They are also key/value pairs however with
Secrets, the values are base64 encoded (not encrypted).

You might ask, "why are they not encrypted?"   Good question, and the K8s developers decided that it's too hard to do
inside the platform and to leave it up to the platform providers to lock these down.

Some k8s distributions, like EKS on AWS, provide a mechanism that allows the platform operators to configure
encryption of the database that holds the Secrets when they are at rest.

When used by an application, the application team can choose to load encrypted values into a Secret, and have the
application deal with the decryption.

Another option is to leave them unencrypted but heavily restrict the access to the platform in production to lock
these down.

Flexibility seems to be the goal, allowing security to be applied as needed.

Anyway, lets create one.

## Create a Secret

Add a file called `echo-server-secret.yaml` and paste the contents below.

```yaml
apiVersion: v1
kind: Secret
metadata:
  name: echo-server
data:
  HELLO: c2hoaGhoaGhoaCB0b3Agc2VjcmV0Cg==
```

Use kubectl to apply the Secret into your cluster.

```shell
kubectl apply -f echo-server-secret.yaml
```

## ✅Exercise

Now we have a Secret, we want the Echo Server to pick that up instead of the HELLO value in the clear that we
created in the ConfigMap.

Have a look at the Kubernetes docs again, and replace the ConfigMap with the Secret, so that when you make the echo
request to the server, it prints the value of the Secret.

Print the value of the Secret using the command: `curl "cube/echo?echo_env_body=HELLO"`

```
"shhhhhhhhh top secret\n"
```

Now paste your updated Deployment YAML file below to show how you did it:

```
apiVersion: apps/v1
kind: Deployment
metadata:
  name: echo-server
spec:
  replicas: 1
  selector:
    matchLabels:
      app: echo-server
  template:
    metadata:
      labels:
        app: echo-server
    spec:
      containers:
        - image: ealen/echo-server:latest
          imagePullPolicy: IfNotPresent
          name: echo-server
          ports:
            - containerPort: 80
          env:
            - name: PORT
              value: "80"
            - name: HELLO
              valueFrom:
                secretKeyRef:
                  name: echo-server
                  key: HELLO
```

## Conclusion

You have successfully configured the deployment of an application in 3 different ways and seen how that has modified
the behaviour of the application and broke it (intentionally) in one instance.
