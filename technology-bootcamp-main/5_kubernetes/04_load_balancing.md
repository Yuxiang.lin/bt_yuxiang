# 04 - Load balancing

It's all well and good to have an application running inside a K8s cluster, but a huge benefit of K8s is the ability
scale your application out to multiple instances, so you can deal with higher traffic demands.

To be able to scale your application, you need to have a deployment pattern that allows you to manage multiple
copies of the application and to direct traffic to them automatically when new instances are added.

Luckily, you already have all of those things set up!!

Let's get started on using them.

## Create a Test Harness

Firstly, we want to be able to test the application with a single instance, then scale it up and see the change in
output.

Create a script on your machine with the following content called `load-test.sh`, making sure to set the executable
bit with `chmod +x load-test.sh`:

```shell
#!/usr/bin/env bash

sleep_seconds="${1}"

if [ -z "${sleep_seconds}" ]; then
  echo "Usage: ./load-test.sh 1"
  exit 1;
fi

while true; do
  curl "http://cube/echo?echo_env_body=HOSTNAME"
  echo ""
  sleep "${sleep_seconds}"
done
```

And in a terminal, start running it against your environment like this:

```shell
./load-test.sh 1
```

Now we have a test hitting the server which is responding with the hostname of the server (also the name of the Pod),
like.


```shell
# ./load-test.sh 1
"echo-server-7c46dc569b-k6qgn"
"echo-server-7c46dc569b-k6qgn"
"echo-server-7c46dc569b-k6qgn"
"echo-server-7c46dc569b-k6qgn"
"echo-server-7c46dc569b-k6qgn"
```

## ✅Exercise

The host name/Pod name is made up of some parts.  Use Google and the K8s docs, to describe the parts that make up the
name of the Pod.

Hint: Pods created in different ways have different names, here we used a K8s `Deployment`.  We will cover more
deployment types later.

```
replicaSet's name = <deployment's name>+"-"+`<unique 9 character pod tempate hash> `
pod's name = replicateSet's name + "-"+<5 random hexadecimal characters>
```

## Scaling Out

Scaling out, also known `horizontal scaling`, is an activity where you add more instances of the deployment to allow
your application to deal with more traffic.

We aren't going to overload the server as it is limited to the resources on your laptop/desktop, but hopefully you get
the idea.

With your terminal, open another and make sure you can see both at the same time.  With the `load-test.sh` still
running again the deployment, run the following command in the other terminal window:

```shell
kubectl scale --replicas=3 deployment/echo-server
```

Let's look at the impact of this scaling out.  When I scaled the deployment, almost immediately I started to see more
hostnames being printed by the script, showing that there are now 2 extra hosts getting requests and responding with
their unique hostname.

```shell
"echo-server-7c46dc569b-k6qgn" # Original pod 1
"echo-server-7c46dc569b-zrlzn" # New pod 2
"echo-server-7c46dc569b-xxhtw" # New pod 3
"echo-server-7c46dc569b-k6qgn" # Original pod 1
"echo-server-7c46dc569b-zrlzn" # New pod 2
"echo-server-7c46dc569b-xxhtw" # New pod 3
```

You might notice a pattern to the responses, easy to see with the added comments above.  They are (roughly) in
round-robin order.

You will see variation in this as traffic management will depend on a few variables, but we now have 3 instances of
our application now getting roughly evenly distributed traffic sent to them.

## ✅Exercise

Scaling can be handled in more ways in Kubernetes.  All ultimately modify the Deployment resource that we
created earlier.  We did one above, show another way to scale the replica count to 3 for the `echo-server` Deployment:

```
just modify the deployment yaml's replica from 1 to 3? not sure if this is what is asking
```

## Scaling Up

Scaling up (or Vertical Scaling) is another useful way to have your application deal with more requests.  This is
more complex, as you need to make more memory and CPU available to the application pods, but then you also need to
tune the internals of the application to be able to use more of the available compute.

For a Java app like Ping or ForgeRock, this could mean increasing the JVM size and changing the internal memory heap
size allocations.  Also increasing HTTP handler counts, thread counts and connection pools within the application or
the server like Tomcat/Jetty running it.

This we won't go in to here, maybe a topic for a future bootcamp.

So as you can see, scaling out is much easier to do than scaling up.  But scaling out won't always work, it depends
on your application.  For large Java apps like FR and Ping, you usually have to use both techniques to get the
application throughput you want.

# Scaling Out Mechanics

While Scaling Up is hard work for you, Scaling Out is handled by the Kubernetes cluster.

When you add a new Pod into your Deployment by scaling, a new process (the Pod) starts running.  It gets an IP
address, and is ready to get traffic.

But how does traffic get to your Pod?   The `curl` request in the `load-test.sh` script is the client, the Pod is
the server, but how does the traffic actually reach these new Pods?

Before scaling:

```
curl --> ? --> pod
```

After scaling:

```
           --> pod
curl --> ? --> pod
           --> pod
```

What is this `?` thing that works the magic to direct traffic to all the new Pods?  Hint - you created it earlier ;)

```
Nginx -> echo service(cluster ip) -> load balance to pods
```

## ✅Exercise

Just tidy things up for now.  Scale back to 1 and stop your load test script.

## Conclusion

You have learned how to scale up to add more instances of your application and also about one of the mechanisms that
Kubernetes uses to route traffic to those new instances.  Nice!
