# 02 - Expose App With Nginx

In this follow-up exercise, we will deploy an open source load balancer (Nginx) and configure it to route traffic to
this our deployment. Here are the steps:

## Create a Deployment YAML file

A Deployment ensures that a specified number of replicas of an application are running at any given time. In this case
the Deployment is the Nginx Load Balancer.

Create a file named `nginx-deployment.yaml` with the following content:

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: nginx
spec:
  replicas: 1
  selector:
    matchLabels:
      app: nginx
  template:
    metadata:
      labels:
        app: nginx
    spec:
      containers:
      - name: nginx
        image: nginx:latest
        ports:
        - containerPort: 80
```

## Apply the Deployment

Use kubectl to apply the deployment configuration to your Kubernetes cluster.

```shell
kubectl apply -f nginx-deployment.yaml
```

This command will create a deployment with one replica of the Nginx container. A replica is an identical instance of a
Pod to other Pods managed by the deployment.

In fact, a deployment contains a ReplicaSet that defines the configuration and number of replicas in a Deployment.

## ✅Exercise

Look in the docs/Google for the command to show the information about the `replicasets` in the namespace.  Use it to
provide the output below:

```
kubectl get rs

NAME                     DESIRED   CURRENT   READY   AGE
echo-server-768fdbbd5f   1         1         1       62m
nginx-7c79c4bf97         1         1         1       78s

** hi paul, if you are reading, this exercise you have a missing step below, after creating the config map yaml, need to apply it, usually you put the apply step but you did not mention for config map, i assume you miss out not on purpose for people to find out
```

## Verify the Deployment

Check the status of the deployment to ensure it was created successfully.

```shell
kubectl get deployments
```

## ✅Exercise

Get the pods **in all namespaces** in your cluster.  Hint `--help` is your friend to work this out.

Paste the output of your command below:

```
k get pods --all-namespaces

NAMESPACE     NAME                                     READY   STATUS    RESTARTS        AGE
bootcamp      echo-server-768fdbbd5f-hz7v9             1/1     Running   0               29m
bootcamp      nginx-7c79c4bf97-v6n7h                   1/1     Running   0               3m18s
kube-system   coredns-76f75df574-6sxmj                 1/1     Running   4 (14d ago)     29d
kube-system   coredns-76f75df574-bzj4n                 1/1     Running   4 (14d ago)     29d
kube-system   etcd-docker-desktop                      1/1     Running   7 (14d ago)     29d
kube-system   kube-apiserver-docker-desktop            1/1     Running   7 (3h21m ago)   29d
kube-system   kube-controller-manager-docker-desktop   1/1     Running   7 (14d ago)     29d
kube-system   kube-proxy-ksbh8                         1/1     Running   4 (14d ago)     29d
kube-system   kube-scheduler-docker-desktop            1/1     Running   8 (14d ago)     29d
kube-system   storage-provisioner                      1/1     Running   8 (3h20m ago)   29d
kube-system   vpnkit-controller                        1/1     Running   4 (14d ago)     29d

```

## Create a Service YAML file

A Service exposes the deployment to the network. Create a file named `nginx-load-balancer.yaml` with the following content:

```yaml
apiVersion: v1
kind: Service
metadata:
  name: nginx-load-balancer
spec:
  selector:
    app: nginx
  ports:
    - protocol: TCP
      port: 80
      targetPort: 80
  type: LoadBalancer
```

## Apply the Service

Use kubectl to apply the service configuration.

```shell
kubectl apply -f nginx-load-balancer.yaml
```

## Verify the Service

Check the status of the service to ensure it was created and get the external IP.

```shell
kubectl get services
```

You should see output similar to:

```shell
kubectl get svc
NAME                        TYPE           CLUSTER-IP       EXTERNAL-IP   PORT(S)        AGE
echo-server                 ClusterIP      10.110.222.46    <none>        8080/TCP       19m
echo-server-load-balancer   LoadBalancer   10.111.184.247   localhost     80:30592/TCP   10m
nginx-load-balancer         LoadBalancer   10.102.208.140   <pending>     80:32706/TCP   9s
```

There is a problem here though.

As there are 2 LoadBalancer type services in the cluster, and because this is a cluster on a single machine, only one
LoadBalancer Service will get assigned the localhost external ip as there is only one.

To fix. this, just delete the echo-server LB. We won’t need it with Nginx running.

```shell
kubectl delete svc echo-server-load-balancer
```

After a few seconds, the nginx-load-balancer will be given the localhost external ip.

```shell
kubectl get svc
NAME                  TYPE           CLUSTER-IP       EXTERNAL-IP   PORT(S)        AGE
echo-server           ClusterIP      10.110.222.46    <none>        8080/TCP       21m
nginx-load-balancer   LoadBalancer   10.102.208.140   localhost     80:32706/TCP   109s
```

The reason we want to do this is that Nginx can allow us to expose many services running within the cluster, instead of
just one like a LoadBalancer Service can do.

## Access the Application

If using Docker Desktop, the default local address if kubernetes.docker.internal, so open a browser at the following
HTTP page, and you should see the NGinx welcome page.

http://kubernetes.docker.internal/

You can also use the following aliases as they are set up in the docker-compose.yml file in The Cube repo:

- http://cube/
- http://cube.midships.io/

Nice work on getting this far. You’ve now got a working Load balancer and a running microservice. Next we need to
connect them to make the application accessible outside the cluster without any port forwarding.

## Update Nginx to Route Traffic

Create a ConfigMap containing Nginx Routing rules by creating a file called `nginx-config-map.yaml` with the following
content.

```yaml
apiVersion: v1
kind: ConfigMap
metadata:
  name: nginx
data:
  echo-server.conf: |
    server {
        listen 80;

        location /echo {
            proxy_pass http://echo-server:8080/;
            proxy_set_header Host $host;
            proxy_set_header X-Real-IP $remote_addr;
            proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
            proxy_set_header X-Forwarded-Proto $scheme;
        }
    }
```

Update the existing Nginx configuration to route traffic to the "echo-server" microservice. Update the Nginx
deployment YAML file `nginx-deployment.yaml` with the updated configuration below to mount the data element in the
ConfigMap into the Pod as a file:

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: nginx
spec:
  replicas: 1
  selector:
    matchLabels:
      app: nginx
  template:
    metadata:
      labels:
        app: nginx
    spec:
      containers:
        - name: nginx
          image: nginx:latest
          ports:
            - containerPort: 80
          volumeMounts:
            - name: nginx-config
              mountPath: /etc/nginx/conf.d
      volumes:
        - name: nginx-config
          configMap:
            name: nginx
```

Apply the updated Nginx deployment configuration:

```shell
kubectl apply -f nginx-deployment.yaml
```

There is a lot in the above, and we won't cover it all.  But the main parts are documented here:

- Deployment
  - **spec.containers[0].image** - Run the latest nginx container from DockerHub
  - **spec.containers[0].ports[0].containerPort** - Expose it on port 80
  - **spec.containers[0].volumeMounts[0]** - Create a local volume with a mount path of `/etc/nginx/conf.d` and mount
    the volume with name `nginx-config` onto it.
  - **spec.volumes[0]** - Define a volume called `nginx-config` that contains the contents of the referenced ConfigMap.
    This will result in the keys in the ConfigMap under `data` to be mounted as files inside the container - cool huh?
- ConfigMap
  - **data.echo-server.conf** - the Nginx configuration under here will be created as a file at
    `/etc/nginx/conf.d/echo-server.conf`. The configuration in this file basically tells Nginx to route all requests
    to `/echo', to the internal ClusterIP Service which forwards the requests to the Pods.

## Verify the Setup

Verify that all deployments are running correctly:

```shell
kubectl get deployments -n bootcamp
NAME          READY   UP-TO-DATE   AVAILABLE   AGE
echo-server   1/1     1            1           12h
nginx         1/1     1            1           11m
```

## Access the Microservice

To access the "echo-server" microservice through Nginx, open a web browser and navigate to:

http://kubernetes.docker.internal/echo for the "echo-server" microservice.

## Debugging network challenges

If you are having issues with networking not working, you can use this tool to run a pod then connect to its shell
inside the cluster and diagnose using standard network debugging tools.  Will cover this in more detail in a later
bootcamp.

```
docker pull nicolaka/netshoot
kubectl run netshoot-shell --rm --stdin --tty --overrides='{"spec": {"hostNetwork": true}}'  --image nicolaka/netshoot
```

## Conclusion

You have successfully deployed a basic microservice application in a different exercise and now you have configured
Nginx to route traffic to that deployment.

This exercise demonstrated how to manage traffic ingress into your deployed microservices.
