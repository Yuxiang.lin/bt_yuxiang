# 09 - Observability

In this chapter, you are going to discover a new deployment pattern, the Operator, and use it to install a monitoring
tool called Prometheus.

Prometheus is a popular project, which has defined a metrics format that is widely adopted and also contains an 
open-source tool for running a time series database for scraping metrics from applications that support the 
prometheus metrics format. 

You are also going to install some other tooling to allow you to visualise the metrics and logs from your entire 
cluster.

## Observability Challenges in Kubernetes

With the incredible power of the Kubernetes platform, there are certain challenges faced by users around how to 
support applications running inside it.  

As you've seen, Pods can be killed any time, and their logs would be lost with them.  Their metrics are also lost 
when they are killed, so without externalising these metrics into a collector of some kind, supporting applications 
and performing root cause analysis would be impossible.

So applications in K8s have to be treated differently to applications running inside VMs.  We have to take their 
metrics and logs and push them to an aggregator that we can use to search, query and chart the logs and metrics and also 
alert on problems based on those data.

## Metrics

Metrics in the Prometheus format are already being exposed in your springbootcamp application through the magic of 
[Spring Boot's Actuator framework](https://docs.spring.io/spring-boot/reference/actuator/index.html).

Make this curl request into the cluster to see the available stats: 

```shell
curl https://cube.midships.io/bootcamp/actuator/prometheus
```

This response is a reflection of all metrics in prometheus format, at the point in time that it generated the 
response to your request.

This is not super useful on its own, as you want to be able to see how metrics change over time in response to other 
situations, like increased traffic, complex request processing etc.

This is where the time series database comes into play.

Prometheus can be configured with or auto-discover metrics APIs of the applications inside the cluster it is 
running in, to go and then pull the metrics and collate them.

It then provides a basic user interface to allow you to view the metrics over time.

Let's get it deployed and see it in action!

## Deploying and Configuring Prometheus

Run this command which will get the latest version from the GitHub API, download the bundle.yaml and use the kubectl 
create command to deploy all the required resources into your cluster: 

```shell
LATEST=$(curl -s https://api.github.com/repos/prometheus-operator/prometheus-operator/releases/latest | jq -cr .tag_name)
curl -sL https://github.com/prometheus-operator/prometheus-operator/releases/download/${LATEST}/bundle.yaml | kubectl create -f -
```

We have just installed the Prometheus Operator.  An Operator is just software that manages other software.

Now we need to use it to run Prometheus.  Firstly, we will set up some Role Based Access Controls rules for the 
Prometheus server to be able to do its job.

Paste the YAML below into a file called `prometheus.yaml` and apply it into your cluster.  Here's what is does:
1. Create a new Namespace called `prometheus`
2. Create a `ServiceAccount` in the prometheus namespace
3. Create a new `ClusterRole` and bind that `ClusterRole` to the prometheus `ServiceAccount` using a `ClusterRoleBinding`
4. Create a deployment of the Prometheus server as a StatefulSet in the prometheus namespace which can monitor Pods 
   and Services in any namespace
5. Create a `Service` for the prometheus STS so that we can access it 

```yaml
apiVersion: v1
kind: Namespace
metadata:
  name: prometheus
---
apiVersion: v1
kind: ServiceAccount
metadata:
  name: prometheus
  namespace: prometheus 
---
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRole
metadata:
  name: prometheus
rules:
  - apiGroups: [""]
    resources:
      - nodes
      - nodes/metrics
      - services
      - endpoints
      - pods
    verbs: ["get", "list", "watch"]
  - apiGroups: [""]
    resources:
      - configmaps
    verbs: ["get"]
  - apiGroups:
      - discovery.k8s.io
    resources:
      - endpointslices
    verbs: ["get", "list", "watch"]
  - apiGroups:
      - networking.k8s.io
    resources:
      - ingresses
    verbs: ["get", "list", "watch"]
  - nonResourceURLs: ["/metrics"]
    verbs: ["get"]
---
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRoleBinding
metadata:
  name: prometheus
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: prometheus
subjects:
  - kind: ServiceAccount
    name: prometheus
    namespace: prometheus
---
apiVersion: monitoring.coreos.com/v1
kind: Prometheus
metadata:
  name: prometheus
  namespace: prometheus
spec:
  serviceAccountName: prometheus
  podMonitorNamespaceSelector: {}
  podMonitorSelector:
    matchLabels:
      prometheus: enabled
  serviceMonitorNamespaceSelector: {}
  serviceMonitorSelector:
    matchLabels:
      prometheus: enabled
  resources:
    requests:
      memory: 400Mi
  enableAdminAPI: false
  externalUrl: https://cube.midships.io/prometheus
  routePrefix : prometheus/
---
apiVersion: v1
kind: Service
metadata:
  name: prometheus
  namespace: prometheus
spec:
  type: ClusterIP
  ports:
    - name: web
      port: 9090
      protocol: TCP
  selector:
    prometheus: prometheus
```

Now wait until the prometheus application is running using `kubectl get pods -n prometheus -w` and then create an 
`Ingress` for it, so you can access the Prometheus UI from the browser on your host machine:

```yaml
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: prometheus
  namespace: prometheus
  annotations:
    nginx.ingress.kubernetes.io/use-regex: "true"
    cert-manager.io/cluster-issuer: "selfsigned-ca-issuer"
    cert-manager.io/common-name: "cube.midships.io"
spec:
  ingressClassName: nginx
  tls:
    - hosts:
        - cube.midships.io
      secretName: prometheus-certificate
  rules:
    - host: cube.midships.io
      http:
        paths:
          - path: /prometheus.*
            pathType: ImplementationSpecific
            backend:
              service:
                name: prometheus
                port:
                  number: 9090
```

Go to the browser on your host machine and load the address: https://cube.midships.io/prometheus/graph and you will 
see the Prometheus query browser. 

Now have a look at the page https://cube.midships.io/prometheus/targets?search=.  This shows you all the 'targets' 
that the Prometheus server knows about, which is none.  To configure something to be monitored, we need to create a 
`ServiceMonitor` which allows the Prometheus Operator to configure the Prometheus Server with a new Service to 
monitor, so that the server knows where to pull data from.

Create a file called `springbootcamp-service-monitor.yaml` and put the following YAML into it and apply it:

```yaml
apiVersion: monitoring.coreos.com/v1
kind: ServiceMonitor                         # New resource type defined by a Custom Resource Definition
metadata:
  name: springbootcamp
  namespace: bootcamp                        # Place it in the bootcamp namespace
  labels:
    prometheus: enabled                      # Must have a label of prometheus:enabled to match configuration we added in prometheus.yaml 
spec:
  endpoints:
    - interval: 15s                          # How often to pull metrics
      path: /bootcamp/actuator/prometheus    # Path to pull metrics from 
      targetPort: 8080                       # Port on the Service (if yours isn't 8080 then change this)
      scheme: http                           # Should be HTTP or HTTPS
  selector:
    matchLabels:
      prometheus: enabled                    # IMPORTANT - this must match a Label on your springbootcamp Service
```

This ServiceMonitor uses Label Selectors to determine if it should monitor a Service.  

---
If you want more information on Labels and Selectors, the check-out this video on the Pluralsight course  [Managing the 
Kubernetes API Server and Pods](https://app.pluralsight.com/ilx/video-courses/a73f33c6-2241-4f4b-84cb-84f74cdd4569/74ff9a8d-1dd6-462d-89d7-32499889ffed/95ac2042-39c4-48e0-a928-3695522e209d).
---

The ServiceMonitor is configured above to look for Services with a label of `prometheus=enabled`.  To make sure of 
this, run the following command, it will do nothing if the label is already there (adjust the service name if you 
named yours differently):

```shell
kubectl label service springbootcamp prometheus=enabled
```

Now you should see something in the Targets page like below.

![Prometheus Targets](assets/prometheus-targets.png)

And if you go to the Graph page, you can now view graphs of all metrics for all monitored Services in your cluster!

![Prometheus Graph](assets/prometheus-graph.png)

## ✅Exercise

Prometheus graph capability is not it's best feature.  That's why everyone will install Grafana alongside Prometheus 
to provide great dashboards and charts.

Your challenge is to install Grafana following 
[this guide](https://grafana.com/docs/grafana/latest/setup-grafana/installation/kubernetes/)

If you get completely stuck, reach out for help, and we can provide the solution.  But do give it a good go.  

**Hints:**
1. Create it in a new namespace called `grafana`
2. Put all resources for the deployment in a YAML file and use `kubectl apply -f <filename>` to develop the installation
3. Where the installation guide asks to install a LoadBalancer service, this won't work as you already have Nginx 
   installed.  Instead, change the Service type to ClusterIP and create an Ingress too (copy and adapt the Prometheus 
   one), making sure the ports match.
4. Grafana needs to be configured to know it's behind a reverse proxy (Nginx) too, so add the following snippet 
   to your deployment to properly configure it: 
   ```yaml
    env:
      - name: GF_SERVER_DOMAIN
        value: cube.midships.io
      - name: GF_SERVER_ROOT_URL
        value: https://cube.midships.io/grafana/
      - name: GF_SERVER_SERVE_FROM_SUB_PATH
        value: "true"
   ```
   Also see this: https://grafana.com/tutorials/run-grafana-behind-a-proxy/

You can test this all by going to https://cube.midships.io/grafana/login and authenticating with `admin` as the 
default username and password.

You will need to connect it up to the Prometheus server as a data source, you should use the following address in 
the admin URL: http://prometheus-prometheus-0.prometheus-operated.prometheus.svc.cluster.local:9090

This is the internal address for the Prometheus server that is only available inside the K8s cluster.  Use this 
instead of the Nginx address via cube.midships.io as that will have an invalid certificate and Grafana won't connect 
to it.

![Grafana Prometheus Datasource](assets/grafana-prometheus-datasource.png)

From there, have a play around and create a Dashboard like this example:

![Grafana Dashboard](assets/grafana-dashboard.png)

Also, you can view an out-of-the-box metrics explorer using your Prometheus data source right in Grafana, with no 
additional configuration!

https://cube.midships.io/grafana/explore/metrics/trail

![Grafana Metrics](assets/grafana-metrics.png)

## Log Aggregation

You might have noticed from earlier content that looking at logs isn't straight forward.  All you get out of the box 
is the ability to stream logs from a Pod or a set of Pods using the `kubectl logs` command.  

This is fine for simple debugging cases, but doesn't work for large datasets or if the server is under high load. 
You can't query or chart them over time.  And if a deployment happens while you are looking at logs, they will be 
cut off, and you have to go and look in the new Pod(s).

All in all, it's not great.  So we need to aggregate our logs and luckily, Grafana is able to display, query and 
chart our Pod logs, we just have to aggregate them first.

Enter [Loki](https://github.com/grafana/loki/).  Loki is a Log Aggregation tool, like Elasticsearch, or Splunk or 
SumoLogic, or many others.  Loki is good just because its fast, free and natively integrates with Grafana which we 
already have installed. 

### Installing Loki

⚠️ Firstly, if you haven't got Grafana working from the above exercise, stop and get that going.  If stuck, ask for 
help.  You must have Grafana running to complete this chapter.

Run the following commands to create a namespace and get the Grafana Helm charts into your local system:

```shell
kubectl create namespace loki # IMPORTANT - make sure you use this namespace name 
helm repo add grafana https://grafana.github.io/helm-charts
helm repo update
```

Now we are going to create a [Helm Values](https://helm.sh/docs/chart_best_practices/values/) file.  This file allows you to specify configuration options into a 
Helm Chart.  A Helm Chart is a template that contains all the Kubernetes resource definitions to run a thing. 

Create a file called `loki-values.yaml` and paste the following content:

```yaml
deploymentMode: SingleBinary
loki:
  auth_enabled: false # Not production ready
  commonConfig:
    replication_factor: 1 # Must be one to work in a simple monolithic deployment - DO NOT ADJUST
  storage:
    type: 'filesystem'
  schemaConfig:
    configs:
    - from: "2024-01-01"
      store: tsdb
      index:
        prefix: loki_index_
        period: 24h
      object_store: filesystem
      schema: v13
singleBinary:
  replicas: 1
read:
  replicas: 0
backend:
  replicas: 0
write:
  replicas: 0
```

Then run the Helm install command:

```shell
helm upgrade --install --values loki-values.yaml loki --namespace=loki grafana/loki
```

When Loki is up and running (`kubectl get pods -n loki -w`), we need to add it as a data source in the Grafana UI.  
Just like you did for Prometheus, create a data source and give the Loki URL as the following value:

`http://loki-gateway.loki.svc.cluster.local/`

One final step is to add a tool for scraping logs from everything inside the cluster, and pushing it into the log 
aggregator.  Luckily, Grafana have also created a tool called Alloy, that does just that.

### Installing Alloy 

Firstly, create a file called `alloy-values.yaml` and put the following content into it (if you installed Loki into 
a namespace with a name other than 'loki', you will need to update the value below):

```yaml
alloy:
  configMap:
    content: |-
      // Write your Alloy config here:
      logging {
        level = "info"
        format = "logfmt"
      }
      loki.write "default" {
          endpoint {
          url = "http://loki-gateway.loki.svc/loki/api/v1/push"
        }
      }
      
      // discovery.kubernetes allows you to find scrape targets from Kubernetes resources.
      // It watches cluster state and ensures targets are continually synced with what is currently running in your cluster.
      discovery.kubernetes "pod" {
        role = "pod"
      }

      // discovery.relabel rewrites the label set of the input targets by applying one or more relabeling rules.
      // If no rules are defined, then the input targets are exported as-is.
      discovery.relabel "pod_logs" {
        targets = discovery.kubernetes.pod.targets

        // Label creation - "namespace" field from "__meta_kubernetes_namespace"
        rule {
          source_labels = ["__meta_kubernetes_namespace"]
          action = "replace"
          target_label = "namespace"
        }

        // Label creation - "pod" field from "__meta_kubernetes_pod_name"
        rule {
          source_labels = ["__meta_kubernetes_pod_name"]
          action = "replace"
          target_label = "pod"
        }

        // Label creation - "container" field from "__meta_kubernetes_pod_container_name"
        rule {
          source_labels = ["__meta_kubernetes_pod_container_name"]
          action = "replace"
          target_label = "container"
        }

        // Label creation -  "app" field from "__meta_kubernetes_pod_label_app_kubernetes_io_name"
        rule {
          source_labels = ["__meta_kubernetes_pod_label_app_kubernetes_io_name"]
          action = "replace"
          target_label = "app"
        }

        // Label creation -  "job" field from "__meta_kubernetes_namespace" and "__meta_kubernetes_pod_container_name"
        // Concatenate values __meta_kubernetes_namespace/__meta_kubernetes_pod_container_name
        rule {
          source_labels = ["__meta_kubernetes_namespace", "__meta_kubernetes_pod_container_name"]
          action = "replace"
          target_label = "job"
          separator = "/"
          replacement = "$1"
        }

        // Label creation - "container" field from "__meta_kubernetes_pod_uid" and "__meta_kubernetes_pod_container_name"
        // Concatenate values __meta_kubernetes_pod_uid/__meta_kubernetes_pod_container_name.log
        rule {
          source_labels = ["__meta_kubernetes_pod_uid", "__meta_kubernetes_pod_container_name"]
          action = "replace"
          target_label = "__path__"
          separator = "/"
          replacement = "/var/log/pods/*$1/*.log"
        }

        // Label creation -  "container_runtime" field from "__meta_kubernetes_pod_container_id"
        rule {
          source_labels = ["__meta_kubernetes_pod_container_id"]
          action = "replace"
          target_label = "container_runtime"
          regex = "^(\\S+):\\/\\/.+$"
          replacement = "$1"
        }
      }

      // loki.source.kubernetes tails logs from Kubernetes containers using the Kubernetes API.
      loki.source.kubernetes "pod_logs" {
        targets    = discovery.relabel.pod_logs.output
        forward_to = [loki.process.pod_logs.receiver]
      }

      // loki.process receives log entries from other Loki components, applies one or more processing stages,
      // and forwards the results to the list of receivers in the component’s arguments.
      loki.process "pod_logs" {
        stage.static_labels {
            values = {
              cluster = "docker-desktop",
            }
        }

        forward_to = [loki.write.default.receiver]
      }
      
      // loki.source.kubernetes_events tails events from the Kubernetes API and converts them
      // into log lines to forward to other Loki components.
      loki.source.kubernetes_events "cluster_events" {
        job_name   = "integrations/kubernetes/eventhandler"
        log_format = "logfmt"
        forward_to = [
          loki.process.cluster_events.receiver,
        ]
      }

      // loki.process receives log entries from other loki components, applies one or more processing stages,
      // and forwards the results to the list of receivers in the component’s arguments.
      loki.process "cluster_events" {
        forward_to = [loki.write.default.receiver]

        stage.static_labels {
          values = {
            cluster = "docker-desktop",
          }
        }

        stage.labels {
          values = {
            kubernetes_cluster_events = "job",
          }
        }
      }
```

Now install the helm chart using the values file.

```shell
helm upgrade --install --values alloy-values.yaml alloy --namespace=grafana grafana/alloy
```

When you log into the Grafana UI, you can now see all logs for all Pods in your cluster!!

![Grafana Logs](assets/grafana-logs.png)

# Conclusion

Another meaty chapter completed.  Congratulations!  At this point, you know have a wealth of knowledge about 
Kubernetes applications and how to deploy, expose and monitor them! 

Even more than that, you now have a working local cluster that you can use for all sorts of local development :)
