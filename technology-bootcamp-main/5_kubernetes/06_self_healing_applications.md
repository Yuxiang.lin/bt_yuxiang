# 06 - Self-healing Applications

One of the best things about Kubernetes is that it manages the life cycle of your applications.  When properly
configured, the platform will kill unhealthy instances of your application and replace them with new ones.

This self-healing capability is at the core of the Kubernetes way of designing and developing applications.  With
K8s managing the applications in this way, operations become easier as operating teams don't need to manually
triage and deal with unhealthy instances, this is handled automatically.

The important point above though is that the applications, to be properly managed, have to be _properly configured_.

This is one of the hardest concepts in the platform that people often struggle to remember how it works, understand the
impact of the different configurations and know how to tune these configurations correctly for their applications.

Hopefully this hands on lab will help you, and you can refer back to this when working on probes in the future.

## How Pods Are Made

The definition of a StatefulSet (or anything in K8s really) is stored in a highly available, distributed Key/Value
store called `etcd`.  This is the storage mechanism for all K8s platform and application configuration.

So when you use `kubectl` to  request a new STS to be created, a record is stored in `etcd` with your definition of
what you want.  The `kubectl` command line then exits as it has done its job.

Kubernetes internally uses the eventually consistent paradigm implemented using a number of controllers that watch
for state changes in the `etcd` data store.  When you create your STS, the controller responsible for dealing with
them, the **StatefulSet Controller**, sees that a new one is required and makes a `PodSpec` for each of the defined
number of Pods you requested.

These `PodSpec`(s) are passed to the Kubernetes **Scheduler** which then is responsible for determining where to run
the Pod defined by that `PodSpec` - the pod enters the _Pending_ state.

The Scheduler looks at the available compute on all the worker nodes in the cluster, and that which is requested by
you in your STS definition to decide which worker node to run your Pod on.  It also looks at other aspects of scheduling
like dedicated nodes for some workloads, or affinity rules, or topology constraints, but these are not covered here.

Once the scheduler has identified a spot on a node for your Pod, your Pod will be started, it will enter the
_Container Creating_ state, then the _Running_ state.

Later, if your Pod fails or is killed, the StatefulSet Controller detects this, sees that the number of Pods does not
equal the desired number of Pods, makes a new `PodSpec` and passes it to Scheduler and a new Pod is born.

Now we will create and kill some Pods to illustrate this capability.

## ✅Exercise

---

**NOTE:**

When you create a StatefulSet or a Deployment, using the `kubectl` command line tool, or Helm or whatever,
ultimately you are just making an API call to the K8s API Server.

When you make a `kubectl` command to modify an STS or Deployment, you are actually making a GET request to the K8s API
Server for the current STS specification, comparing it with the locally rendered version and then making a PATCH
request to modify the resource on the server.

You can see this for yourself by setting the verbosity flag to a high value when running kubectl commands, this is
handy if you want to know how to call a particular API directly, e.g.

```shell
kubectl get pods --v=9
```

---

OK, you already have a StatefulSet YAML file from Chapter 5.  If that Echo Server StatefulSet is not running, use the
`kubectl` binary to create it.

Once you have those 2 Pods in a _Running_ state, use the following command to delete a Pod:

```shell
k delete pod es-1
```

Describe what you see, what state did the Pod enter after you ran the command?  What happened, in your own words, to
bring the Pod back?

```
es-1                           1/1     Terminating   0          37s
es-1                           0/1     Terminating   0          37s
es-1                           0/1     Terminating   0          38s
es-1                           0/1     Terminating   0          38s
es-1                           0/1     Terminating   0          38s
es-1                           0/1     Pending       0          0s
es-1                           0/1     Pending       0          0s
es-1                           0/1     ContainerCreating   0          0s
es-1                           1/1     Running             0          4s

The behaviour looks different for deployment when it come to stateful set. For stateful set the same pod name is being re-created as per the statefulset yml specification to maintain 2 replicas and its carring out in sequencial. IE, after the pod is terminated then the creation starts. In contrast to deployment where we see that while one pod is terminating, the new pod in parallel is being spin up.
```

## Container Life Cycle Probes

Now you know how Pods are created and recreated, the platform has built-in capabilities to enable it to perform Pod
management automatically.  It is able to detect if Pods become temporarily or permanently unhealthy, and take action
to restore the deployment back to the previously specified desired state.

The key mechanism for this self-healing are life cycle probes.  These probes continuously test the application to see
if it responds successfully.  If it doesn't within a specified number of attempts, the platform will react.  That
reaction varies depending on the type of probe.

We will go over the 3 types of probes and see the impact they have on the application life cycle.  The 3 types of
probes are:

- Startup
- Liveness
- Readiness

### Startup Probes

Startup probes, when defined, are run against the running Pod from the point of startup until successful.  This is
useful for applications with long startup times.  If startup probes are defined, liveness and readiness probes are
not executed until the startup probe completes.

Startup probes are not run throughout the lifecycle of the application like the other probes and stop once successful.

If the timeout is reached before the probe succeeds, the Pod is restarted.

An example startup probe configuration on a container could be:

```yaml
startupProbe:
  httpGet:                          # Perform a GET request 
    path: /bootcamp/actuator/health # Request path 
    port: 8080                      # Request port
  successThreshold: 1               # How many times to succeed before being marked as started
  failureThreshold: 30              # How many times to check for startup completion
  periodSeconds: 10                 # Time between checks
                                    # Total wait time = failureThreshold x periodSeconds (300 seconds in this example)
```

Startup Probes are a great tool for slow, legacy applications, but not really needed for fast-to-boot microservices.

### Liveness Probes

_Liveness in this context means - "is the application responsive?"_

Liveness probes are the main mechanism for finding unhealthy applications and applying the old "turn it off and on
again" approach to fixing them.

When a Pod is determined to be unhealthy, the platform will kill it, and once terminated replace it with another
replica.  This new replica will hopefully become healthy.

Liveness probes are run either from Pod start up time, or if a startup probe is defined, from when that finishes
successfully.

Liveness probes have 3 options you can configure for testing if the application is running, and they are good for
different use cases:

- HTTP request - to test the web application is healthy and working inside the web server
- TCP socket connection - to test if the application is listening on a given port - sometimes this is enough for liveness
- Command execution - when the platform will execute a command inside the application Pod, useful if there is no
  externally exposed interface

Liveness probes have other options like timeouts too - check
[the docs](https://kubernetes.io/docs/tasks/configure-pod-container/configure-liveness-readiness-startup-probes/#configure-probes)
for more details.

An example liveness probe configuration on a container could be:

```yaml
livenessProbe:
  httpGet:                          # Perform a GET request 
    path: /bootcamp/actuator/health # Request path 
    port: 8080                      # Request port
  successThreshold: 1               # How many times to succeed before being marked as alive
  failureThreshold: 3               # How many times to check for startup completion
  periodSeconds: 15                 # Time between checks
                                    # Time before Pod Kill = failureThreshold x periodSeconds (45 seconds in this example)
```

In the above example, if the HTTP call to `/bootcamp/actuator/health` fails 3 times in a row, the Pod will be killed.
The delay of 15 seconds between calls means the platform will kill the Pod roughly 45 seconds after becoming unhealthy.

When one HTTP request passes, the failure counter is reset to 0.

**Tweaking these properties requires consideration of the specific application needs and behaviour.**

### Readiness Probes

_Readiness in this context means - "is the application ready to receive traffic?"_

Readiness probes are the main mechanism for finding unhealthy applications and stopping sending traffic to them.  When a
Pod is determined to be unready, the K8s platform configures the Service in front of the group of Pods by automatically
removing the IP Address of the unready Pod from its group of IPs.  This mean no more traffic will be sent to the
application until it becomes ready again.

This can be a useful way to allow a Pod to recover from a period of high load in which it is swamped with work that
it needs to clear before it can accept more.

This is a less heavy-handed approach to system health, allowing systems to get through in-flight work and then get
new work, without the impact of killing that instance and losing the in-flight work.

Readiness probes are run either from Pod start up time, or if a startup probe is defined, from when that finishes
successfully.

Readiness probes have 3 options you can configure for testing if the application is ready, and they are good for
different use cases:

- HTTP request - to test the web application is running inside the web server
- TCP socket connection - to test if the application is listening on a given port - sometimes this is enough for
  readiness too
- Command execution - when the platform will execute a command inside the application Pod, useful if there is no
  externally exposed interface (less common on Readiness configuration).

Readiness probes have other options like timeouts too - check
[the docs](https://kubernetes.io/docs/tasks/configure-pod-container/configure-liveness-readiness-startup-probes/#configure-probes)
for more details.

An example readiness probe configuration on a container could be:

```yaml
readinessProbe:
  httpGet:                          # Perform a GET request 
    path: /bootcamp/actuator/health # Request path 
    port: 8080                      # Request port
  successThreshold: 1               # How many times to succeed before being marked as ready
  failureThreshold: 3               # How many times to check for startup completion
  periodSeconds: 5                  # Time between checks
                                    # Time before traffic stop = failureThreshold x periodSeconds (15 seconds in this example)
```

In the above example, if the HTTP call to `/bootcamp/actuator/health` fails 3 times in a row, the Pod will be marked as
not ready.

**Note:** It's very important to ensure that the readiness and liveness probe configuration is done together,
ensuring that Pods are marked as not ready _before_ they are killed.  This ensures a smoother experience for client
traffic and better application availability as Pods are given a chance to come healthy before they are killed and
recreated.

**Once again, tweaking these properties requires consideration of the specific application needs and behaviour.**

Also, **RTFM** (Read the Fine Manual) :) - the K8s docs are great and cover these probes in more details.  Very useful
when configuring them - https://kubernetes.io/docs/tasks/configure-pod-container/configure-liveness-readiness-startup-probes/#configure-probes

## ✅Exercise

For the following exercises, we are going to use a new image built with Gradle and Spring Boot.  This gives us more
control of application behavior than the echo server in the earlier chapters and gives you an intro into Spring Boot,
a framework dedicated to building cloud-native microservices.

If you are using Cube (which you should be for all of these exercises), then your environment will already be
configured to build a Spring Boot application we are using to demonstrate some more advanced K8s features.  If you are
not using Cube, then you just need to make sure you have a JDK installed on a Linux environment, minimum JDK 17 and the
JAVA_HOME environment variable set.

Go to the [boot-camp-spring](https://gitlab.com/midships/bootcamp-spring-boot) project and clone it (using SSH).

This example app was created with zero code using the [Spring Initializr Tool](https://start.spring.io/), it's super
handy.  Importantly, the Spring Boot Actuator was included as a dependency when using the tool, to make sure this
capability was included.

### Build the Java App

Run `./gradlew build` from the root of the repository.  This will create the Spring Boot application WAR file which
includes a web server.

You can then run the application using: `java -jar build/libs/bootcamp-0.0.1-SNAPSHOT.war`

Test it using cURL: `curl http://localhost:8080/bootcamp/actuator | jq` and paste the output below:

```
{"_links":{"self":{"href":"http://localhost:8080/bootcamp/actuator","templated":false},"health-path":{"href":"http://localhost:8080/bootcamp/actuator/health/{*path}","templated":true},"health":{"href":"http://localhost:8080/bootcamp/actuator/health","templated":false}}}your answer here
```

Cancel this process now using Control+c.

## Create a Container with the Java App

Again from the root of the repository, run `docker build --tag bootcamp:spring-boot .` to build a local container image
we will later deploy into K8s.

Check its working by running `docker run bootcamp:spring-boot` and paste the output of the following command below
(Note you should see a Spring logo and some server startup log entries):

```
 .   ____          _            __ _ _
 /\\ / ___'_ __ _ _(_)_ __  __ _ \ \ \ \
( ( )\___ | '_ | '_| | '_ \/ _` | \ \ \ \
 \\/  ___)| |_)| | | | | || (_| |  ) ) ) )
  '  |____| .__|_| |_|_| |_\__, | / / / /
 =========|_|==============|___/=/_/_/_/

 :: Spring Boot ::                (v3.3.3)
```

What happens when you curl it using the same curl command and why? (`curl http://localhost:8080/bootcamp/actuator | jq`)

```
site could not be reached because (my guess): the springboot is running on docker desktop where there is no exposure of the service externally via localhost
```

### Create a Deployment for this new application

Follow the outline steps below, you know what to do by this point :)

You can refer to older chapters for examples.

1. Create a Deployment definition in a YAML file for the new container image with 3 replicas of the Deployment. Make
   sure the container image used in your Deployment is `bootcamp:spring-boot`
2. Create a Service to load balance the Pods (_make sure the Service exposes the default Spring Boot port of 8080_)
3. Update the Nginx ConfigMap so that your new Service is accessible outside the K8s cluster using the path `/bootcamp`
4. Ensure your Deployment contains a Liveness and a Readiness probe definition (reach out if you get stuck), you can
   create them both using the same endpoint (`/bootcamp/actuator/health`), but make sure the configuration makes sense
   for both the liveness and readiness use cases.

Paste your Deployment, ConfigMap and Service YAML files here:

```
*** btw paul if you are reading, the dockerfile in your springboot repo is not pointing to the target folder, i have to change it in order to build the image **

apiVersion: apps/v1
kind: Deployment
metadata:
  name: ms-server
spec:
  replicas: 2
  selector:
    matchLabels:
      app: ms-server
  template:
    metadata:
      labels:
        app: ms-server
    spec:
      containers:
        - image: bootcamp:spring-boot
          imagePullPolicy: IfNotPresent
          name: ms-server
          ports:
            - containerPort: 80
          livenessProbe:
            httpGet:
              path: /bootcamp/actuator/health
              port: 8080
            initialDelaySeconds: 10
            periodSeconds: 5
          readinessProbe:
            httpGet:
              path: /bootcamp/actuator/health
              port: 8080
            initialDelaySeconds: 10
            periodSeconds: 5
          env:
            - name: PORT
              value: "8080"
  
==================================================

apiVersion: v1
kind: Service
metadata:
  name: ms-service
spec:
  ports:
    - port: 8080
      targetPort: 8080
      protocol: TCP
  type: ClusterIP
  selector:
    app: ms-server

====================================================

apiVersion: v1
kind: ConfigMap
metadata:
  name: nginx
data:
  echo-server.conf: |
    server {
        listen 80;

        location /bootcamp/ {
            proxy_pass http://ms-service:8080/bootcamp/;
            proxy_set_header Host $host;
            proxy_set_header X-Real-IP $remote_addr;
            proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
            proxy_set_header X-Forwarded-Proto $scheme;
        }
    }
```

Now modify your Deployment file so that the readiness probe endpoint is invalid, e.g. `/bootcamp/actuator/404`, and
re-apply your YAML file containing the deployment to update the config.

What happens when you do this?  Does the deployment succeed?  Describe what you see below:

```
NAME                           READY   STATUS    RESTARTS     AGE
ms-server-7c4d9c5f6b-cgvql     0/1     Running   3 (0s ago)   60s
ms-server-7c4d9c5f6b-cgvql     0/1     Running   4 (1s ago)   81s

the pod can never enter ready state
```

It's hard to manufacture more interesting failure scenarios without coding something into the application, but hopefully
you can see from the above example, the impact that the probes have on the Pods life cycle.

# Conclusion

Here you have learned how to configure the platform to help your applications remain healthy without any human
intervention, cool, huh?
