# 00 - The Cube
The Cube Developer Toolkit is a standardised environment that we will all use for work at Midships on internal projects 
like the accelerator.

This first bootcamp session is designed to get you a working local environment with the following tools:

- Docker Desktop - for building container images
- Kubernetes - a local K8s Cluster managed by Docker Desktop
- The Cube - a developer environment

By all starting on the same footing with the same environment, the following exercises can more easily be designed and completed by you.

So this first part of the K8s Bootcamp, is all about installing the tools and starting from a level playing field.

## ✅ Exercise
You can skip to the overview below if you already have it running 🤩

### Install The Cube.

Clone the following repo from GitLab and follow the instructions in the README.md file in the root of it: 
https://gitlab.com/midships/cube

You should by the end, have a working developer toolkit and K8s cluster for the following exercises.

## The Cube Overview

Now you have The Cube running, lets take it for a spin.

### Understand Docker Compose

Running this single command will allow you to build, start and then SSH into the running Cube container.  

So give it a try from a native shell terminal on your host laptop.

```shell
docker compose up --build --detach && docker compose exec cube bash
```
Lets break this down:

`docker compose up` - This starts the containers defined by the docker-compose.yml file found in the current directory.  
This defines the container image, the networking, the storage, and the things that vary per environment that the container is going to be run in.

`--build` - This says to build the container (not necessary every time but caching is on so its quick).  When built, the
OS and tools are all patched to the latest version so it is a good idea to do this frequently.

`--detach` - Start the container in detached mode, so that if the shell is exited by a script or an accidental ctrl+d, 
the container is still running and you can just ‘exec’ again to get back in

`docker compose exec cube bash` - Once built and running, this starts a shell session inside The Cube.

Have a look at the `docker-compose.yml` file below and we will go through the important pieces, check the inline 
comments for an explanation of the parts:

```yaml
services:
  cube: # Name of the service
    build:
      dockerfile: ./Dockerfile # Defines how to build the container image
      args: # Build arguments passed into the build process to enable variation depending on the local environment
        - USER
        - CPU_ARCH
    network_mode: "host" # Join the Cube to the host network to allow access to the K8s API and the Docker Engine
    container_name: cube
    hostname: cube
    entrypoint: # What to run - something that never stops so neither does the container
      - sh
      - -c
      - tail -f /dev/null
    extra_hosts: # Configure the /etc/hosts file on your host machine so you can access servers running inside it from your browser
      - cube:127.0.0.1
      - cube.midships.io:127.0.0.1
      - kubernetes.docker.internal:127.0.0.1
    user: "1000"
    volumes: # Mount resources from the host into the container (these are defined in the .env file you created during setup)
      - home:/home/${USER}:rw
      - ${DOCKER_DESKTOP_KUBECONFIG_HOST_LOCATION}:/home/${USER}/.kube/docker-desktop-config
      - ${SSH_CONFIG_HOST_LOCATION}:/home/${USER}/.ssh
      - ${DOCKER_SOCKET_HOST_LOCATION}:/var/run/docker.sock
      - ${SOURCE_HOST_LOCATION}:/usr/local/src
      - ${GIT_CONFIG_HOST_LOCATION}:/home/${USER}/.gitconfig
      - ${CUBE_HOST_LOCATION}:/home/${USER}/.cube
    working_dir: /home/${USER}
    environment: # Some environment configuration
      - KUBECONFIG=/home/${USER}/.kube/docker-desktop-config:/home/${USER}/.kube/config
      - TZ=${TZ}
      - HELM_PLUGINS=/home/${USER}/.local/share/helm/plugins

# https://docs.docker.com/compose/compose-file/07-volumes/
volumes: # Define a Volume allow files to persist between runs of the container
  home:
    # https://docs.docker.com/compose/compose-file/07-volumes/#external
    external: false
```

Once The Cube starts and you exec into it, the shell prompt will look something like this:

```shell
☸ docker-desktop 🛠 in ~
#
```

### Understand the prompt
The prompt is the first thing you see when you start the shell session.

Here it is Starship which is a very configurable prompt built in Rust.  The config for it is in 
`~/.config/starship.toml` in case you want to play around with how it looks.

When you enter a Git directory inside The Cube, you get Git information printed on the prompt too, which is very useful.

```shell
☸ docker-desktop 🛠  in cube  🍣 feature/change-prompt-config-to-avoid-wrapping-issues 📝 ×1🛤️  ×1
#
```

The above prompt shows the git branch I’m on, the fact that I have local uncommitted changes, that I am ahead of the 
main branch etc.

When you use Kubernetes commands, this too updates the prompt.

Try the following:

```shell
kubens
```

It should list all of the namespaces inside the K8s cluster you have running in Docker Desktop.  Now select one using:

```shell
kubens default
```

The prompt will now show you the kubernetes context you are using (in this case docker-desktop) followed by the 
namespace you are currently working in (default)

```shell
☸ docker-desktop 🛠  (default) in ~/src
#
```

## ✅ Exercise
Create a Merge Request against The Cube.  This will be merged on to the main branch of this repository and impact 
everyone’s version of The Cube, the next time they refresh the code and rebuild and use it.

This is intentional. The Cube is an [innersourced](https://about.gitlab.com/topics/version-control/what-is-innersource/)
tool that allows us as a team to all maintain and grow our developer toolkit.

Your Merge Request could be anything that improves The Cube.  It could be any one of the following things or literally 
anything that you think will make this tool better:
- Add a new CLI tool 
  - e.g. gcloud or AWS CLI
- Add support for a new language:
  - e.g. Go, Python
- Add a bash alias
- Improve the prompt
- Improve the documentation
- Remove something unnecessary
- Change the version of an existing tool
- Add an autocomplete for an existing tool like kubectl or helm
- Fixing a bug
- Anything…

Here is an example one I created to add Java and update the docs:

https://gitlab.com/midships/cube/-/merge_requests/3

Or one to update the Starship prompt configuration:

https://gitlab.com/midships/cube/-/merge_requests/4

Make sure you add Aja OR Paul to your review.

## Conclusion
This tool will allow all of us to use the same development environment, have the same tools available and also easily 
run ForgeRock locally - that will come in future exercises.

The benefits of using an approach like this are:

- Consistency - everyone has the same tools and is using the same base approach for developing.  So if someone has a 
  problem, others can help as they are familiar with the setup.
- When bugs are found, if someone fixes it and raises a Merge Request, they fix it for all users.
- Same with tooling, if someone adds a new one or configures it better and raises a Merge Request, then all users 
  benefit from this.
- Linux is default environment you can now easily use day to day.  This means you can get really comfortable using the 
  Linux environment and toolset, meaning you will be far better placed to help our customers and make yourself a more 
  desirable employee in a cloud-native world where Linux is king.