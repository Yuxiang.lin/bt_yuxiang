# 05 - More Deployment Types

## Deployments

In K8s, the most commonly used deployment pattern is a **D**eployment.  In this pattern, you get X numbers of
`replicas`, or copies of your Pod, as defined by your Deployment resource.  We've seen this already.

They don't have state or persistent volumes, any state they deal is held in an external store like a cache or a
database.

The Pods in a Deployment should be treated as cattle, not pets.  They don't have names, and you shouldn't care if
they die.

They are never accessed directly as they don't have easy to use DNS addresses and these change for each one.  Pods
in Deployments should be scalable out and in very easily, with all traffic reaching them through a Service DNS address.

Deployments are perfect for microservices.

## StatefulSets

The name gives it away really, this is a Set of Pods that are Stateful.  This means that they (usually) have a
persistent storage volume attached to them, and they have names.  These Pods are treated more like pets :)

This deployment type caters to certain use cases, like running heavyweight applications like ForgeRock and Ping, or
clusters of MongoDB or other stateful workloads.

The components in these apps need to know the addresses of the other individual component Pods, for data replication
and internal load balancing mechanisms.  So we have to know up front how many Pods we want in the StatefulSet (or STS)
so that we can configure the DNS address for the Pod into some of the application components.

StatefulSets also provide a new mechanism for the pod, a Volume Claim Template, which allows each Pod to claim a volume
in the storage array attached to the cluster or on a network, and as this Pod is named, if it is replaced with another
pod of the same name, it will get access to the same volume.  This provides the stateful mechanism in the StatefulSet.

## Pod

A Pod is also a deployment pattern.  You can request that the K8s platform runs a standalone Pod containing a single
instance of your application.

This can be useful for playing around and experimenting, but is rarely used in a production capacity.

Deployments and StatefulSets, but have a Controller ensuring that there are X numbers of Pods running as defined by the
Deployment or StatefulSet.  This means when a Pod dies, it will be replaced by the Controller.  A Pod on its own has no
Controller managing it, so when it dies, it is gone for good.  Hence, it's not good for production usage.

## Job / CronJob

A Job is another way to run one or more Pods.  Pods run in this way are usually kicked off to perform some kind of
batch processing of data (though usually one Pod is used to avoid data processing complexity).

Jobs are useful ways to perform tasks asynchronously but the K8s server has to be told to run the Job
for a particular reason.  This can often be done by running a job after a pipeline build to perform some cleanup or
as an adhoc or scheduled pipeline to run a task within the cluster.

A CronJob is a special type of Job that has a cron (or time) schedule.  This means that the Job can be kicked off when
the cron schedule triggers.  See https://crontab.guru/ for a great tool for creating cron schedules.

## ✅Exercise

Create a CronJob to run a Pod in your cluster every minute, that prints out "Hello from the Kubernetes cluster" and
then exits.  Provide the YAML file you used to create it below:

```
apiVersion: batch/v1
kind: CronJob
metadata:
  name: yuxiang-cronjob
spec:
  schedule: "* * * * *"  
  jobTemplate:
    spec:
      template:
        spec:
          containers: 
          - name: hello
            image: busybox:1.28
            imagePullPolicy: IfNotPresent
            command:
            - /bin/sh
            - -c
            - date; echo Hello Yuxiang
          restartPolicy: OnFailure
```

Now paste the full logs from one of the Pods that ran, started by your CronJob:

```
Sat Sep 21 16:31:00 UTC 2024
Hello Yuxiang
```

## Create A StatefulSet

Let's dive into StatefulSets (STS) a bit more as they are used heavily in our work.

A StatefulSet is a set of X number of Pods, just like a Deployment.  Though with a StatefulSet, the Pods are named in a
consistent way.  So instead of having the random alphanumeric suffixes seen on Pod names in a Deployment, they are named
with a zero-based ordinal which increments by one for each new Pod added to the set.

This means that the Pod has a consistent identifier, which is used when defining access to a persistent volume.  It's
also used in DNS addresses, so you can know the address of the individual pods in advance.  This is not possible with
Pods in a Deployment, which have to be accessed behind a Service.

So when you need to store state in a Volume, or know the direct address of a Pod in advance, you need to use a
StatefulSet.

### Anatomy of an STS

STSs have some features that differ from a Deployment.  Let's make one and look at the differences.

Create a file called `echo-server-statefulset.yaml` and apply it into the bootcamp namespace.

```yaml
apiVersion: apps/v1
kind: StatefulSet
metadata:
  name: es
spec:
  replicas: 2
  selector:
    matchLabels:
      app: echo-server-sts
  template:
    metadata:
      labels:
        app: echo-server-sts
    spec:
      containers:
      - name: echo-server-sts
        image: ealen/echo-server:latest
        ports:
        - containerPort: 80
          name: http
        volumeMounts:
        - name: demo
          mountPath: /demo
  volumeClaimTemplates:
  - metadata:
      name: demo
    spec:
      accessModes: [ "ReadWriteOnce" ]
      resources:
        requests:
          storage: 100Mi
```

You will end up with 2 new Pods coming up, called `es-0` and `es-1`, e.g:

```shell
# k get po
NAME                           READY   STATUS    RESTARTS   AGE
echo-server-7c46dc569b-8hq7d   1/1     Running   0          22m
es-0                           1/1     Running   0          12m
es-1                           1/1     Running   0          12m
```

See the name differences?  As this is an STS, the pods are all named using the _name_ of the STS and an integer suffix.

Let try hitting these guys now.  As we don't have any configuration in our Nginx it won't work.

From your terminal session in The Cube, run this command: `curl es-1`.

This is because our Cube environment and the Kubernetes environment are on different networks, and the DNS name `es-1`
means nothing to your Cube.

As we still want to test this URL, lets run the same command from inside one of the Pods.

```shell
# Create an SSH Session in Pod 1
kubectl exec --stdin --tty es-1 -- sh
```

Now try hitting the endpoint from inside the Pod itself.  Note that `cURL` is not available here, but `wget` is, so
we'll use that to validate the server is running, so from inside the SSH session in Pod 1, run:

```shell
wget -q -O - es-1
```

This works from inside the Pod as the Pod knows it's own hostname of es-1.  You can see that in the Pod by running:

```shell
env | grep HOSTNAME
```

So how do we make this useful for us, so that we can hit both of the STS pods from the outside?

### STS Networking

We need to again update the Nginx Ingress mechanism to route requests to our STS Pods.

## ✅Exercise

In this exercise we will define a new Service for the STS, update the Nginx config to be able to route requests to this
new Service and then access the StatefulSet from your Cube.

Create a new Service like the one you created in Chapter 1, but for routing to the STS `echo-server-sts` and paste the
YAML configuration for it here:

```
your answer heapiVersion: v1
kind: Service
metadata:
  name: echo-server-stateful
spec:
  ports:
    - port: 8080
      targetPort: 80
      protocol: TCP
  type: ClusterIP
  selector:
    app: echo-server-stsre
```

Update the Nginx Configuration by creating a new file, copied from your old `nginx-config-map.yaml` file from chapter 2
and create a _new location_ in it pointing to `/sts`.  When you apply this new ConfigMap into your environment, you will
need to restart the Nginx Pod to make it pick up the configuration.  You can do this by just deleting the Pod.  The
Deployment will make sure a new one comes right back.

Once the new Nginx Pod is up, try the following curl command again and print your answer below:

```shell
curl "http://cube/sts?echo_env_body=HOSTNAME"
```

```
"es-0"
```

### STS Storage

Now we want to push a file into each Pod's persistent volume.  Run this one-liner that will push a unique log file into
the persistent file system for each pod.

```shell
for i in {0..1}; do   echo "Hi from volume ${i}" > "demo.log" && k cp "demo.log" "es-${i}:/demo"; rm demo.log; done
```

Now delete each pod.  They will come back to life like we saw earlier and once they are up, for each Pod, I want you to
take inspiration from the above script, and pull the log file from each Pod and print the contents below.

Show all the commands you use to pull the file from the Pod and print the contents of each, along with the contents:

```
for i in {0..1}; do   k cp "es-${i}:/demo/demo.log" "demo${i}.log"; cat "demo${i}.log";  done
```

# Conclusion

You've now learned about the main deployment types a Kubernetes developer would use, Deployment, StatefulSet, Job,
CronJob, Pod. There are a few more, but that's enough for now.
