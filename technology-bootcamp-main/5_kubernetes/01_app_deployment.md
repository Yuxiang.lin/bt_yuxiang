# Introduction

Welcome to the K8s Bootcamp.  This is a long hands on course created to teach you the fundamentals of building
applications using Kubernetes (or K8s).

When following along with the exercises, we recommend that you create all of the files uniquely for each chapter,
either named with the chapter name in it or in folders named for chapters.  This will help you reuse some of the example
content in later chapters as some of the exercises build on previous examples.

As always, the docs on [kubernetes.io](kubernetes.io) are an excellent place to get help, but do reach out to the
authors if you get stuck.  Have fun!!

# 01 - Simple App Deployment

Deploying a simple application to Kubernetes can be a bit overwhelming for beginners, but breaking it down into clear
steps with code samples can make the process more approachable. Here, we'll deploy a simple application by following
these steps:

### Prerequisites

- **Kubernetes cluster**: Ensure you have access to a Kubernetes cluster. You can use Docker Desktop, as already
  configured for The Cube.
- **Kubectl**: The Kubernetes command-line tool and other required too should already be installed and configured to
  interact with your cluster. Start with [00 - The Cube](./00_cube.md) if you haven't already.

## Create a Namespace to work in

Namespaces are the basic configuration boundary within Kubernetes. You can create and destroy namespaces as you need to.
If you follow through these steps and get stuck, just delete the namespace and start again.

```shell
kubectl create namespace bootcamp
```

Here we have used the `kubectl` (or Kube Control) CLI tool to call the Kubernetes API to create a Namespace resource.

Everything in Kubernetes is described as Resources. You can get the details of the namespace using:

```shell
kubectl get namespace bootcamp --output yaml
```

Note that lots of K8s resources have short names, just like Kubernetes and k8s. So to get the Namespace YAML, you can
also run:

```shell
kubectl get ns bootcamp -o yaml
```

Nice to have lazy shortcuts!

Finally, use the kubens tool to select that namespace as the current one you wish to work with:

```shell
kubens bootcamp
```

Kubens is not always installed on all systems, so the equivalent kubectl command is:

```shell
kubectl config set-context --current --namespace=bootcamp
```

## ⚠️ AN IMPORTANT NOTE ON NAMESPACES️ ⚠️

When using the Cube, the prompt will show you the namespace you are currently using.

```shell
☸ docker-desktop 🛠  (bootcamp) in ~
#
```

Here the Kube Context (docker-desktop) and currently selected Namespace (bootcamp) are displayed.

When working with K8s, it's important to make sure you know what namespace you are going to target with your commands,
to avoid pushing configuration into a namespace by accident - hence why it's good to have on your shell prompt.

By using `kubens` we have created some configuration in `~/.kube/docker-desktop-config` that means we will by
default target the bootcamp namespace with our use of `kubectl`.

There are other ways to make sure of this too.  You can put the namespace as an option on your resource YAML files.  Normally you
don't want to do this, so you can apply the same files into multiple namespaces, e.g. through a CI/CD pipeline, but there are
good reasons to do so too.

You can also specify the namespace on the command line like this:

```shell
kubectl get pods --namespace kube-system
```

All 3 are valid approaches, you can use which ever suits your need.

Another little note - you notice the `kube-system` namespace above, that namespace or others that end in `-system`
should not be modified unless you know what you are doing.  Modifications of the system namespaces can break the
cluster and in most corporate environments, these are fully locked down except to administrators.

## Create a Deployment for an Echo Server App

Create a deployment YAML file named `echo-server-deployment.yaml` for the "echo-server" microservice:

The echo-server docs are here, if you need to refer to them in any exercise: https://ealenn.github.io/Echo-Server/

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: echo-server
spec:
  replicas: 1
  selector:
    matchLabels:
      app: echo-server
  template:
    metadata:
      labels:
        app: echo-server
    spec:
      containers:
        - image: ealen/echo-server:latest
          imagePullPolicy: IfNotPresent
          name: echo-server
          ports:
            - containerPort: 80
          env:
            - name: PORT
              value: "80"
```

Apply the deployment configuration:

```shell
kubectl apply -f echo-server-deployment.yaml
```

Verify the deployment:

```shell
kubectl get pods
```

## ✅Exercise

Add the output of `kubectl get pods` below:

```
NAME                           READY   STATUS    RESTARTS   AGE
echo-server-768fdbbd5f-wnq66   1/1     Running   0          25syour answer here
```

## ✅Exercise

Describe the above `echo-server-deployment.yaml` file. Use Google to tell us:

What the `spec.selector` field does:

```
it define the how the created replicaSet (I assume replicaSet refer to a deployment as defined in the yaml) finds which pod to manage. In our yaml, its saying match labels -> app = echo-server and it should then means that the replicaSet is going to manage/deploy pod under the template section that has labels matching echo-server
```

What the `spec.template.spec.containers` field and the properties below it do

```
the template define the pod, template.spc.containers indicates the containers that pod should run, in our case our pod should run just one contrainer that is the echo server image
```

In the output from `kubectl get pods`, there are 5 fields. Use Google to tell us the following:

What does STATUS mean?

```
It shows if the pod is running or pending (starting up) or error (not working)
```

How can you modify the above command to print more information about all the running pods in the namespace?

```
kubectl get pods --output=wideyour answer here
```

## Create a Service for the App

The above deployment gets us a server to connect to, but there is no way to connect to it yet. So we need to introduce a
networking concept known as a `Service` to provide an IP to expose the Deployment.

Create a service YAML file named  to expose the "echo-server" microservice:

```yaml
apiVersion: v1
kind: Service
metadata:
  name: echo-server
spec:
  ports:
    - port: 8080
      targetPort: 80
      protocol: TCP
  type: ClusterIP
  selector:
    app: echo-server
```

Apply the service configuration:

```shell
kubectl apply -f echo-server-service.yaml
```

With The Cube and how it is configured with the local Kubernetes Cluster, we are able to access this Service type of
ClusterIP. So to test, open a terminal session inside The Cube, and run the following:

```shell
curl -I --header 'X-ECHO-CODE: 200' "http://$(kubectl get service echo-server -o jsonpath='{.spec.clusterIP}'):8080/"
```

There is a lot going on there so lets break it down from inside out:

- **kubectl get service echo-server -o jsonpath='{.spec.clusterIP'}** - get the Service using the jsonpath option to
  modify the response to only include the field at that path. In this case, that is the `clusterIP` address.
- **curl** - make a request to the IP address provided by the subshell command in the $()
- **:8080** - use the port we have selected to expose the application on via it's Service
- **-I --header 'X-ECHO-CODE: 200'** - Curl options to ask for just the HEAD response (-I) and use the echo-server
  specific header to get the server to return a 200.

When run from inside The Cube, this will return a 200 response. If you try to access this from the
browser on your host laptop, it won't be accessible. Try it now at  http://localhost:8080/.

This is because The Cube and the Kubernetes Cluster are on the same network provided by Docker Desktop. Your host laptop
network is outside of that network.

So you need another type of Service to make it accessible.

## Expose the App using Load Balancer Service

Kubernetes applications run inside a segregated software-defined network. So in order to access this application, you
will need to map the port from the host using the IP address of the laptop (localhost or 127.0.0.1).

Create another service of type LoadBalancer by creating a file called `echo-server-load-balancer.yaml`

```yaml
apiVersion: v1
kind: Service
metadata:
  name: echo-server-load-balancer
spec:
  selector:
    app: echo-server
  ports:
    - name: http
      protocol: TCP
      port: 80
      targetPort: 80
  type: LoadBalancer
```

Then:

```shell
kubectl apply -f echo-server-load-balancer.yaml
```

**Note:**

In Docker Desktop, if nothing else is running in your cluster which has already been assigned the localhost address, you
should get localhost assigned to this new Service.

If you don't and it shows as `<pending>` as per the following example, you should check other services in other namespaces
for the localhost address assignment and delete that to be able to continue (Hint, use `k get svc -A`).

```shell
# k get svc echo-server-load-balancer
NAME                       TYPE           CLUSTER-IP       EXTERNAL-IP   PORT(S)        AGE
echo-server-load-balancer  LoadBalancer   <cluster-ip>     <pending>     80:31234/TCP   1m
```

## Test

Look at the Services in your namespace now

```shell
kubectl get svc
```

You should see something like:

```shell
NAME                        TYPE           CLUSTER-IP      EXTERNAL-IP   PORT(S)        AGE
echo-server                 ClusterIP      10.110.222.46   <none>        8080/TCP       5m37s
echo-server-load-balancer   LoadBalancer   10.96.177.139   localhost     80:31592/TCP   26s
```

Since the EXTERNAL-IP field is set to localhost and PORT 80, that means we can hit http://localhost/ in a browser or
using curl to test.  The port is 80 as in the LB Service, we are exposing port 80, and targeting port 80 on the
Pod (look at the Deployment file we create for the echo-server to see the port being exposed on the Pod).

```
--LAPTOP---    --K8s-LoadBalancer--    -----K8s-Service------    -----K8s-Pod-----
| browser | -> | localhost:80     | -> | 10.110.222.46:8080 | -> | 10.1.11.86:80 |
-----------    --------------------    ----------------------    -----------------
```

You can map these Service ports however you want.  But Pod ports have to map to the port being used
by the server process inside the Pod.

## ✅Exercise

Run `curl -I "localhost/?echo_code=200-400-500"` can paste the output here:

```
HTTP/1.1 200 OK
X-ECHO-RANDOM-STATUS: 200
X-ECHO-RANDOM-STATUS: 400
X-ECHO-RANDOM-STATUS: 500
Content-Type: application/json; charset=utf-8
Content-Length: 846
ETag: W/"34e-0yqVLgPDI1tIH6bGJUZZp3drjy0"
Date: Sat, 21 Sep 2024 13:46:36 GMT
Connection: keep-alive
Keep-Alive: timeout=5
```

## Pod Death

So, you might be wondering what happens when a Pod dies.  Does it go to Pod heaven?  Or is there just nothingness for
Pod in the afterlife.

Unfortunately for them, its the latter.   But fortunately for us, when one dies, a new one is born to take its place.

Lets look at this in action.

## ✅Exercise

Run the following command get a list of your Pods - `k get po` - and you should see something like:

```shell
# k get po
NAME                           READY   STATUS    RESTARTS   AGE
echo-server-7c46dc569b-t6vdf   1/1     Running   0          11s
```

Now get 2 terminals going side by side.  In one, run the following: `kubectl get pods -w`

Copy the above 'echo-server' Pod name, and do the following - `k delete pod <pod_name>`.

What happens next and why?  Please explain using the K8s docs as needed:

echo-server-768fdbbd5f-wnq66   1/1     Running   0          33m
echo-server-768fdbbd5f-wnq66   1/1     Terminating   0          34m
echo-server-768fdbbd5f-hz7v9   0/1     Pending       0          0s
echo-server-768fdbbd5f-hz7v9   0/1     Pending       0          0s
echo-server-768fdbbd5f-hz7v9   0/1     ContainerCreating   0          0s
echo-server-768fdbbd5f-wnq66   0/1     Terminating         0          34m
echo-server-768fdbbd5f-wnq66   0/1     Terminating         0          34m
echo-server-768fdbbd5f-wnq66   0/1     Terminating         0          34m
echo-server-768fdbbd5f-wnq66   0/1     Terminating         0          34m
echo-server-768fdbbd5f-hz7v9   1/1     Running             0          0s

Looks like when the wnq66 is killed, another pod that is running the same image is also being brought up and take its place. My guess is that in our deployment, we defined replica as 1 so the deployment that is managing, ensures that there is always 1 instance of the pod to be running, when that instance is killed, the deployment will spin up another one using the template defined.

## Conclusion

You have successfully deployed a simple echo-server application to a local Kubernetes cluster and learned about how to
make it accessible in different ways from both inside the k8s local network and outside it, from the host laptop.
