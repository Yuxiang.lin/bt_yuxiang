# 08 - Certificates

Strap in, this one is going to get spicy!

In this chapter, you are going to deploy a new way to define and create resources in Kubernetes, and we will install
a new ingress mechanism along with a certificate issuer to allow us to create certificates which match our URLs.

Firstly, before you start, if you are not comfortable with TLS and certificates, or want a refresher, go and watch
the chapter [HTTPS Fundamentals](https://app.pluralsight.com/course-player?clipId=c3fbf361-0d1c-419e-9826-7ea6f688d1ba)
in the Pluralsight course - [What Every Developer Must Know About HTTPS
](https://app.pluralsight.com/library/courses/https-every-developer-must-know/table-of-contents)

---

**A note on extensibility:**

Kubernetes provides many ways to extend the functionality inside it.  It also defines many specifications for
functionality and other 3rd parties provide the functionality.

These specifications and extensions are often defined as Custom Resource Definitions, or CRDs.

It this chapter we are going to see the Ingress and Certificate resources defined using a CRD and a new type of the
Secret resource, which is fulfilled using an extension provider.

---

## Deploying a new Nginx

If you still have the Nginx server running in your bootcamp namespace from Chapter 2, go and delete it, if it is
still running for these exercises, they will not work.

We are going to deploy a more modern and Kubernetes-native version of Nginx which we can configure more easily with the
direct use of the Kubernetes APIs.

## ✅Exercise

First, install the Ingress-Nginx Controller.

```shell
# Install Ingress Nginx into a new namespace using Helm
helm upgrade --install ingress-nginx ingress-nginx \
  --repo https://kubernetes.github.io/ingress-nginx \
  --namespace ingress-nginx --create-namespace
```

Now check that the controller is running OK in the `ingress-nginx` namespace using the following command and paste
the output below:

```shell
k get deployment --namespace ingress-nginx
```

```
NAME                       READY   UP-TO-DATE   AVAILABLE   AGE
ingress-nginx-controller   1/1     1            1           5m3s
```

And prove the Nginx server is working by printing the 404 response from `curl http://cube.midships.io` below:

```
<html>
<head><title>404 Not Found</title></head>
<body>
<center><h1>404 Not Found</h1></center>
<hr><center>nginx</center>
</body>
</html>
```

## Ingress Controllers

An `Ingress` is a kind of Kubernetes resource that is picked up and managed by an `Ingress Controller` which
configures a TCP/UDP entrypoint into the cluster.  There are a few implementations of Ingress Controllers out there
as the functionality is defined by a specification in the Kubernetes ecosystem.  This allows different companies or
opensource projects to solve the same problem in different ways, and we get to pick the best solution for us.   We
are going to use the [Ingress-Nginx Controller](https://kubernetes.github.io/ingress-nginx/).

Previously, we manually ran an instance of Nginx and used a LoadBalancer service to expose an entrypoint, then
configured the nginx server running in a Pod using a ConfigMap.  This was a bit of a clunky approach, and required us to
reload the Nginx server for it to pick up the latest configuration.

This new approach is more Kubernetes-native.  Once the Ingress Controller is installed, we can use the Kubernetes
API to create an ingress rule, and the Ingress Controller takes care of pushing that configuration into all the
Ingress Controller Pods at runtime, with no reload required.

This means we can take advantage of all the tooling around Kubernetes, like Helm, to define our ingress routes in
YAML and release them with zero downtime.

So let's create an `Ingress` and see it in action.

## ✅Exercise

We know we have the Nginx Controller running from the earlier `curl http://cube.midships.io` command which gave us a
404, which is perfect.

You are also going to need the Spring Bootcamp microservice from chapter 6 deployed and running for this exercise.
If it's not, go get it going again.

Now we want to add an Ingress to expose a path to the bootcamp microservice health endpoint.  So make sure you are
using the `bootcamp` namespace by using `kubens bootcamp`, then create a file called `bootcamp-ingress.yaml` with
the content below and apply it.

```yaml
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: bootcamp
  annotations:
    nginx.ingress.kubernetes.io/use-regex: "true" # Allows us to use pattern matches in the paths
spec:
  ingressClassName: nginx                         # Default IngressClass name
  rules:
    - host: cube.midships.io                      # Host and path are used to match a request to a routing rule 
      http:
        paths:
          - path: /bootcamp/.*                    # Match http://cube.midships.io/bootcamp/anything
            pathType: ImplementationSpecific
            backend:
              service:                            # Service to send the request to
                name: springbootcamp              # Name of the service - must be in same namespace as the Ingress 
                port:
                  number: 8080                    # Port the service is listening on
```

Once you have applied that into the bootcamp namespace, if you make this request you should get a JSON response:

```shell
curl --silent http://cube.midships.io/bootcamp/actuator | jq
```

Paste your response payload here:

```
{"_links":{"self":{"href":"http://cube.midships.io/bootcamp/actuator","templated":false},"health":{"href":"http://cube.midships.io/bootcamp/actuator/health","templated":false},"health-path":{"href":"http://cube.midships.io/bootcamp/actuator/health/{*path}","templated":true}}}your answer here
```

## ⚠️ Important ⚠️

If you did not get a 200 response that starts with `{"_links": {` then you can't continue further.  The rest of this
chapter relies on the Ingress Controller working.  See the Troubleshooting section next or ask for help as you
cannot continue without this working.

If you got a 200 response, you can skip the troubleshooting though there might be useful future tips for you in there.

### Troubleshooting

If you are having trouble making the above request work, firstly have a look at the logs in the Ingress Controller
Pod(s) using the following command:

```shell
k logs -f deployment/ingress-nginx-controller -n ingress-nginx
```

**TIP:** The above command gets all the logs for all Pods in the Deployment instead of just looking at the logs for
individual Pods, one at a time.  This can be very helpful.

You might see a 503 or a 404 in the logs which tells you that some of the configuration is not quite right but the
request is hitting the ingress controller which is good.  Then you should check that the Ingress is in the right
Namespace using `k get ingress -n bootcamp`, where one Ingress should be found.

If you are not getting any logs relating to failed requests in the Ingress Controller logs, you might have a
conflicting Service that is holding on to the one `localhost` IP mapping.  So use `k get svc -A` to look at all
the Services in all Namespaces,  If your `EXTERNAL-IP` for the `ingress-nginx-controller` Service is not showing as
`localhost` or there are multiple with `localhost`, then delete the other one(s) to allow this Nginx Controller to
bind to that localhost address.

```shell
k get svc -n ingress-nginx
NAME                                 TYPE           CLUSTER-IP       EXTERNAL-IP   PORT(S)                      AGE
ingress-nginx-controller             LoadBalancer   10.100.130.253   localhost     80:30100/TCP,443:32501/TCP   14h
ingress-nginx-controller-admission   ClusterIP      10.96.84.247     <none>        443/TCP                      14h
```

Make sure the `springbootcamp` Service exists and Deployment is healthy:

```shell
k get service springbootcamp -n bootcamp
NAME             TYPE        CLUSTER-IP      EXTERNAL-IP   PORT(S)    AGE
springbootcamp   ClusterIP   10.96.195.223   <none>        8080/TCP   41h

k get deployment springbootcamp -n bootcamp -o wide
NAME             READY   UP-TO-DATE   AVAILABLE   AGE   CONTAINERS       IMAGES                 SELECTOR
springbootcamp   3/3     3            3           41h   springbootcamp   bootcamp:spring-boot   app=springbootcamp
```

Other than that, poke around your cluster, try deleting namespace(s) and recreating the resources.  If you get stuck,
ask for help :)

## Checking the Current TLS Certificate

Ingress Nginx comes with a built-in certificate, so straight away we can use HTTPS. It's very obviously a self-signed
certificate, meaning that your operating systems (Cube's Linux OS and your host OS) won't trust it. Also, the Common
Name (CN) of the certificate doesn't match the address of the service we are hitting.  So even if we decided to trust
that self-signed cert, we still couldn't get a secure connection as the CN and the Host Name would never match.

We can't create valid certificates in an environment like our local one, as we need to prove ownership of a Domain
Name for any certificate issuer to create a valid certificate for us.  But we can create a local Certificate
Authority and use that to issue certificates to the services in our cluster.

So first to check the current cert:

```shell
# Run this first - curl will print an error about the self-signed certificate
curl https://cube.midships.io/bootcamp/actuator/health

# Temporarily trust this self-signed cert using the --insecure flag
curl --insecure https://cube.midships.io/bootcamp/actuator/health  
```

Let's inspect the cert being presented using `OpenSSL`.

```shell
openssl s_client -showcerts -servername "cube.midships.io" -connect "cube.midships.io:443" </dev/null
```

You should see a decoded certificate with some interesting details, like:

```shell
subject=O = Acme Co, CN = Kubernetes Ingress Controller Fake Certificate
issuer=O = Acme Co, CN = Kubernetes Ingress Controller Fake Certificate
```

Obviously, we don't want to be insecure, and we don't want to trust a self-signed cert as it could have come from
anywhere, so we have to configure a real cert to present to clients when they connect to us.

## Installing the CertManager

To install the cert manager, run the following command.  This will create a new `cert-manager` namespace, install
some CustomResourceDefinitions, a Deployment, some Services, some RBAC stuff which we've not covered.  Basically
everything needed to run the cert-manager:

```shell
LATEST=$(curl -s https://api.github.com/repos/cert-manager/cert-manager/releases/latest | jq -cr .tag_name)
kubectl apply -f "https://github.com/cert-manager/cert-manager/releases/download/${LATEST}/cert-manager.yaml"
```

Check its running:

```shell
k get po -n cert-manager
NAME                                     READY   STATUS    RESTARTS   AGE
cert-manager-6fd987499c-th85n            1/1     Running   0          2m15s
cert-manager-cainjector-5b94bd6f-5xf9s   1/1     Running   0          2m15s
cert-manager-webhook-575479ff47-cwgz9    1/1     Running   0          2m15s
```

Now, think back to the video on how SSL & TLS work.  We need to have an Issuer to issue certificates to us on demand.
The Issuer will react to the creation of a `CertificateRequest`, and use the properties in that to create and sign a
certificate.

## Exercise

Create a file called `self-signed-issuer.yaml` and put the following contents into it.  This configuration will
create the resources:

1. A root certificate issuer called `selfsigned-issuer`
2. A root certificate stored in a secret called root-secret in the cert-manager namespace
3. An intermediate certificate issuer called selfsigned-ca-issuer

```yaml
apiVersion: cert-manager.io/v1
kind: ClusterIssuer
metadata:
  name: selfsigned-issuer
spec:
  selfSigned: {}
---
apiVersion: cert-manager.io/v1
kind: Certificate
metadata:
  name: selfsigned-ca
  namespace: cert-manager
spec:
  isCA: true
  commonName: selfsigned-ca
  secretName: root-secret
  privateKey:
    algorithm: ECDSA
    size: 256
  issuerRef:
    name: selfsigned-issuer
    kind: ClusterIssuer
    group: cert-manager.io
---
apiVersion: cert-manager.io/v1
kind: ClusterIssuer
metadata:
  name: selfsigned-ca-issuer
spec:
  ca:
    secretName: root-secret
```

After applying that into your, run this command and print the output `k get clusterissuer -o wide`:

```
NAME                   READY   STATUS                AGE
selfsigned-ca-issuer   True    Signing CA verified   23s
selfsigned-issuer      True                          23s
```

Now we have the Cert Manager installed with a valid Issuer, we can issue a request for a certificate.  The way we do
that is quite simple, we add an annotation and an extra bit of TLS config in the Ingress resource.

Update your `bootcamp-ingress.yaml` file by adding the extra stuff below:

```yaml
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: bootcamp
  annotations:
    nginx.ingress.kubernetes.io/use-regex: "true"
    cert-manager.io/cluster-issuer: "selfsigned-ca-issuer" # New annotation which says which Issuer you want a cert from
    cert-manager.io/common-name: "cube.midships.io"        # New annotation the Common name for the cert
spec:
  ingressClassName: nginx
  tls:                                                     # New TLS configuration
    - hosts:
        - cube.midships.io                                 # List of Subject Alternative Names for the certificate
      secretName: bootcamp-certificate                     # K8s Secret to store the certificate in
  rules:
    - host: cube.midships.io
      http:
        paths:
          - path: /bootcamp/.*
            pathType: ImplementationSpecific
            backend:
              service:
                name: springbootcamp
                port:
                  number: 8080
```

Apply the above into your `bootcamp` namespace and the following things will happen:

1. The Cert Manager will see the change on the `Ingress` resource and use the new configuration to create a
   Certificate Signing Request - `k get certificaterequests`
2. The Cert Manager will use the issuer defined on the Ingress (`selfsigned-ca-issuer`) to issue a new Certificate -
   `k get certificate`
3. The Cert Manager will create a Secret in the bootcamp namespace called `bootcamp-certificate` which will contain
   the Certificate, and it's private key plus the CA certificate that signed it.

To validate run the following command and paste the output below

```shell
k get secret bootcamp-certificate -o jsonpath='{.metadata.annotations}' | jq
```

```
{"cert-manager.io/alt-names":"cube.midships.io","cert-manager.io/certificate-name":"bootcamp-certificate","cert-manager.io/common-name":"cube.midships.io","cert-manager.io/ip-sans":"","cert-manager.io/issuer-group":"cert-manager.io","cert-manager.io/issuer-kind":"ClusterIssuer","cert-manager.io/issuer-name":"selfsigned-ca-issuer","cert-manager.io/uri-sans":""}
```

Now lets check that the server is presenting the new certificate using this command, again print the output below:

```shell
openssl s_client -showcerts -servername "cube.midships.io" -connect "cube.midships.io:443" </dev/null \
  | openssl x509 -noout -subject -ext subjectAltName
```

```
Connecting to 127.0.0.1
depth=0 CN=cube.midships.io
verify error:num=20:unable to get local issuer certificate
verify return:1
depth=0 CN=cube.midships.io
verify error:num=21:unable to verify the first certificate
verify return:1
depth=0 CN=cube.midships.io
verify return:1
DONE
subject=CN=cube.midships.io
X509v3 Subject Alternative Name: 
    DNS:cube.midships.io
```

## One Final Step

Running `curl https://cube.midships.io/bootcamp/actuator/health` (without the --insecure flag) still returns an error.
To fix this for our local environment, we sadly have to trust the self-signed certificate authority which we will do

```shell
kubectl get secret/root-secret -o 'jsonpath={.data.ca\.crt}' -n cert-manager \
  | base64 -d \
  | sudo tee /usr/local/share/ca-certificates/bootcamp-root.crt && \
  sudo update-ca-certificates
```

Once the above is run, you can now trust any certificate issued by your local Cert Manager :)

Try it now with `curl https://cube.midships.io/bootcamp/actuator/health`.

One final exercise, describe _in detail in your own words_, what the above command is doing, not the curl, but the
one starting `kubectl get secret/root-secret`...

```
as our cert is still self signed (not by an well known trusted ca), the command add our cert manager as a trusted ca manually to cube so cert signed by the cert-manager is also deem as signed by a trusted CA.
```

One final note, if you access the above URL in your host OS browser, it still won't be trusted.  As we have only trusted
the Root Certificate in the Cube's trust store.  We won't cover how to update the host operating system trust mechanism
as there are too many variables.

## Links

[Cert Manager](https://cert-manager.io/docs/)

[Ingress Nginx](https://kubernetes.github.io/ingress-nginx/)

# Conclusion

Congrats on completing this chapter! There was a lot in this one, but now you have a more complete cluster, with an
Ingress mechanism, that can issue certificates for you on demand.
