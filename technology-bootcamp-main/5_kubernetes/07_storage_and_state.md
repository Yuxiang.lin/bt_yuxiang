# 07 - Storage And State

We touched a bit on state storage earlier, but here we go into more detail on the various parts, from the temporary
in-memory storage option to the persistent kind for managing state across Pod termination and re-creation.

## Introduction to Kubernetes Storage

Kubernetes provides several types of storage volumes that are used to manage and persist data within a cluster.
Understanding these storage options is crucial for managing stateful applications. The main components we'll cover are:

1. **Volumes**
2. **StorageClasses**
3. **PersistentVolumes (PV)**
4. **PersistentVolumeClaims (PVC)**

## 1. Volumes

In Kubernetes, a volume is a directory that is accessible to containers in a Pod. Volumes are used to store data that
needs to persist across container restarts. There are various types of volumes like emptyDir, hostPath, configMap,
secret, and more.

### Example: EmptyDir Volume

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: test-pod
spec:
  volumes:
    - name: test-volume           # Volume name to use in mounting in a Container
      emptyDir: { }               # Type of Volume, { } show there is no additional configuration needed for this key
  containers:
    - name: test-container
      image: busybox
      command: [ "sh", "-c", "while true; do echo $(date) >> /data/hello.txt; sleep 1; done" ] # Using the Volume
      volumeMounts:
        - name: test-volume
          mountPath: /data        # Path in this Container the volume is mounted at
```

This `emptyDir` example creates an empty directory in the Pod that you can use for storing temporary content for the
operation of all Containers in the Pod.  Note that the `emptyDir` survives Container crashes, so it will still be
there when the Container is restarted.  If the Pod is deleted, the `emptyDir` is deleted permanently.

If you run the above, you can SSH into the Pod and look at the file growing in the /data directory.  Delete this Pod
before continuing.

## 2. StorageClasses

A StorageClass provides a way to describe the "classes" of storage that a cluster can offer. Different classes might map
to quality-of-service levels, backup policies, or other parameters determined by the cluster administrators.  This
can include where the data is ultimately stored, for example in a storage disk local to a VMWare farm running the
K8s cluster, or an EFS, EBS or S3 backed file system in AWS.

### Example: StorageClass

This example describes a storage class that is backed by the AWS EBS service, specifically using the GP2 storage
type.  Cluster administrators can create more storage classes backed by different mechanisms and make them available
to all workloads in a cluster or to just specific ones as needed.  We won't go into this, but StorageClasses are key
for understanding how a PersistentVolume is actually provisioned.

```yaml
apiVersion: storage.k8s.io/v1
kind: StorageClass
metadata:
  name: standard
provisioner: kubernetes.io/aws-ebs     # Storage provisioner
parameters:
  type: gp2                            # Cloud platform-specific type
```

## 3. PersistentVolumes (PV)

A PersistentVolume (PV) is a piece of storage in the cluster that has been provisioned by an administrator or
dynamically provisioned using StorageClasses. It is a resource in the cluster just like a node is a cluster resource.
PVs are independent of Pods.

PersistentVolumes, when created directly through an API call or through a VolumeClaimTemplate (see the StatefulSet
example in Chapter 5), are external to the Cluster and persisted across all Pod, Node and Cluster operations.

PersistentVolumes are defined with a capacity, access modes and a specific type (like `hostPath` in the below
example).

A PersistentVolume can only be used by one Workload through a claim on that PersistentVolume, created by using a
PersistentVolumeClaim.  A StorageClass can be more flexible as the Storage Provider, a cloud platform for example, can
dynamically provision volumes on demand.  On-premise, things are more limited and PersistentVolumes are usually
created up front at specific sizes.

### Example: Static PersistentVolume

```yaml
apiVersion: v1
kind: PersistentVolume
metadata:
  name: pv-demo
spec:
  capacity:
    storage: 1Gi                           # Capacity of the volume
  accessModes:
    - ReadWriteOnce                        # Access mode - one Pod can read from and write to this volume
  hostPath:                                # Volume type, this one provided by a particular path on the host
    path: /mnt/data                        # The path on the host to provide when mounting in to a Pod
  persistentVolumeReclaimPolicy: Recycle   # Recycle this Volume when the Claim on it is relinquished
```

## 3. PersistentVolumeClaims (PVC)

A PersistentVolumeClaim (PVC) is a request for storage by a user. A PVC specifies the size and access modes of the
storage needed. It binds to a PV that matches the request.

### Example: PersistentVolumeClaim

```yaml
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: pvc-demo
spec:
  accessModes:
    - ReadWriteOnce
  resources:
    requests:
      storage: 1Gi
```

## Putting It All Together

Here's a complete example that includes a  dynamically provisioned PersistentVolume, and a PersistentVolumeClaim
attached to a Pod.  _Note we are not including a StorageClass in this example, as we don't have a Controller deployed
to manage creating PersistentVolume from the StorageClass._

Create YAML files containing the following resources and apply all of them (in order) into your k8s cluster.

### Create a PersistentVolume

```yaml
apiVersion: v1
kind: PersistentVolume
metadata:
  name: pv-bootcamp
spec:
  persistentVolumeReclaimPolicy: Recycle
  capacity:
    storage: 1Gi
  accessModes:
    - ReadWriteOnce
  hostPath:
    path: /mnt/data
```

### Create PersistentVolumeClaim

```yaml
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: pvc-bootcamp
spec:
  accessModes:
    - ReadWriteOnce
  resources:
    requests:
      storage: 100Mi
```

### Use PVC in a Pod

Create a Pod, StatefulSet or a Deployment that uses the above PersistentVolumeClaim.  When the Pod is up and running,
run the following command to get the full YAML specification of your _Pod_ and paste the content below:

```shell
kubectl get po <POD_NAME> -o yaml
```

```
apiVersion: v1
kind: Pod
metadata:
  creationTimestamp: "2024-09-22T15:09:08Z"
  generateName: pvc-
  labels:
    app: echo-server-pvc
    apps.kubernetes.io/pod-index: "0"
    controller-revision-hash: pvc-54cd967f5
    statefulset.kubernetes.io/pod-name: pvc-0
  name: pvc-0
  namespace: bootcamp
  ownerReferences:
  - apiVersion: apps/v1
    blockOwnerDeletion: true
    controller: true
    kind: StatefulSet
    name: pvc
    uid: 6e24ec34-72ab-4ad5-b97e-c5b8e006ef8d
  resourceVersion: "510320"
  uid: 856e263f-9ed3-4d30-b5a2-ea1154e5ebcf
spec:
  containers:
  - image: ealen/echo-server:latest
    imagePullPolicy: Always
    name: echo-server-pvc
    ports:
    - containerPort: 80
      name: http
      protocol: TCP
    resources: {}
    terminationMessagePath: /dev/termination-log
    terminationMessagePolicy: File
    volumeMounts:
    - mountPath: /demo
      name: demo-pvc
    - mountPath: /var/run/secrets/kubernetes.io/serviceaccount
      name: kube-api-access-7zzmc
      readOnly: true
  dnsPolicy: ClusterFirst
  enableServiceLinks: true
  hostname: pvc-0
  nodeName: docker-desktop
  preemptionPolicy: PreemptLowerPriority
  priority: 0
  restartPolicy: Always
  schedulerName: default-scheduler
  securityContext: {}
  serviceAccount: default
  serviceAccountName: default
  terminationGracePeriodSeconds: 30
  tolerations:
  - effect: NoExecute
    key: node.kubernetes.io/not-ready
    operator: Exists
    tolerationSeconds: 300
  - effect: NoExecute
    key: node.kubernetes.io/unreachable
    operator: Exists
    tolerationSeconds: 300
  volumes:
  - name: demo-pvc
    persistentVolumeClaim:
      claimName: pvc-bootcamp
  - name: kube-api-access-7zzmc
    projected:
      defaultMode: 420
      sources:
      - serviceAccountToken:
          expirationSeconds: 3607
          path: token
      - configMap:
          items:
          - key: ca.crt
            path: ca.crt
          name: kube-root-ca.crt
      - downwardAPI:
          items:
          - fieldRef:
              apiVersion: v1
              fieldPath: metadata.namespace
            path: namespace
status:
  conditions:
  - lastProbeTime: null
    lastTransitionTime: "2024-09-22T15:09:12Z"
    status: "True"
    type: PodReadyToStartContainers
  - lastProbeTime: null
    lastTransitionTime: "2024-09-22T15:09:08Z"
    status: "True"
    type: Initialized
  - lastProbeTime: null
    lastTransitionTime: "2024-09-22T15:09:12Z"
    status: "True"
    type: Ready
  - lastProbeTime: null
    lastTransitionTime: "2024-09-22T15:09:12Z"
    status: "True"
    type: ContainersReady
  - lastProbeTime: null
    lastTransitionTime: "2024-09-22T15:09:08Z"
    status: "True"
    type: PodScheduled
  containerStatuses:
  - containerID: docker://7043cd24db4b05705d7c5cdbeee1f005854b7049b1c2241db6b59a81a0134177
    image: ealen/echo-server:latest
    imageID: docker-pullable://ealen/echo-server@sha256:ec8a6e95890df937a1eb5fafca033a32172d4f43c1fea1f302931d5f230a137f
    lastState: {}
    name: echo-server-pvc
    ready: true
    restartCount: 0
    started: true
    state:
      running:
        startedAt: "2024-09-22T15:09:12Z"
  hostIP: 192.168.65.3
  hostIPs:
  - ip: 192.168.65.3
  phase: Running
  podIP: 10.1.0.136
  podIPs:
  - ip: 10.1.0.136
  qosClass: BestEffort
  startTime: "2024-09-22T15:09:08Z"your answer here
```

## Conclusion

By using PersistentVolumes, PersistentVolumeClaims, and StorageClasses, Kubernetes provides a robust mechanism to manage
storage resources for applications running in a cluster. This setup allows for better data management, portability, and
flexibility in handling storage needs.
