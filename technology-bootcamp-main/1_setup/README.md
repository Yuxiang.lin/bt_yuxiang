# Setting up your environment

The aim of this first exercise is:

1. Learn and become comfortable with a popular IDE - Visual Studio Code.
2. Be comfortable using that IDE in a linux environment
3. Learn about extensions and how they can improve your productivity
4. Use git as your SCM and become very comfortable with it

## 1. Installing Visual Studio Code
[MacOS]

1. Install VS Code:
   * Option 1 - Install from VS Code official Website :
     - A. Download the Installer:
        * Visit the official Visual Studio Code download page: [VS Code Download Page](https://code.visualstudio.com/download)
        * Select the version for macOS & the type of chip (intel or apple or universal[works for both]). It usually downloads as a .zip file.
     - B. Install the Application:
        * Open the downloaded .zip file to extract the VS Code application.
        * Drag the Visual Studio Code app to the Applications folder. This makes it available in the Launchpad.
   * Option 2 - Install using Homebrew:
     - A. Run brew install command using CLI : 
        * If brew is installed, run -> ` brew install --cask visual-studio-code `           
            
2. Launch Visual Studio Code:
    * Open your Applications folder and double-click on Visual Studio Code to start it.
    * The first time you open it, macOS may warn you that it is from the internet. Confirm you want to open it.
    * (Optional) Enable command line usage:
    * Launch VS Code, open the Command Palette (Shift+CMD+P) and type 'Shell Command: Install 'code' command in PATH'.
    Click on the suggestion to execute the command. This allows you to open VS Code from the terminal by typing '_code_'.

[Windows]
1. Download the Installer:
    * Go to the official Visual Studio Code website: VS Code Download Page.
    * Choose the Windows version (either the user or system installer, typically a .exe file).
2. Run the Installer:
    * Once the download is complete, open the .exe file to start the installation process.
    * Follow the prompts in the setup wizard. Agree to the license agreement.
3. Select Installation Options:
    * Choose the installation path if you want to change the default.
    You may be offered options like adding VS Code to the PATH (recommended for easy access from the command prompt) and creating a desktop icon.
4. Complete the Installation:
    * Continue through the installer and click 'Finish' when done. You may choose to launch VS Code immediately after installation.
5. Launch Visual Studio Code:
   * You can start VS Code by double-clicking the desktop icon, if created, or by searching for "VS Code" in the start menu.

## 2. Setting up a linux environment locally

*_Users with macbooks and linux on their computers can skip this step_ 

[Windows]
1. Open a command prompt as Administrator
2. Type `wsl --install`
3. You will be asked to restart your computer to complete the installation. Then you will be asked to pick a username and finally if all works well, you will be presented with an Ubuntu shell.
4. At this point you have successfully setup a linux environment. You can subsequently launch a linux shell by typing `wsl`.


### Setting up vscode to work from WSL [windows only] 
1. Go to your wsl shell
2. Type `code .`
3. This will setup and launch vscode which will work within wsl

_Always be mindful of whether you are using vscode from windows or WSL and switch appropriately._

## 3.vscode extensions

Using the extensions icon on the left of your vscode, install the following extensions:

1. Shellcheck (by Timon Wong)
2. Docker (by Microsoft)
3. Python (by Microsoft)
4. Compare Folders (by MoshFeu)
