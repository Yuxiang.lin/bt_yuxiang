#! /bin/bash

# create a ED25519 keypair if no existing keypair
ssh-keygen -t ed25519 -C "yuxiang.lin@midships.io"

#copy the public key to clipboard so it can be added to gitlab
tr -d '\n' < id_ed25519.pub| pbcopy

#add public key to gitlab on the browser and test connectivity with SSH 
 ssh -T git@gitlab.com

