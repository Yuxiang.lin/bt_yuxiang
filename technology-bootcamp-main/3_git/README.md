# Exercise 3: Git

Welcome to Exercise 3! You are well on your way to becoming something of an expert at Midships! (However, you still have a long way to go - hah!)

In this exercise, you will learn how to use intermediate and advanced git features and not feel intimidated by them.
Most often people feel scared of git when 
 - there are conflicts during merges, 
 - when code is checked into the wrong branch, 
 - when an earlier commit used to work and now there are too many commits from changes and merges.

By the end of this exercise, you will hopefully not be scared of the above anymore. More importantly, you will learn how to use git to manage the above issues and not simply resort to copying all the code into yet another new branch and fix it manually.

This is not a tutorial for beginners. You are expected to be familiar with installing git, using `git clone, git add, git commit, git status and git merge`.

## Git Mental Model 

![Git Architecture](git-architecture.png)

One of the most important things you can do when learning Git is to have a very clear mental model about what part of Git repo you are updating when executing your command.
Git has four areas:
 - Working Tree(Or working area): - This is the place where your files are first added or created. This is in your local computer and git only knows this as untracked files.
 - Staging Area: This is the place where you move your files to for git to start tracking them.
 - Local Repo: This is a fully functional git repo residing locally on your computer. 
 - Remote Repo - This is the git repo residing on the remote server where you eventually push your files to.

 Take a moment to go above the above diagram (courtesy of http://wwww.softpost.org).
  - Try and understand the difference between `git pull` and `git fetch`
  - Look at `git diff`. Do you know why you need to run `git pull` or `git fetch` before you can compare branches using `git diff`?

  As you progress through these exercises, keep a copy of this diagram handy and keep asking yourself what part of storage are you affecting with your command.

## Getting Started
  1. This exercise assumes you have git installed and familiar with basic git commands
  2. For each answer that you have to write out, please write out the command or explanation. You should NOT attach the output of the command with your answer. 
  3. You should only use git via the commandline (unless explictly asked to do otherwise). Do not use any third-party tools. This exercise aims to make you proficient in git. 
  4. Download a zip file of the repo **technology-bootcamp** and move it into your own branch bt_\<username> and push the changes into your remote repo. The main thing you will need for this exercise is the folder **3_git**. 

## No passwords
 - Setup an ssh key with gitlab to ensure that you never have to enter your gitlab password while working with the remote repo (clone, push, pull, etc). Setup this key with ed25519 encryption and no passphrase. Enter the high-level steps and commands you executed to do this.
```
# create a ED25519 keypair if no existing keypair
ssh-keygen -t ed25519 -C "yuxiang.lin@midships.io"

#copy the public key to clipboard so it can be added to gitlab
tr -d '\n' < id_ed25519.pub| pbcopy

#add public key to gitlab on the browser and test connectivity with SSH 
 ssh -T git@gitlab.com
```

## git status and git log aliases
 - Setup an alias for `git log` (gl) and `git status` (gs). Add it into your .bashrc file so it is automatically picked up everytime you start a new session. How did you craete these aliases?
```
nano ~/.zshrc
#add below to .zhrc 
alias gs='git status'
alias gl='git log'
#update current terminal session
 . ~/.zshrc
```

## HEAD and origin
 - Explain what `origin` means in Git along with an example command below
```
"origin" is a shorthand name for the remote repository that a project was originally cloned from. Tt is used instead of that original repository's URL
# to check what is the current remote repo url
git remote show origin 
```
- Explain what `HEAD` means in GIt along with an example command below
```
HEAD is the current branch
# check current head(branch)
cat .git/HEAD
```

## Git log
For this exercise, clone the repo **technology-bootcamp** into your computer. Swtich to the branch **main**.
 - Using git log, print a list of all the commit hashes and commit messages (one per line) for the main branch.
```
gl  --oneline
```
 - Print the last 5 commits for this branch
```
 gl  -5 --oneline
```
 - Print the list of commits that have happened between 17-May-2024 and 20-May-2024
```
gl --since='17 May 2024' --until='20 May 2024' --oneline
```
 - Print a list of all commits for README.md
```
gl --oneline --follow -- README.md
```
 - Print a list of all commits for README.md where the author is "Aja R" and show the diff (addtions and deletes) for each commit
```
## tested using my own repo, so there is no Aja as author, so tested with myself as Author
 gl -p --author="Yuxiang Lin" --follow -- README.md
```

### Branches branches everywhere
In this exercise you are going to keep switching between branches, pull chnages across them, merge them, examine them, etc, etc. Work through the exercise slowly and carefully and using `gs`(git status) and `gl` (git log) to frequently check the status and commits to fully grok what's happening.

 - In your repo Bt_\<username>, go to the folder 3_git.
 - Create a new branch locally called **br_1_fruits**
 - add a file called fruits.txt and add 3 fruits here (one per line) - apple, banana and mango.
 - Using git add, commit and push - push this branch to your remote repository
 - What were all the commands you executed to do the above
```
git checkout -b br_1_fruits
git add . 
## added the fruit.txt and check and confirm that its the fruit txt that is going to be commited
gs
git commit -a -m "add fruit txt"
## check that there is one commit not pushed to origin yet (the new branch created)
gl
git push origin br_1_fruits
## check if now commit is already pushed to the origin (new branch)
gl
```
 - Switch to the branch **main**
 - using git pull, you want to pull changes from br_1_fruits to the main branch. However, you only want to see what would be changed without actually pulling the files. How can you do this using git pull?
```
git switch main
# using git fetch seem to be better here, do we need to use git pull?
git fetch origin br_1_fruits
git diff HEAD..origin/br_1_fruits
```
 - Using git pull, get the changes from br_1_fruits and push those changes to the main branch origin. What commands did you execute to do this?
```
git pull origin br_1_fruits
git push origin main
```
### Discarding changes

- From the main branch, add another fruit, **carrot** to your fruits.txt file and save it locally. Do not commit the file.
- Doh! Carrot is not a fruit, you silly goose. You wish to discard all your local changes and get the files from the latest commit in the remote repo. Do this using `reset` and `HEAD`. Provide a second answer where you can do this with `git clean`.
```
git reset --hard HEAD
or
git checkout fruit.txt
git clean -d -x -f
```

**Pro Tip**: Did you know when using git from the commandline, it support autocompletion for branch names as well? For e.g., try this type **git checkout br_1\<Tab>**, This will complete the full branchname for you.

### Comparing changes across branches, working areas local and remote

 - Switch to the branch br_1_fruits and update fruits.txt by adding "apricot" as the first fruit. You should now have 4 fruits in there (apricot, apple, banana, mango). Commit and push the changes to the remote server.
  - Switch to the branch main and update fruits.txt by adding "kiwi" as the last fruit. You should have 4 fruits in there (apple, banana, mango, kiwi). Stage your changes but do not commit it yet.
 - Now, with all these changes, you are not sure how your local working copy differs from what is in the remote main branch. How do you compare these two?
```
git diff --staged
```
 - Now you also want to compare your local working copy against the branch br_1_fruits. How do you compare these two?
```
git diff --staged origin/br_1_fruits
```
 - Now also compare the remote main branch against remote br_1_fruits branch. How would you do this?
```
git diff HEAD..origin/br_1_fruits
```
### Using git to temporarily hide your changes instead of creating a backup folder

 - You want to first get the changes from br_1_fruits into main. After that you want to apply changes from your working copy on top of it and then push it to main branch. At the end of this operation. 

**Hint**: This involves git stash, pull and merge. You can use VS Code to perform the merge manually. Examine all the options in VS Code and get comfortable with merging conflicts using vscode. 

At this point your file should contain 5 fruits (apricot, apple, banana, mango and kiwi). Commit and push your changes into remote main branch. List all the commands you executed to perform these steps.
```
git stash
git merge origin/br_1_fruits
# reolve conflict
git add .
git stash apply
# reolve conflict
git add .
git commit -a -m "merging again"
git push origin main
```
- Confirm that there still exists a difference between main and br_1_fruits. What command did you run to confirm this?
```
git diff HEAD..origin/br_1_fruits
```

### Undoing changes after they have been committed

- Create a new branch br_2_fruits, add a new fruit at the end - orange. Commit and push the branch to the remote server.
- Switch branch to main and pull changes from br_2_fruits.
- Now you regret having an orange in your list and you wish to revert this latest pull commit. At the end of this you should have 5 fruits only. What command did you execute for this?
```
git checkout -b br_2_fruits
git add .
git commit -a -m "add orange"
git push origin br_2_fruits
git switch main
git merge origin/br_2_fruits
git log
git revert HEAD
git add .
git commit -a -m "revert merge"
```
 - Create a new branch br_3_fruits.
 - Add a new fruit pineapple to fruits.txt. Commit and push this to remote server.
 **Pro Tip**: You can chain multiple commands like this: `git add fruits.txt && git commit -m "adding pineapple" && git push`.
 - Add a new file called poultry.txt with a single line item called chicken. Save, commit and push it to remote server.
 - Add a new file called veggie.txt with a single line item called potato. Save, commit and push it to the remote server.
 - Now, you dont really want the changes pushed with the poultry.txt. Revert that commit to ensure your branch no longer contains poultry.txt. How do you do this with git revert?
 **Hint**: Use `git log --oneline` for an easy to read summary of commits.
```
git checkout -b br_3_fruits
git add .
git commit -a -m "add grape"
git add .
git commit -a -m "poultry"
git add .
git commit -a -m "veggie"
git push origin br_3_fruits
git revert b54585a

```
 - Add another line item to fruits.txt called egg. Save, commit and push it to remote server
 - Add another line item to fruits.txt called watermelon. Save, commit and push it to remote server
 - Ugh! Egg is not a fruit!!!!! Try and do a git revert on it and see what happens. If you use vs code, does it get any easier to deal with the conflict.
 - Explain what happened here
```
git revert c00d574
# its going back to the parent of egg, which also deletes watermelon
# from VS code, merge editior, can change the result and also commmit
```

### Learning to tag your changes everytime you hit a significant milestone. 
 - Switch back to main branch. Confirm that fruits.txt has 5 fruits.
 - Phew! This is a major milestone. You want this commit to be tagged for your first release - **v1.0.0**!
 - Using `git tag` tag the latest commit on main branch as v1.0.0.
```
git tag v1.0.0 

```
 - Now browse to gitlab and your repo and check if you can see the tag. Why not? How do you push the tag to the remote server?
```
git push origin --tags
```

### More ways to get branches to tell your their deepest secrets

 - List all the branches of your repo that are pushed to the remote server only
```
git branch -r
```
 - List all branches of your repo that are not yet pushed to the remote server
```
git branch -l
```
 - Switch to main branch. List all branches that are merged into your current branch.
```
git branch --merged
```
 - List all branches that are not merged into your current branch.
```
git branch --no-merged
```
 - List all the commits made to fruits.txt with one commit info per line.
```
gl --oneline --all -- fruit.txt
```

### The final cherry on this cake.....
- Now, you will learn to use cherry-pick to pick up specific commits from a branch and apply it to main branch. You will also perform manual merges and ultimately ensure you are comfortable using git to pick up specific changes.
- Switch to main branch, examine fruit.txt and confirm it has the following fruits in it (apricot, apple, banana, mango, kiwi). If it does not, then modify the file to ensure it has these 5 fruits in it.
- Switch to branch br_3_fruits and confirm it has the following in it (apricot, apple, banana, mango, kiwi, grape, egg, pineapple). Also make sure that grape, egg and pineapple all have their own commits.
- With this in place, switch back to main branch.
- Now do a diff between main branch and br_3_fruits
```
 git diff HEAD..origin/br_3_fruits
```
 - Now use git cherry-pick with the commit id of grape. Confirm that the fruits.txt file in main branch shows grape. How did you do this?
```
git cherry-pick 413155e
```
 - Now use git cherry-pick with the commit id of pineapple.(Notice we have skipped egg since egg is a really bad fruit). Notice git has detected a conflict. Resolve the conflict using vscode, commit the changes and push it to remote server. Explain your steps below.
```
git cherry-pick 024ea9c

```

Congratulations! You are ready to go forth and git!

**Git gud, my fellow techies!**






