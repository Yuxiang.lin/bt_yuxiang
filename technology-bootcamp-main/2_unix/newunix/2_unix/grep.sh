#! /bin/bash

#grep with range
printf "==========grep for logs between 09:01 to 09:03 start=============\n\n"
grep -E "09:(0[1-3])" ./sample.log
printf "==========grep for logs between 09:01 to 09:03 end===============\n\n"

# count line 
printf "==========count number of INfo level log start=============\n\n"
grep -E "INFO" -c ./sample.log
printf "==========count number of INfo level log end===============\n\n"

# print up and down line 
printf "==========print 3 up 5 down of error log start=============\n\n"
grep -E "ERROR" -B 3 -A 5 ./sample.log
printf "==========count number of INfo level log end===============\n\n"

# grep with invert match
printf "==========grep all INfo that do not deal with alice and bob start=============\n\n"
grep -v -E  "alice|bob" sample.log | grep "INFO"
printf "==========grep all INfo that do not deal with alice and bob end===============\n\n"

# grep for file with keyword, and show only file name
printf "==========grep for filename start=============\n\n"
grep -R -l "bash" 
printf "==========grep for filename start end===============\n\n"

# grep for line number and only print line number
printf "==========grep line number start=============\n\n"
grep -n -o "bash" ./README.md | grep -o -E "[0-9]+"
printf "==========grep for line number start end===============\n\n"

#grep with exclude
printf "==========grep with exclude start=============\n\n"
grep -R  --exclude="*.sh" --exclude="*.log" "bash" 
printf "===========grep with exclude end===============\n\n"

#grep with end of line match
printf "==========grep with end of line matc start=============\n\n"
grep -E "out$" ./sample.log
printf "===========grep with end of line match end===============\n\n"



