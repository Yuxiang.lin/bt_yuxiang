#! /bin/bash

awk '{ for(i=1; i<=NF; i++) if ($i ~ /^org/) print $i }' sample.log

awk '{ for(i=1; i<=NF; i++) if ($i ~ /^org/) print $i }' sample.log | sort | uniq

awk -F, 'NR>1 { if ($4 > 25) print $0}' users.csv

awk -F, 'NR>1 { sum+=$4} END {print sum}' users.csv 

awk -F, 'NR>1 { if($3>=30) sum+=$4} END {print sum}' users.csv