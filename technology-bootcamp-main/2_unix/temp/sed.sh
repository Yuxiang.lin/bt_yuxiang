#! /bin/bash

#substitute
sed -i -e "s/forgerock/midships/g" sample.log

#remove
sed -i .bak -e 's/\[[^][]*\]//g' sample.log

#conditional execution
sed -i .bak -e '/INFO /s/user: [a-z]*/user: anon/g'   sample.log

