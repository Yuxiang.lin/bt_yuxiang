# Exercise 2: Unix

Welcome to exercise 2! 
Congratulations on completing exercise 1! You are already getting your engines warmed up and ready to rev. 

**Wroooom!**

These exercises will no longer hand-hold you or give you precise steps. Only high level steps will be given to instruct you on what needs to be achieved (and sometimes, imposing constraints!). You are expected to research the answers through experimentation and google and figure it out on your own. This is not a beginners's tutorial. For that, there are lots of excellent resources on the internet. This assumes familiarity with basic unix commands. If you are not faimiliar with those, do spend some time googling and get comfortable with `cd, ls, pwd, sudo, head, tail, cat, echo, cp, rm, top and ps`.

Sometimes, there may be multiple answers to solving the same problem and that is ok. I am eager to see how **YOU** approach this problem and solve it.

**Reminder**: Don't ask others for help or chatGPT. Use google and unix `man` pages. They are dense but amazing! If you dont like or not used to `man` pages then use google.

Where you see the block `your answer here`, you are expected to paste the command or write an explanation of your answer. You need not paste the output of your command here. 

## Getting Started
**Pre-requisites**
1. Completed exercise 1
2. Setup your own git repo `tb_<username>`. **Important**: This is your own repo and not a branch of _technology-bootcamp_ repo.
3. Add Paul and Aja as Developers on your repo above.

## Exercise
### 1. Debugging a basic bash script
 - Copy folder (ex2_unix) into your repo.
 - Open the file first.sh and launch the bashdb debugger by pressing F5 and setup a launch.json. Notice that you now have a .vscode folder created.
 - Using a git specific file, make sure that this folder (.vscode) never gets checked in or marked as an untracked file. Explain how you managed to get git to ignore the .vscode folder:
```
see commit "added git ignore"
```
 - Commit your changes (with an information commit message) and push it into the remote repo
 - View the commit history for your current branch. Paste the command below.
 ```
 using github desktop as UI so its shown there, on terminal tested git log, can also see commit history
 ```
  - Place a breakpoint on Line 6 of first.sh and launch the debugger. When the debugger stops at line 6, examine the variables in the local windows in "Run and Debug" window.
 - What does the variable $? mean?
 ```
 it looks like its showing some status of the previous command exececuted, assume the value 0 means things are good with the previous command 
 ```
  - Print the value of the _dir_ variable from the watch window. What is the value?
  ```
'/Users/yuxianglin/Development/bt_yuxiang'
  ```

### 2. grep
For this exercise, we will use sample.log as the practise data file. Open it and have a quick glance at its contents.

 - Using only grep, print the lines of logs that occur between 09:01AM and 09:03AM. You can only use the grep command
```
grep -E "09:(0[1-3])" ./sample.log
```
- Using the grep command, count the number of lines that have INFO log level. You should not use wc command
```
grep -E "INFO" -c ./sample.log
```
 - There is a single ERROR in this log file. You want to do a quick check for ERRORS around this line. Print 3 lines before the ERROR line and 5 lines after the ERROR line. Do this using only grep.
 ```
grep -E "ERROR" -B 3 -A 5 ./sample.log
 ```
 - You want to check all INFO statements except where it contains the users alice or bob. Using grep print all INFO lines except those which contain alice or bob. Hint: You can chain grep commands
 ```
grep -v -E  "alice|bob" sample.log | grep "INFO"
 ```
 - Using grep check all files in this folder where the whole word 'bash' is present. Print only the file names which contain the word bash.
 ```
grep -R -l "bash"
 ```
 - Using grep print the line numbers on README.md where the string bash is present.
 ```
grep -n -o "bash" ./README.md | grep -o -E "[0-9]+"
 ```
 - Using grep's --exclude option, exclude _log_ and _sh_ extension when searching for the word bash in all files in the current directory
 ```
grep -R  --exclude="*.sh" --exclude="*.log" "bash" 
 ```
 - Using grep, find lines that end with the string 'out' in sample.log
 ```
grep -E "out$" ./sample.log
 ```

 ### 3. history
 
  - Before you start this command, type `history` on your commandline and confirm that you have at least 5 commands present in your history. Execute the following 2 commands before starting this exercise: `ls -lart /etc` and `grep "INFO" sample.log`.

  - If you wanted to execute the third command in your history list, how would you do that. This involves 2 steps. Write them below.
  ```
  use "history" to get the history
  use "!"+"number assigned to the command you want to run" to run that command
  ```

   - If you press `Control-R` on your commandline, you go into reverse search mode. In this mode, explain how you would search for all the grep commands you have executed and execute this one without typing in the full command `grep "INFO" sample.log`.
   ```
   type "grep" and if the reverse search is not showing "grep "INFO" sample.log", keep pressing 'control-R' till you that command and press enter
   ```

   ### 4. sed  

 - All exercises for sed will be performed on sample.log
 - Using sed, replace all occurrences of the word **forgerock** with **midships** in sample.log
 ```
 sed -i .bak "s/forgerock/midships/g" sample.log
 ```
 - If you look at samepl.log, most lines contain a thread name like [http-nio-exec-8080-exec-6]. Using sed, remove this (including the square brackets from the entire line. Hint: You will have to use regular expressions for this.
So, the line:

    **2024-05-12 09:00:00 INFO  <span style="color:red">[http-nio-8080-exec-1]</span> org.forgerock.openam.authentication - User login attempt for user: alice**

    becomes

    **2024-05-12 09:00:00 INFO  org.forgerock.openam.authentication - User login attempt for user: alice** 
```
sed -i .bak -e 's/\[[^][]*\]//g' sample.log
```
 - If you look at sample.log you can see user names in several lines. We want to anonymise all the usernames (like alice, bob, etc) with the word `anon`. We only want to do this for INFO statements and leave the names in DEBUG statements. How can you do this with sed's conditional execution capability?
 ```
sed -i .bak -e '/INFO /s/user: [a-z]*/user: anon/g'   sample.log
 ```

 ### 5. awk
 - All exercises for sed will be performed on sample.log and users.csv
 - Print the names of all classes used in each line of the log using awk. For e.g., for the first line in sample.log, it should print only `org.forgerock.openam.authentication`
 ```
 awk '{ for(i=1; i<=NF; i++) if ($i ~ /^org/) print $i }' sample.log
 ```

 - Print only the unique list of java classes found in this log file. Hint: You can use `awk`, `sort` and `uniq` to achieve this.
 ```
awk '{ for(i=1; i<=NF; i++) if ($i ~ /^org/) print $i }' sample.log | sort | uniq
 ```

 - Review users.csv. Print a list of firstname, lastname, age and pushups of users who can do more than 25 pushups.
 ```
awk -F, 'NR>1 { if ($4 > 25) print $0}' users.csv
 ```
 - Get the total sum of pushups all 4 users can do using awk
 ```
awk -F, 'NR>1 { sum+=$4} END {print sum}' users.csv 

 ```
 - Get the total sum of pushups of users who are older than 30.
 ```
awk -F, 'NR>1 { if($3>=30) sum+=$4} END {print sum}' users.csv
 ```

### 6. alias
 - Confirm there is no command in your bash session called `gs`. Then create a command such that executing gs will be the same as executing `git status`.
 ```
 alias gs='git status'
 ````
 - If you wish to ensure this alias for `git status` is created everytime you login to your bash terminal, what should you do?
 ```
nano ~/.zshrc
 add alias gs ='git status'
 restart terminal
 ```

### 7. cut
 - From users.csv print the first 3 characters of each person's firstname without the doulbe quotes. Example: For "Claire","Joy"....., it should print **Cla**. Do this for the whole file.
 ```
cut -c 2-4 users.csv
 ```
 - Using only the cut command do the same and print the first 3 characters of the lastname. Hint: You can chain multiple cut commands.
 ```
  cut -d "," -f 2  users.csv | cut -c 2-4
 ```

### 8. df and du
 - How do you check the amount of space used in all your filesystems and how much space is free?
 ```
 df -h
 ```
 - How do you check for the total amount of space used by /etc
 ```
du -s -h /etc/ 
 ```
 - How do you find the subdirectory inside /etc which contributes to the highest disk usage?
 ```
du -s -h /etc/* | sort -r -n | head -n 1
 ```

### 9. tar
- Move up one folder out of 2_unix. Now using tar archive and zip the entire 2_unix folder into a file called unix.tar.gz
``` 
tar -czf unix.tar.gz 2_unix
```
- Create a new folder called newunix. Extract unix.tar.gz into newunix using the tar command.
```
mkdir newunix
tar -xf unix.tar.gz -C newunix
```

### 10. find

 - Using find, print a list of only directories inside /etc which contain the string `it` in their name
 ```
 find /etc/  -name "*it*" 2>/dev/null
 ```

 - Print a list of all files under your home directory (and subdirectories) which were modified less than 90 minutes from the current time
 ```
find /Users/yuxianglin -type f -mmin -90
 ```

 ### 11. xargs
 - Create a folder called temp in your current location. Using xargs and find, find a list of files inside your home folder which contain the string `it`. Copy all these files into temp folder with xargs. This whole thing is a single command.
 ```
 mkdir temp
 i dont want to copy home directory files, change to "sh" in project directory
find /Users/yuxianglin/Development/bt_yuxiang/technology-bootcamp-main  -name "*sh*" -print0 | xargs -0 -I{} cp {} /Users/yuxianglin/Development/bt_yuxiang/technology-bootcamp-main/2_unix/temp
 ```

 ### 12. openssl
 - Try this command on your commandline `openssl s_client -connect google.com:443`.
 - Got to ChatGPT and ask it to explain the output of this command. Once you have read it, provide an explanation in your own words of what are 1) certificates 2) what is the benefit of using this command.
 ```
 A certificate is a document presented by a server to prove its identity. This certificate is issued by a CA which must be a 3rd party trusted issuer. The CA signs the server's public key with its private key to create a digtal signature. whoever receives this cert can first verify the signature to ensure that a trusted CA has confirm the authencity of the public key in the cert. then using the public key, SSL handshake can take place where a symetric key exchange take place for encrypting all subsequent communication.
 good to test connection, if https can be established from the server and use the output to see if need to add anything to trust store (PXIX error)
 ```  











