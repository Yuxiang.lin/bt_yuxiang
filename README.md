# Technology Bootcamp

The Midships Technology Bootcamp is aimed to get you prepared with the basic skills of working in Midships. The exercises in this bootcamp will start from the basics of setting up the commonly used tools in a linux-like environment. From there, you will progressivly learn about Git (source control), unix tools (working environment), ForgeRock components (basic installation and configuration), Midships-specific tooling (kube and Accelerator) and  Python.

**Ground Rules**:
1. There is no expectation for you to know all of the above skills. That's the aim of this bootcamp.
2. There are daily sessions setup for you to clarify your questions and if you are stuck on a particular exercise. Again, **you will not be judged for asking help**.
3. There is a strong expectation that you will try to complete the exercises yourself or use Google and search online and find out the solution.  
4. You should not ask your colleagues, friends, ChatGPT or others for answers. Similarly, do not copy-paste solution from other people. If you do this, you will be judged and will be guilty of breaking the rules of this Bootcamp.  
5. You will be expected to finish each exercise in the alloted timeframe from when you start it. If you have unofreseen circumstances get in your way, (emergencies, work deadlines, production issues,/deployments), you may ask for an extension. That is acceptable. Other excuses arent. The people who have built this course have spent a significant amount of time on it and to invest in you. We similarly ask the same of you.  

Finally, remember, the aim of this bootcamp is to help improve your skills and make you an amazing technologist. Please approach it in the right spirit of integrity, hard work and curiosity.  

## Getting started 
Here are the steps to finishing each exercise.  
1. In Gitlab, create a fork of this project in your GitLab namespace with the following naming convention: bt_paul. For example: if your name is Paul, you should create a fork called _pmckeown/bt_paul_.
2. Go to this guide - [How to Bootcamp](https://midships.atlassian.net/wiki/spaces/ORG/pages/272433154/How+to+Bootcamp) for more detailed information and screenshots.
3. Create a new branch in your repo for completing each numbered exercise by following the steps and filling in the readme.md.
4. Once finished, create a Merge Request to merge the branch to the main branch in your fork.  Add Aja or Paul as reviewer but let us both know it completed so we can share the reviewing.
5. One of the reviewer's will review your lesson or do a call with you to go over the details and once it is done, they will accept the pull request and you will have completed that exercise. Time to move to the next one!
6. Aja or Paul will update this main repo with a check mark to keep track of your progress as you work through the exercises.  Have fun!

## Student List

| Name     | Exercise 1  | Exercise 2 | Exercise 3 | Exercise 4         | Exercise 5   |
|----------|-------------|------------|------------|--------------------|--------------|
|          | _Tooling_   | _Unix_     | _Git_      | _FR Configuration_ | _Kubernetes_ |
| Aja      | &check;     | &check;    | &check;    | &check;            | -            |
| Aravind  | &check;     | &check;    | &check;    | -                  | -            |
| Atharva  | &check;     | &check;    | &check;    | &check;            | C            |
| Debasis  | &check;     | &check;    | &check;    | &check;            | -            |
| Deepak   | &check;     | R          | R          | -                  | -            |
| Florin   | &check;     | -          | -          | -                  | -            |
| Mohamed  | &check;     | &check;    | &check;    | R                  | -            |
| Pablo    | &check;     | &check;    | &check;    | -                  | -            |
| Pragnya  | &check;     | &check;    | &check;    | &check;            | -            |
| Prakhar  | &check;     | &check;    | &check;    | &check;            | C            |
| Ravi     | &check;     | &check;    | &check;    | &check;            | &check;      |
| Sri      | &check;     | &check;    | &check;    | -                  | -            |
| Utkarsh  | &check;     | &check;    | &check;    | &check;            | C            |
| Vamsi    | &check;     | &check;    | &check;    | -                  | C            |
| Vinay    | &check;     | &check;    | &check;    | -                  | -            |
| Yuxiang  | &check;     | &check;    | &check;    | &check;            | -            |
